package BitTrader::Exchange::BTCe;

use Modern::Perl;
use parent qw/BitTrader::Exchange::Base/;
use utf8;

use JSON qw/decode_json/;
use Data::Dumper;
use DateTime;
use Time::HiRes;

use Class::MethodMaker [ hash => [ qw/pair_info/ ] ];

sub EXCHANGE_ID { 'BTCe' };

my $info = 'https://wex.nz/api/3/info';
my $ticker = 'https://wex.nz/api/3/ticker/';
my $fees = 'https://wex.nz/api/3/fee/';
my $trading_pairs_list;

my $tapi_url = "https://wex.nz/tapi";
my %trading_fee;
my $last_nonce;


sub initialise
{
    my $self = shift;

# Don't ever use market rates at BTC-e ; results in massive losses
    $self->use_market_rates(0);
}

sub _get_nonce
{
	my $self = shift;
	my $new_nonce = DateTime->now()->epoch();

	if ( $last_nonce && $last_nonce >= $new_nonce ) {
        $new_nonce = $last_nonce + 1;
	}

	$last_nonce = $new_nonce;

	return $new_nonce;
}

sub load_trading_fee
{
	my $self = shift;
	my $data;

	my $response = $self->useragent()->get($fees . $trading_pairs_list);
	if ( $response->is_success ) {
		eval {
			$data = decode_json($response->decoded_content);
		     };
		if ( $@ ) {
			$self->log_info("Problem decoding response", $@, $response->decoded_content);
		}
		else {
			$self->log_debug($fees, 'returned:', $response->decoded_content);
			%trading_fee = %{$data};
		}
	}
}

sub estimated_trade_fee
{
	my ($self, $short_action, $trading_pair, $units_to_trade) = @_;
	my @ccys = split(/[\/_]/, $trading_pair);
	my $ccy_key = lc(join('_', @ccys));
	my $fee_rate = ( ($trading_fee{$ccy_key}//0.5) / 100 ) * 2;

	return ($fee_rate * $units_to_trade, $ccys[0]);
}

sub _get_trading_pairs
{
    my $self = shift;
    my @trading_pairs;
    my $data;

	my $response = $self->useragent()->get($info);
	if ( $response->is_success ) {
		eval {
			$data = decode_json($response->decoded_content);
		     };
		if ( $@ ) {
			$self->log_info("Problem decoding response", $@, $response->decoded_content);
		}
		else {
			$self->log_debug($info, 'returned:', $response->decoded_content);

            @trading_pairs = keys %{$data->{pairs}};
            $self->pair_info($data->{pairs});
		}
	}

    $trading_pairs_list = join('-', @trading_pairs);

    return @trading_pairs;
}

sub fetch_exchange_rates
{
	my $self = shift;
	my @exchange_rates;
	my $data;

	$self->load_trading_fee()				if !(keys %trading_fee);

	my $response = $self->useragent()->get($ticker . $trading_pairs_list);
	if ( $response->is_success ) {
		eval {
			$data = decode_json($response->decoded_content);
		     };
		if ( $@ ) {
			$self->log_info("Problem decoding response", $@, $response->decoded_content);
		}
		else {
			$self->log_debug($ticker, 'returned:', $response->decoded_content);
			foreach my $key (keys %{$data}) {
				my $ccypair = uc($key);
				$ccypair =~ s/_/\//;
				my $item;
                eval {
				    $item->{_EXCHANGE} = EXCHANGE_ID;
				    $item->{ccypair} = $ccypair;
				    $item->{volume} = $data->{$key}->{vol}; # Trade volume in units
				    $item->{sell} = $data->{$key}->{sell};
				    $item->{buy} = $data->{$key}->{buy};
				    $item->{high} = $data->{$key}->{high};
				    $item->{low} = $data->{$key}->{low};
				    $item->{timestamp} = $data->{$key}->{updated};
				    push @exchange_rates, $item;
                };
                if ( $@ ) {
                    $self->log_info('Error occured processing exchange response.',
                                    'Response:', $response->decoded_content,
                                    '. Error:', $@
                                   );
                }
			}
		}
	}
	else {
		$self->log_info('BTC-e fetch exchange rates failed!!', Dumper(\$response));
	}

	$self->last_exchange_update(DateTime->now());
	$self->exchange_rates(\@exchange_rates);

# TEST TEST TEST TEST
#    map {
#        $self->estimated_trade_value_in_btc($_->{ccypair}, 'sell', 5);
#    } @exchange_rates;
# TEST TEST TEST TEST

	return @exchange_rates;
}


use WWW::Mechanize;
use LWP::Debug qw(+);
use Digest::SHA qw/hmac_sha512_hex/;

sub fetch_balances
{
	my ($self) = @_;
	my $mech = WWW::Mechanize->new();
	$mech->stack_depth(0);
	$mech->agent_alias('Windows IE 6');
	my $nonce = $self->_get_nonce();
	my $url = "https://wex.nz/tapi";
	my $data = "method=getInfo&nonce=".$nonce;
	my $hash = hmac_sha512_hex($data,$self->api_secret);
	$mech->add_header('Key' => $self->api_key);
	$mech->add_header('Sign' => $hash);

	my $element;
	eval {
        $mech->post($url, ['method' => 'getInfo', 'nonce' => $nonce]);
	    $self->log_debug('getInfo returned:', $mech->content);
	    my %apireturn = %{decode_json($mech->content())};

	    $element->{_EXCHANGE} = EXCHANGE_ID;
	    $element->{timestamp} = $apireturn{return}{server_time};
	    $element->{balances} = $apireturn{return}{funds};
    };
    if ( $@ ) {
        $self->log_info('ERROR: Fetch_balances:', $@);
    }

	return $element;
}


####################################################################################################################################
# New more Discrete methods
#
sub order_place
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_trade_at, $amount_to_buy, $units_to_trade, $last_price, $do_market_trade) = @_;
	$trading_pair =~ s/\//_/;
    $rate_to_trade_at = sprintf("%0.3f", $rate_to_trade_at);

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&pair=%s&type=%s&rate=%s&amount=%s',
			     'Trade', $nonce, lc($trading_pair), lc($short_action), $rate_to_trade_at, $units_to_trade);
    $self->log_debug('order_place', $tapi_url, 'method' => 'Trade', 'nonce'  => $nonce, 'pair'   => lc($trading_pair), 'type'   => lc($short_action), 'rate'   => $rate_to_trade_at, 'amount' => $units_to_trade);
	eval { $mech->post($tapi_url,
                        [	'method' => 'Trade',
			    	        'nonce'  => $nonce,
			    	        'pair'   => lc($trading_pair),
			    	        'type'   => lc($short_action),
			    	        'rate'   => $rate_to_trade_at,
			    	        'amount' => $units_to_trade
			            ]
    ); };
	if ( $@ ) {
		$self->log_debug('ERROR Placing order', $@);
		return ( 0, undef, 'MECHERROR' );
	}

	my %apireturn;
	eval { %apireturn = %{decode_json($mech->content())}; };
	if ( $@ ) {
		$self->log_info('Unexpected non-json response from Trade call', $mech->content());
		$self->log_info('Assuming trade failed');
		return ( 0, undef, $mech->content() );
	}

	$self->log_debug('Trade returned', Dumper(\%apireturn));

    my $order_status;
  eval {
    if ( $apireturn{success} && !$apireturn{return}{order_id} ) {
        my @trades = $self->_get_trade_history();
        my @wanted_trades;
        my $valid_trades_count;
        my $units_traded = 0.000000;
        my $rate_traded_at = 0.000000;
        foreach my $trade (sort { $b->{timestamp} <=> $a->{timestamp} } @trades) {
            if ( !$valid_trades_count && $trade->{type} ne $short_action && $trade->{pair} ne lc($trading_pair) ) {
$self->log_debug('SKIPPING', 'Timestamp', $trade->{timestamp}, $trade->{type}, $trade->{pair});
                next;
            }
            last    unless $trade->{type} eq $short_action && $trade->{pair} eq lc($trading_pair);

$self->log_debug('Timestamp', $trade->{timestamp}, $trade->{type}, $trade->{pair}, 'Amount', $trade->{amount}, 'Rate', $trade->{rate});
            $units_traded += $trade->{amount};
            $rate_traded_at = $trade->{rate};
$self->log_debug('Amount now', $units_traded, '; Rate traded at now', $rate_traded_at);
            $valid_trades_count++;
        }
$self->log_debug('Used', $valid_trades_count, 'trades');
        $order_status->{units} = $units_traded;
        $order_status->{rate} = $rate_traded_at;
    }
  };
  if ( $@ ) {
      $self->log_debug($@);
  }

	return ( $apireturn{success}, $apireturn{return}{order_id}, $apireturn{error}, $order_status );
}

sub _get_trade_history
{
    my ($self) = @_;

    $self->log_trace('_get_trade_history');

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d', 'TradeHistory', $nonce);
	$mech->post($tapi_url, [ 'method' => 'TradeHistory', 'nonce'  => $nonce ]);

	my %apireturn = %{decode_json($mech->content())};

    my @toreturn = map { $apireturn{return}{$_} } keys %{$apireturn{return}};

    return ( sort { $a->{timestamp} <=> $b->{timestamp} } @toreturn );
}

#		($success, $error) = $self->exchange->order_cancel($orderid);
sub order_cancel
{
	my ($self, $orderid) = @_;

    $self->log_debug('Cancelling order id', $orderid);

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&order_id=%s', 'CancelOrder', $nonce, $orderid);
	$mech->post($tapi_url, [ 'method' => 'CancelOrder', 'nonce'  => $nonce, 'order_id' => $orderid ]);

	my %apireturn = %{decode_json($mech->content())};
	$self->log_debug('CancelOrder returned', Dumper(\%apireturn));

	return ( $apireturn{success}, $apireturn{error} );
}

sub order_info
{
	my ($self, $orderid) = @_;
    my %apireturn;

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&order_id=%s', 'OrderInfo', $nonce, $orderid);
	eval {
        $mech->post($tapi_url, [ 'method' => 'OrderInfo', 'nonce' => $nonce, 'order_id' => $orderid ]);

	    %apireturn = %{decode_json($mech->content())};
	    $self->log_debug('OrderInfo returned', Dumper(\%apireturn));
    };

	return {
	    pair	=> $apireturn{return}{$orderid}{pair},
	    type	=> $apireturn{return}{$orderid}{type},
	    units	=> ($apireturn{return}{$orderid}{start_amount}//0) - ($apireturn{return}{$orderid}{amount}//0),
	    remains	=> ($apireturn{return}{$orderid}{amount}//0),
	    rate	=> $apireturn{return}{$orderid}{rate},
	    status	=> $apireturn{return}{$orderid}{status} < 1 ? 'O' : 'C',
	}
}

sub _get_mech
{
	my $self = shift;
	my $data_pattern = shift;

	my $mech = WWW::Mechanize->new(agent => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36' );
	$mech->stack_depth(0);
	my $data = sprintf $data_pattern, @_;
	my $hash = hmac_sha512_hex($data, $self->api_secret);
	$mech->add_header('Key' => $self->api_key);
	$mech->add_header('Sign' => $hash);

	return $mech;
}

1;
