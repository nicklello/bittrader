package BitTrader::Exchange::CEXio;

use Modern::Perl;
use parent qw/BitTrader::Exchange::Base/;
use BitTrader::Exchange::WWW_CexIo_API;

use Data::Dumper;
use DateTime;
use Time::HiRes;

sub EXCHANGE_ID { 'CEXio' };

my $api;

sub initialise
{
    my $self = shift;
    
    $api = BitTrader::Exchange::WWW_CexIo_API->new(
        ApiUser => $self->api_user,
        ApiKey => $self->api_key,
        ApiSecret => $self->api_secret
    );
}

sub _get_trading_pairs
{
	my $self = shift;
    my @pairs;

    $self->log_trace('_get_trading_pairs');
	my $data = $api->_get('method' => 'currency_limits');
	$self->log_debug('https://cex.io/api/currency_limits', 'returned:', Dumper($data));

    @pairs = map { $_->{symbol1} . '/' . $_->{symbol2} } @{$data->{data}->{pairs}};

    $self->log_debug('_get_trading_pairs', 'Pairs:', @pairs);
    return @pairs;
}

sub estimated_trade_fee
{
	my ($self, $short_action, $trading_pair, $units_to_trade) = @_;
	my @ccys = split(/[\/_]/, $trading_pair);
	my $fee_rate = 0.03;

	return ($fee_rate * $units_to_trade, $ccys[0]);
}

sub fetch_exchange_rates
{
	my $self = shift;
	my @exchange_rates;

	foreach my $currency_pair (@{$self->trading_pairs}) {
		my $data = $api->ticker($currency_pair);
        $self->log_debug('CEX Ticker for', $currency_pair, 'returns', Dumper($data));
		if ( defined($data->{last}) ) {
			my $item;
			$item->{_EXCHANGE} = EXCHANGE_ID;
			$item->{ccypair} = uc($currency_pair);
			$item->{volume} = $data->{volume};
			$item->{sell} = $data->{ask};
			$item->{buy} = $data->{bid};
			$item->{high} = $data->{high};
			$item->{low} = $data->{low};
			$item->{timestamp} = DateTime->now()->epoch();
			push @exchange_rates, $item;
		}
		else {
			if ( $data->{error} ) {
				$self->log_info('Error fetching exchange rates for pair', $currency_pair, '-', $data->{error});
			} else {
				$self->log_info("Fetch exchange rates $currency_pair; Unexpected (last may be 0):", Dumper(\$data));
			}
		}
	}

	$self->last_exchange_update(DateTime->now());
	$self->exchange_rates(\@exchange_rates);

	return @exchange_rates;
}

sub fetch_balances
{
	my ($self) = @_;
    $self->log_debug('Fetching balances from API');

	my $nonce = $self->_get_nonce($self->EXCHANGE_ID);
	my $balances = $api->balance();
	my $element;

	$element->{_EXCHANGE} = EXCHANGE_ID;
	$element->{timestamp} = $balances->{timestamp};

	if ( $balances->{error} ) {
		$self->log_info("Error getting CEX.io balances", Dumper($balances));
	} else {
		my $ccydata;

		foreach my $key (keys %{$balances}) {
			next  unless length($key) == 3;

			$ccydata->{$key} = ($balances->{$key}->{available}//0); # + ($balances->{$key}->{orders}//0);
		}
		$element->{balances} = $ccydata;
	}

	return $element;
}


####################################################################################################################################
# New more Discrete methods
#

sub order_place
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_trade_at, $amount_to_buy, $units_to_trade, $last_price, $do_market_trade) = @_;
	$trading_pair =~ s/_/\//;
	my $_trade_amount  = lc($short_action) eq 'buy' ? $amount_to_buy : $units_to_trade;
    $rate_to_trade_at = sprintf "%.4f", $rate_to_trade_at unless $rate_to_trade_at == 0.1;

	my $order = $api->_post(
		'method' => 'place_order', 
		'pair' => uc($trading_pair),
		'type' => lc($short_action),
		'amount' => $_trade_amount,
		'price' => $rate_to_trade_at,
		);

	$self->log_debug('Trade returned', Dumper(\$order));

    my $success = $order->{error} ? 0 : 1;
    my $order_status;
    if ( $order->{complete}//0 == JSON::true ) {
        $order_status->{rate} = $order->{price};
        $order_status->{units} = $order->{amount};
        $order->{id} = undef;
    }

	return ( $success, $order->{id}, $order->{error}, $order_status );
}

#		($success, $error) = $self->exchange->order_cancel($orderid);
sub order_cancel
{
	my ($self, $orderid) = @_;
    return ( $orderid, 'FAILED' ) unless $orderid;

	my $cancelreturn = $api->cancel_order($orderid);

	$self->log_debug('CancelOrder returned', Dumper(\$cancelreturn));

	unless ( $cancelreturn ) {
		return ( 0, 'FAILED' );
	}

	return ( 1, undef );
}

sub order_info
{
	my ($self, $orderid, $pair) = @_;
	$pair =~ s/_/\//;

# NEW API
# Get order status using POST https://cex.io/api/get_order/
	my $orderinfo = $api->_post('method' => "get_order", 'id' => $orderid);

    $self->log_debug('order_info', $orderid, 'Returned:', Dumper($orderinfo));

	return {
		pair	=> $pair,
		type	=> $orderinfo->{type},
		units	=> $orderinfo->{amount},
		rate	=> $orderinfo->{price},
        remains => $orderinfo->{remains},
		status	=> $orderinfo->{status} ? 'C' : 'O',
	};
}

1;
