package BitTrader::Exchange::Bittrex;

use Modern::Perl;
use parent qw/BitTrader::Exchange::Base/;

use JSON qw/decode_json/;
use Data::Dumper;
use DateTime;
use Time::HiRes qw/tv_interval gettimeofday/;
use WWW::Mechanize;
use Digest::SHA qw/hmac_sha512_hex/;

sub EXCHANGE_ID { 'Bittrex' };

my $url_base = 'https://bittrex.com/api/v1.1/';
my @currency_pairs;


sub initialise
{
    shift->use_market_rates(0);     # Market trades are disabled internally by Bittrex despite being documented.
}

sub _get_trading_pairs
{
    my $self = shift;
    my @trading_pairs;
    my %data;

	my $uri = $url_base . 'public/getmarkets'; # ?apikey=' . $self->api_key . '&nonce=' . $self->_get_nonce();

	my $response = $self->useragent()->get($uri);

	if ( $response->is_success ) {
		eval {
			%data = %{decode_json($response->decoded_content)};
		     };
		if ( $@ ) {
			$self->log_info('Problem getting markets', $@);
		}
		else {
			if ( $data{success} && $data{result} ) {
				@trading_pairs = map {
					my $ccypair = uc($_->{MarketName});
					$ccypair =~ s/-/\//;

                    $ccypair;
				} @{$data{result}};
			}
			else {
				$self->log_info('Unexpected result', Dumper(\%data));
			}
		}
	}
	else {
		$self->log_info('Bitrex fetch markets failed!!', Dumper(\$@));
	}

    @currency_pairs = @trading_pairs;
    return @trading_pairs;
}

sub fetch_exchange_rates
{
	my $self = shift;
    my $time_start = [gettimeofday];
	$self->log_trace('fetch_exchange_rates Start');

	my @exchange_rates;
	my %data;

	my $uri = $url_base . 'public/getmarketsummaries'; # ?apikey=' . $self->api_key . '&nonce=' . $self->_get_nonce();

	my $response = $self->useragent()->get($uri);
	if ( $response->is_success ) {
        $self->log_debug($uri, 'returns', $response->decoded_content);
		eval {
			%data = %{decode_json($response->decoded_content)};
		     };
		if ( $@ ) {
			$self->log_info('Problem getting exchange rates', $@);
		}
		else {
			if ( $data{success} && $data{result} ) {
				map {
					my $ccypair = uc($_->{MarketName});
					$ccypair =~ s/-/\//;
	
					my $item;
					$item->{_EXCHANGE} = EXCHANGE_ID;
					$item->{ccypair} = $ccypair;
					$item->{volume} = sprintf "%.12f", $_->{BaseVolume}//0;
					$item->{sell} = sprintf "%.12f", $_->{Ask}//0;
					$item->{buy} = sprintf "%.12f", $_->{Bid}//0;
					$item->{high} = sprintf "%.12f", $_->{High}//0;
					$item->{low} = sprintf "%.12f", $_->{Low}//0;
					$item->{timestamp} = DateTime->now()->epoch(); # ($_->{TimeStamp});
	
					push @exchange_rates, $item;
				} @{$data{result}};
				@currency_pairs = map { $_->{MarketName} } @{$data{result}};
			}
			else {
				$self->log_info('Unexpected result', Dumper(\%data));
			}
		}
	}
	else {
		$self->log_info('Bitrex fetch exchange rates failed!!', Dumper(\$@));
	}

	$self->last_exchange_update(DateTime->now());
	$self->exchange_rates(\@exchange_rates);

    $self->log_debug('Exchange rates:', Dumper(\@exchange_rates));

	$self->log_trace('fetch_exchange_rates End', tv_interval($time_start, [gettimeofday]), 'secs');
	return @exchange_rates;
}

sub fetch_balances
{
	my $self = shift;
    my $time_start = [gettimeofday];
	$self->log_trace('fetch_balances Start');

	my $element;
    my %data;

	my $uri = $url_base . 'account/getbalances?apikey=' . $self->api_key . '&nonce=' . $self->_get_nonce();

	my $mech = $self->_get_mech($uri);
	eval { $mech->get($uri); };

	if ( !$@ ) {
		my %data = %{decode_json($mech->content())};

		if ( $data{success} && $data{result} ) {
			$element->{_EXCHANGE} = EXCHANGE_ID;
			$element->{timestamp} = DateTime->now()->epoch();
			map {
				$element->{balances}->{$_->{Currency}} = $_->{Balance};
			} @{$data{result}};
	    }
	}
    else {
        $self->log_debug('PROBLEM GETTING', $uri);
    }

	$self->log_trace('fetch_balances End', tv_interval($time_start, [gettimeofday]), 'secs');
	return $element;
}


####################################################################################################################################
# New more Discrete methods
#
sub order_place
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_trade_at, $amount_to_buy, $units_to_trade, $last_price, $do_market_trade) = @_;
    my $time_start = [gettimeofday];
	$self->log_trace('order_place Start');

	$trading_pair =~ s/[\/_]/-/;

	if ( ($rate_to_trade_at * 1) == (($last_price // 0) * 1) ) {
		return ( 0, undef, 'SAME PRICE' );
	}
	if ( ($rate_to_trade_at * 1) == 0 ) {
		return ( 0, undef, 'ZERO RATE' );
	}

    # Apparently BUY and SELL are reversed
    my $bittrex_action = lc($short_action) eq 'buy' ? 'sell' : 'buy';
#$bittrex_action = $short_action;
    my $trade_amount = lc($short_action) eq 'sell' ? $units_to_trade : $amount_to_sell;
    my $uri;
    if ( $do_market_trade ) {
        $uri = sprintf '%smarket/%smarket?apikey=%s&market=%s&quantity=%.8f&nonce=%d',
			   $url_base, $bittrex_action, $self->api_key, $trading_pair, $trade_amount, $self->_get_nonce();
    } else {
        $uri = sprintf '%smarket/%slimit?apikey=%s&market=%s&quantity=%.8f&rate=%.8f&nonce=%d',
			   $url_base, $bittrex_action, $self->api_key, $trading_pair, $trade_amount, $rate_to_trade_at, $self->_get_nonce();
    }

	my $mech = $self->_get_mech($uri);
	$mech->get($uri);

	if ( !$@ && $mech->content ) {
	    $self->log_debug('URI', $uri, 'order_place returned', $mech->content);

		my %data = %{decode_json($mech->content())};

    	$self->log_trace('order_place End', tv_interval($time_start, [gettimeofday]), 'secs');
		return ( $data{success}, $data{result}{uuid}, $data{message} );
	}
    elsif ( $@ ) {
        $self->log_trace('order_place failed:', $@);
    }

	$self->log_trace('order_place End', tv_interval($time_start, [gettimeofday]), 'secs');
 	return ( 0, undef, $@ );
}

#		($success, $error) = $self->exchange->order_cancel($orderid);
sub order_cancel
{
	my ($self, $orderid) = @_;
    my $time_start = [gettimeofday];
	$self->log_trace('order_cancel Start');

	my $uri = sprintf '%smarket/cancel?apikey=%s&uuid=%s&nonce=%d', $url_base, $self->api_key, $orderid, $self->_get_nonce();

	my $mech = $self->_get_mech($uri);
	$mech->get($uri);

    my %data;
	if ( !$@ && $mech->content ) {
	    $self->log_trace('URI', $uri, 'order_cancel returned', $mech->content);
		eval {
			%data = %{decode_json($mech->content)};
		     };
		if ( $@ ) {
			$self->log_info('Problem cancelling order:', $@);
		}
        else {
	        $self->log_trace('order_cancel End', tv_interval($time_start, [gettimeofday]), 'secs');
		    return ( $data{success}, $data{message} );
	    }
	}

	$self->log_trace('order_cancel End', tv_interval($time_start, [gettimeofday]), 'secs');
	return ( 0, $@ );
}

#		my $info = $self->exchange->order_info($orderid);
sub order_info
{
	my ($self, $orderid) = @_;
    my $time_start = [gettimeofday];
	$self->log_trace('order_info Start');

	my $uri = sprintf '%saccount/getorder?apikey=%s&uuid=%s&nonce=%d', $url_base, $self->api_key, $orderid, $self->_get_nonce();
	my $mech = $self->_get_mech($uri);
	$mech->get($uri);

    my %data;
	if ( !$@ && $mech->content ) {
	    $self->log_trace('URI', $uri, 'order_info returned', $mech->content);
		eval {
			%data = %{decode_json($mech->content)};
		     };
		if ( $@ ) {
			$self->log_info('Problem getting order info:', $@);
		}
    }

	$self->log_trace('order_info End', tv_interval($time_start, [gettimeofday]), 'secs');
	return {
		pair	=> $data{result}{Exchange},
		type	=> $data{result}{Type},
		units	=> $data{result}{Quantity} - $data{result}{QuantityRemaining},
		rate	=> $data{result}{Price},
		status	=> $data{result}{IsOpen} ? 'O' : 'C',
	};
}

sub _get_mech
{
	my $self = shift;
	my $data_pattern = shift;

	my $mech = WWW::Mechanize->new(agent => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36' );
	$mech->stack_depth(0);
	my $data = sprintf $data_pattern, @_;
	my $hash = hmac_sha512_hex($data, $self->api_secret);
	$mech->add_header('apisign' => $hash);

	return $mech;
}

1;
