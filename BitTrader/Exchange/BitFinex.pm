package BitTrader::Exchange::BitFinex;

use Modern::Perl;
use parent qw/BitTrader::Exchange::Base/;
use utf8;

use JSON qw/encode_json decode_json/;
use Data::Dumper;
use DateTime;
use Time::HiRes;

use Class::MethodMaker [ array => [ qw/pair_info/ ] ];

sub EXCHANGE_ID { 'BitFinex' };

my $apibase = 'https://api.bitfinex.com/v1';
my $info = "${apibase}/symbols";
my $ticker = "${apibase}/pubticker/";
my $orders = "${apibase}/book/";

my %trading_fee;
my $last_nonce;


sub _get_nonce
{
	my $self = shift;
	my $new_nonce = DateTime->now()->epoch();

	if ( $last_nonce && $last_nonce >= $new_nonce ) {
        $new_nonce = $last_nonce + 1;
	}

	$last_nonce = $new_nonce;

	return $new_nonce;
}

sub estimated_trade_fee
{
	return (0, '');
}

sub _get_trading_pairs
{
    my $self = shift;
    my @trading_pairs;
    my $data;

	my $response = $self->useragent()->get($info);
	if ( $response->is_success ) {
		eval {
			$data = decode_json($response->decoded_content);
		     };
		if ( $@ ) {
			$self->log_info("Problem decoding response", $@, $response->decoded_content);
		}
		else {
			$self->log_debug($info, 'returned:', $response->decoded_content);

            $self->pair_info(@$data);
		}
	}

    return $data;
}

sub fetch_exchange_rates
{
	my $self = shift;
	my @exchange_rates;
	my $data;

    foreach my $pair_lc (@{$self->pair_info}) {
	    my $response = $self->useragent()->get($ticker . $pair_lc);
	    if ( $response->is_success ) {
		    eval {
			    $data = decode_json($response->decoded_content);
		         };
		    if ( $@ ) {
			    $self->log_info("Problem decoding response", $@, $response->decoded_content);
		    }
		    else {
			    $self->log_debug($ticker . $pair_lc, 'returned:', $response->decoded_content);
                my $ccypair = uc(substr($pair_lc, 0, 3) . '/' . substr($pair_lc, 3, 3));
                my $item = {
                    _EXCHANGE       =>  EXCHANGE_ID,
                    ccypair         =>  $ccypair,
                    volume          =>  $data->{volume},
                    sell            =>  $data->{ask},
                    buy             =>  $data->{bid},
                    high            =>  $data->{high},
                    low             =>  $data->{low},
                    timestamp       =>  $data->{timestamp}
                };
				push @exchange_rates, $item;
		    }
	    }
	    else {
		    $self->log_info('BitFinex fetch exchange rates failed!!', Dumper(\$response));
	    }
	}

	$self->last_exchange_update(DateTime->now());
	$self->exchange_rates([ @exchange_rates ]);

	return @exchange_rates;
}


use WWW::Mechanize;
use LWP::Debug qw(+);
use Digest::SHA qw/hmac_sha384_hex/;

sub fetch_balances
{
	my ($self) = @_;
	my $mech = WWW::Mechanize->new();
	$mech->stack_depth(0);
	$mech->agent_alias('Windows IE 6');
	my $nonce = $self->_get_nonce();
	my $url = "${apibase}/balances";
	my $data = { request => '/v1/balances', nonce => $nonce };
	my $hash = hmac_sha384_hex(encode_json($data), $self->api_secret);
	$mech->add_header('X-BFX-APIKEY' => $self->api_key);
	$mech->add_header('X-BFX-PAYLOAD' => encode_json($data) );
	$mech->add_header('X-BFX-SIGNATURE' => $hash);

	my $element;
	eval {
        $mech->post($url, []);
	    $self->log_debug("$url returned:", $mech->content);
	    my $apireturn = decode_json($mech->content());
$self->log_debug(Dumper($apireturn));

	    $element->{_EXCHANGE} = EXCHANGE_ID;
#	    $element->{timestamp} = $apireturn{return}{server_time};
#	    $element->{balances} = $apireturn{return}{funds};
    };
    if ( $@ ) {
        $self->log_info('ERROR: Fetch_balances:', $@);
    }
exit;

	return $element;
}


sub _get_mech
{
	my $self = shift;
	my $data_pattern = shift;

	my $mech = WWW::Mechanize->new(agent => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36' );
	$mech->stack_depth(0);
	my $data = sprintf $data_pattern, @_;
	my $hash = hmac_sha384_hex($data, $self->api_secret);
	$mech->add_header('Key' => $self->api_key);
	$mech->add_header('Sign' => $hash);

	return $mech;
}

1;
__END__
#include "bitfinex.h"
#include "parameters.h"
#include "utils/restapi.h"
#include "utils/base64.h"
#include "hex_str.hpp"

#include "jansson.h"
#include "openssl/sha.h"
#include "openssl/hmac.h"
#include <unistd.h>
#include <sstream>
#include <math.h>
#include <sys/time.h>

namespace Bitfinex {

static RestApi& queryHandle(Parameters &params)
{
  static RestApi query ("https://api.bitfinex.com",
                        params.cacert.c_str(), *params.logFile);
  return query;
}

static json_t* checkResponse(std::ostream &logFile, json_t *root)
{
  auto msg = json_object_get(root, "message");
  if (msg)
    logFile << "<Bitfinex> Error with response: "
            << json_string_value(msg) << '\n';

  return root;
}

quote_t getQuote(Parameters &params)
{
  auto &exchange = queryHandle(params);
  json_t *root = exchange.getRequest("/v1/ticker/btcusd");

  const char *quote = json_string_value(json_object_get(root, "bid"));
  double bidValue = quote ? std::stod(quote) : 0.0;

  quote = json_string_value(json_object_get(root, "ask"));
  double askValue = quote ? std::stod(quote) : 0.0;

  json_decref(root);
  return std::make_pair(bidValue, askValue);
}

double getAvail(Parameters& params, std::string currency)
{
  json_t *root = authRequest(params, "/v1/balances", "");

  double availability = 0.0;
  for (size_t i = json_array_size(root); i--;)
  {
    const char *each_type, *each_currency, *each_amount;
    json_error_t err;
    int unpack_fail = json_unpack_ex (json_array_get(root, i),
                                      &err, 0,
                                      "{s:s, s:s, s:s}",
                                      "type", &each_type,
                                      "currency", &each_currency,
                                      "amount", &each_amount);
    if (unpack_fail)
    {
      *params.logFile << "<Bitfinex> Error with JSON: "
                      << err.text << std::endl;
    }
    else if (each_type == std::string("trading") && each_currency == currency)
    {
      availability = std::stod(each_amount);
      break;
    }
  }
  json_decref(root);
  return availability;
}

std::string sendLongOrder(Parameters& params, std::string direction, double quantity, double price)
{
  return sendOrder(params, direction, quantity, price);
}

std::string sendShortOrder(Parameters& params, std::string direction, double quantity, double price)
{
  return sendOrder(params, direction, quantity, price);
}

std::string sendOrder(Parameters& params, std::string direction, double quantity, double price)
{
  *params.logFile << "<Bitfinex> Trying to send a \"" << direction << "\" limit order: " << quantity << "@$" << price << "..." << std::endl;
  std::ostringstream oss;
  oss << "\"symbol\":\"btcusd\", \"amount\":\"" << quantity << "\", \"price\":\"" << price << "\", \"exchange\":\"bitfinex\", \"side\":\"" << direction << "\", \"type\":\"limit\"";
  std::string options = oss.str();
  json_t *root = authRequest(params, "/v1/order/new", options);
  auto orderId = std::to_string(json_integer_value(json_object_get(root, "order_id")));
  *params.logFile << "<Bitfinex> Done (order ID: " << orderId << ")\n" << std::endl;
  json_decref(root);
  return orderId;
}

bool isOrderComplete(Parameters& params, std::string orderId)
{
  if (orderId == "0") return true;

  auto options =  "\"order_id\":" + orderId;
  json_t *root = authRequest(params, "/v1/order/status", options);
  bool isComplete = json_is_false(json_object_get(root, "is_live"));
  json_decref(root);
  return isComplete;
}

double getActivePos(Parameters& params)
{
  json_t *root = authRequest(params, "/v1/positions", "");
  double position;
  if (json_array_size(root) == 0)
  {
    *params.logFile << "<Bitfinex> WARNING: BTC position not available, return 0.0" << std::endl;
    position = 0.0;
  }
  else
  {
    position = atof(json_string_value(json_object_get(json_array_get(root, 0), "amount")));
  }
  json_decref(root);
  return position;
}

double getLimitPrice(Parameters& params, double volume, bool isBid)
{
  auto &exchange  = queryHandle(params);
  json_t *root    = exchange.getRequest("/v1/book/btcusd");
  json_t *bidask  = json_object_get(root, isBid ? "bids" : "asks");

  *params.logFile << "<Bitfinex> Looking for a limit price to fill " << fabs(volume) << " BTC..." << std::endl;
  double tmpVol = 0.0;
  double p = 0.0;
  double v;

    // loop on volume
  for (int i = 0, n = json_array_size(bidask); i < n; ++i)
  {
    p = atof(json_string_value(json_object_get(json_array_get(bidask, i), "price")));
    v = atof(json_string_value(json_object_get(json_array_get(bidask, i), "amount")));
    *params.logFile << "<Bitfinex> order book: " << v << "@$" << p << std::endl;
    tmpVol += v;
    if (tmpVol >= fabs(volume) * params.orderBookFactor) break;
  }

  json_decref(root);
  return p;
}

json_t* authRequest(Parameters &params, std::string request, std::string options)
{
  using namespace std;

  struct timeval tv;
  gettimeofday(&tv, NULL);
  unsigned long long nonce = (tv.tv_sec * 1000.0) + (tv.tv_usec * 0.001) + 0.5;

  string payload = "{\"request\":\"" + request +
                   "\",\"nonce\":\"" + to_string(nonce);
  if (options.empty())
  {
    payload += "\"}";
  }
  else
  {
    payload += "\", " + options + "}";
  }

  payload = base64_encode(reinterpret_cast<const uint8_t *>(payload.c_str()), payload.length());

  // signature
  uint8_t *digest = HMAC (EVP_sha384(),
                          params.bitfinexSecret.c_str(), params.bitfinexSecret.length(),
                          reinterpret_cast<const uint8_t *> (payload.data()), payload.size(),
                          NULL, NULL);

  array<string, 3> headers
  {
    "X-BFX-APIKEY:"     + params.bitfinexApi,
    "X-BFX-SIGNATURE:"  + hex_str(digest, digest + SHA384_DIGEST_LENGTH),
    "X-BFX-PAYLOAD:"    + payload,
  };
  auto &exchange = queryHandle(params);
  auto root = exchange.postRequest (request,
                                    make_slist(begin(headers), end(headers)));
  return checkResponse(*params.logFile, root);
}

}



####################################################################################################################################
# New more Discrete methods
#
sub Zorder_place
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_trade_at, $amount_to_buy, $units_to_trade, $last_price, $do_market_trade) = @_;
	$trading_pair =~ s/\//_/;
    $rate_to_trade_at = sprintf("%0.3f", $rate_to_trade_at);

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&pair=%s&type=%s&rate=%s&amount=%s',
			     'Trade', $nonce, lc($trading_pair), lc($short_action), $rate_to_trade_at, $units_to_trade);
    $self->log_debug('order_place', $tapi_url, 'method' => 'Trade', 'nonce'  => $nonce, 'pair'   => lc($trading_pair), 'type'   => lc($short_action), 'rate'   => $rate_to_trade_at, 'amount' => $units_to_trade);
	eval { $mech->post($tapi_url,
                        [	'method' => 'Trade',
			    	        'nonce'  => $nonce,
			    	        'pair'   => lc($trading_pair),
			    	        'type'   => lc($short_action),
			    	        'rate'   => $rate_to_trade_at,
			    	        'amount' => $units_to_trade
			            ]
    ); };
	if ( $@ ) {
		$self->log_debug('ERROR Placing order', $@);
		return ( 0, undef, 'MECHERROR' );
	}

	my %apireturn;
	eval { %apireturn = %{decode_json($mech->content())}; };
	if ( $@ ) {
		$self->log_info('Unexpected non-json response from Trade call', $mech->content());
		$self->log_info('Assuming trade failed');
		return ( 0, undef, $mech->content() );
	}

	$self->log_debug('Trade returned', Dumper(\%apireturn));

    my $order_status;
  eval {
    if ( $apireturn{success} && !$apireturn{return}{order_id} ) {
        my @trades = $self->_get_trade_history();
        my @wanted_trades;
        my $valid_trades_count;
        my $units_traded = 0.000000;
        my $rate_traded_at = 0.000000;
        foreach my $trade (sort { $b->{timestamp} <=> $a->{timestamp} } @trades) {
            if ( !$valid_trades_count && $trade->{type} ne $short_action && $trade->{pair} ne lc($trading_pair) ) {
$self->log_debug('SKIPPING', 'Timestamp', $trade->{timestamp}, $trade->{type}, $trade->{pair});
                next;
            }
            last    unless $trade->{type} eq $short_action && $trade->{pair} eq lc($trading_pair);

$self->log_debug('Timestamp', $trade->{timestamp}, $trade->{type}, $trade->{pair}, 'Amount', $trade->{amount}, 'Rate', $trade->{rate});
            $units_traded += $trade->{amount};
            $rate_traded_at = $trade->{rate};
$self->log_debug('Amount now', $units_traded, '; Rate traded at now', $rate_traded_at);
            $valid_trades_count++;
        }
$self->log_debug('Used', $valid_trades_count, 'trades');
        $order_status->{units} = $units_traded;
        $order_status->{rate} = $rate_traded_at;
    }
  };
  if ( $@ ) {
      $self->log_debug($@);
  }

	return ( $apireturn{success}, $apireturn{return}{order_id}, $apireturn{error}, $order_status );
}

sub Z_get_trade_history
{
    my ($self) = @_;

    $self->log_trace('_get_trade_history');

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d', 'TradeHistory', $nonce);
	$mech->post($tapi_url, [ 'method' => 'TradeHistory', 'nonce'  => $nonce ]);

	my %apireturn = %{decode_json($mech->content())};

    my @toreturn = map { $apireturn{return}{$_} } keys %{$apireturn{return}};

    return ( sort { $a->{timestamp} <=> $b->{timestamp} } @toreturn );
}

#		($success, $error) = $self->exchange->order_cancel($orderid);
sub Zorder_cancel
{
	my ($self, $orderid) = @_;

    $self->log_debug('Cancelling order id', $orderid);

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&order_id=%s', 'CancelOrder', $nonce, $orderid);
	$mech->post($tapi_url, [ 'method' => 'CancelOrder', 'nonce'  => $nonce, 'order_id' => $orderid ]);

	my %apireturn = %{decode_json($mech->content())};
	$self->log_debug('CancelOrder returned', Dumper(\%apireturn));

	return ( $apireturn{success}, $apireturn{error} );
}

sub Zorder_info
{
	my ($self, $orderid) = @_;
    my %apireturn;

	my $nonce = $self->_get_nonce();
	my $mech = $self->_get_mech('method=%s&nonce=%d&order_id=%s', 'OrderInfo', $nonce, $orderid);
	eval {
        $mech->post($tapi_url, [ 'method' => 'OrderInfo', 'nonce' => $nonce, 'order_id' => $orderid ]);

	    %apireturn = %{decode_json($mech->content())};
	    $self->log_debug('OrderInfo returned', Dumper(\%apireturn));
    };

	return {
	    pair	=> $apireturn{return}{$orderid}{pair},
	    type	=> $apireturn{return}{$orderid}{type},
	    units	=> ($apireturn{return}{$orderid}{start_amount}//0) - ($apireturn{return}{$orderid}{amount}//0),
	    remains	=> ($apireturn{return}{$orderid}{amount}//0),
	    rate	=> $apireturn{return}{$orderid}{rate},
	    status	=> $apireturn{return}{$orderid}{status} < 1 ? 'O' : 'C',
	}
}

