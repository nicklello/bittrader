package BitTrader::Exchange::Base;

use Modern::Perl;
use LWP::UserAgent;
use DateTime;

use parent qw/BitTrader::Base/;

require Compress::Zlib;
require HTTP::Message;
use Data::Dumper;
use Time::HiRes qw/tv_interval gettimeofday/;

use Class::MethodMaker
	[	scalar		=> [ qw/bittrader api_key api_secret api_user last_exchange_update last_trade_update arbitrage_routes balances exchange_rates orders trader use_market_rates/ ],
        array       => [ qw/trading_pairs/ ],
	];

my $lwp_useragent;

sub new
{
	my $class = shift;
	my $self = bless {}, $class;

    $self->bittrader(shift);
	$self->api_key(shift);
	$self->api_secret(shift);
	$self->api_user(shift);

	if ( $self->can('initialise') ) {
		$self->initialise(@_);
	}
	if ( $self->can('_get_trading_pairs') ) {
        my @trading_pairs = grep { my @ccys = split(/[\/_]/, $_);

		                           !($self->config->dont_buy (undef, $self, $ccys[0]) || $self->config->dont_sell(undef, $self, $ccys[1]))
		                        && !($self->config->dont_sell(undef, $self, $ccys[0]) || $self->config->dont_buy (undef, $self, $ccys[1]))

                            } map { s/_/\//; uc($_); } $self->_get_trading_pairs();

        $self->log_debug('Trading pairs', @trading_pairs);
        $self->trading_pairs_push(@trading_pairs);
    }
	if ( $self->can('load_trading_fee') ) {
		$self->load_trading_fee();
    }
    if ( $self->trader && $self->trader->allow_arbitrage ) {
        $self->_generate_routes;
    }

	return $self;
}

sub _get_trading_pairs
{
    my $self = shift;

    die "$self -- _get_trading_pairs needs to be defined";
}

sub currency_balance
{
	my ($self, $currency) = @_;
	my $balances;

	if ( $self->balances ) {
		$balances = $self->balances;
	}
    unless ( $balances->{lc($currency)} || $balances->{uc($currency)} ) {
		my $data = $self->fetch_balances();
		$balances = $data->{balances};
		$self->balances($balances);
	}

	return $balances->{lc($currency)} // $balances->{uc($currency)};
}

sub useragent()
{
	if ( ! $lwp_useragent ) {
		$lwp_useragent = LWP::UserAgent->new;
		$lwp_useragent->timeout(10);
		$lwp_useragent->default_header('Accept-Encoding' => HTTP::Message::decodable); # Try to get gzipped data
	}
	return $lwp_useragent;
}

sub estimated_trade_fee
{
	my ($self, $short_action, $trading_pair, $units_to_trade) = @_;
	my @ccys = split(/[\/_]/, $trading_pair);

	return ($self->config->config->{percentage_fee} * $units_to_trade, $ccys[0]);
}

sub EXCHANGE_ID
{
	die "EXCHANGE_ID must be defined in subclass";
}

sub fetch_exchange_rates
{
	my $self = shift;

	die "fetch_exchange_rates must be defined in subclass";
}

sub make_trade
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_sell_at, $amount_to_buy, $units_to_trade) = @_;

	warn "make_trade must be defined in exchange subclass";

	return undef;
}

sub _get_nonce
{
	my $self = shift;

# Nonce must be a 10-digit incrementing number
	return sprintf "%10.10s", $self->get_next_nonce();
}

sub get_next_nonce
{
	my $self = shift;
	my $datafile = './last_used_nonce';
	my $nonce;

	if ( -e $datafile ) {
		open(NONCE, "<$datafile");
		my @data = <NONCE>;
		close(NONCE);
		$nonce = $data[0];
		chomp $nonce;
	}

	else {
		$nonce = DateTime->now()->epoch();
	}

	$nonce++;

	open(NONCE, ">$datafile");
	print NONCE "$nonce\n";
	close(NONCE);

	return $nonce;
}

# 
# route calculations
# ------------------
# Convert list of trading pairs to list of arrays ccy[0]/ccy[1]
# for each new ccy0 find all pairs where the pair can be converted back to source ccy
sub _generate_routes
{
    my $self = shift;

    my @work = map {
		my @ccys = split(/[\/_]/, $_);
		{
			ccy1 => $ccys[0],
			ccy2 => $ccys[1],
			pair => $_,
		}
	} grep {
		my @ccys = split(/[\/_]/, $_);

		   !($self->config->dont_buy (undef, $self, $ccys[0]) || $self->config->dont_sell(undef, $self, $ccys[1]))
		&& !($self->config->dont_sell(undef, $self, $ccys[0]) || $self->config->dont_buy (undef, $self, $ccys[1]))

    } @{$self->trading_pairs};

	my @routes;
	foreach my $pairinfo (grep { $self->config->is_arbitrage_ccy($_->{ccy1}) || $self->config->is_arbitrage_ccy($_->{ccy2}) } @work) {
		my @possibles =  map {
				my $short_action = ($_->{ccy1} eq $pairinfo->{ccy1}) ? 'sell' : 'buy';
				my $am_selling = ($short_action eq 'sell') ? 1 : undef;
			    {
					ccy00   => $pairinfo->{ccy1},
					act01	=> $short_action,
					pair1	=> $_->{pair},
					ccy01	=> ($am_selling) ? $_->{ccy2} : $_->{ccy1},
			    }
			} grep { ($_->{ccy1} eq $pairinfo->{ccy1} && $self->config->is_arbitrage_ccy($_->{ccy1}))
			      || ($_->{ccy2} eq $pairinfo->{ccy1} && $self->config->is_arbitrage_ccy($_->{ccy2}))
			} @work;

		my @possibl2s;
		foreach my $possible (@possibles) {
			push @possibl2s, map {
				my $short_action = ($_->{ccy1} eq $possible->{ccy01}) ? 'sell' : 'buy';
				my $am_selling = ($short_action eq 'sell') ? 1 : undef;
				{
					ccy00	=> $possible->{ccy00},
					act01	=> $possible->{act01},
					pair1	=> $possible->{pair1},
					ccy01	=> $possible->{ccy01},
					act02	=> $short_action,
					pair2	=> $_->{pair},
					ccy02	=> ($am_selling) ? $_->{ccy2} : $_->{ccy1},
				}
			}  grep { ( $_->{ccy1} eq $possible->{ccy01} || $_->{ccy2} eq $possible->{ccy01} ) && $_->{pair} ne $possible->{pair1}
			} @work;
		}

		foreach my $possible (@possibl2s) {
			my @newroutes = map {
				my $short_action = ($_->{ccy1} eq $possible->{ccy02}) ? 'sell' : 'buy';
				my $am_selling = ($short_action eq 'sell') ? 1 : undef;
				{
                    routes      => 3,
					exchange    => $self->EXCHANGE_ID,
					ccy	=> $possible->{ccy00},
		     		route1 => {
                        pair   => $possible->{pair1},
						action => $possible->{act01},
					},
					route2 => {
                        pair   => $possible->{pair2},
						action => $possible->{act02},
					},
					route3 => {
                        pair   => $_->{pair},  
						action => $short_action,      
					},
				}
			    }  grep { ( $_->{ccy1} eq $possible->{ccy02} && $_->{ccy2} eq $possible->{ccy00} )
                       || ( $_->{ccy2} eq $possible->{ccy02} && $_->{ccy1} eq $possible->{ccy00} )
			    } @work;

            push @routes, @newroutes;
		}


		my @possibl3s;
		foreach my $possible (@possibl2s) {
			push @possibl3s, map {
				my $short_action = ($_->{ccy1} eq $possible->{ccy02}) ? 'sell' : 'buy';
				my $am_selling = ($short_action eq 'sell') ? 1 : undef;
				{
					ccy00	=> $possible->{ccy00},
					act01	=> $possible->{act01},
					pair1	=> $possible->{pair1},
					ccy01	=> $possible->{ccy01},
					act02	=> $possible->{act02},
					pair2	=> $possible->{pair2},
					ccy02	=> $possible->{ccy02},
					act03	=> $short_action,
					pair3	=> $_->{pair},
					ccy03	=> ($am_selling) ? $_->{ccy2} : $_->{ccy1},
				}
			}  grep { ( $_->{ccy1} eq $possible->{ccy02} || $_->{ccy2} eq $possible->{ccy02} ) && $_->{pair} ne $possible->{pair1}
			} @work;
		}

		foreach my $possible (@possibl3s) {
			my @newroutes = map {
				my $short_action = ($_->{ccy1} eq $possible->{ccy03}) ? 'sell' : 'buy';
				my $am_selling = ($short_action eq 'sell') ? 1 : undef;
				{
                    routes      => 4,
					exchange    => $self->EXCHANGE_ID,
					ccy	=> $possible->{ccy00},
		     		route1 => {
                        pair   => $possible->{pair1},
						action => $possible->{act01},
					},
					route2 => {
                        pair   => $possible->{pair2},
						action => $possible->{act02},
					},
					route3 => {
                        pair   => $possible->{pair3},
						action => $possible->{act03},
					},
					route4 => {
                        pair   => $_->{pair},  
						action => $short_action,      
					},
				}
			    }  grep { ( $_->{ccy1} eq $possible->{ccy03} && $_->{ccy2} eq $possible->{ccy00} )
                       || ( $_->{ccy2} eq $possible->{ccy03} && $_->{ccy1} eq $possible->{ccy00} )
			    } @work;

            push @routes, @newroutes;
		}

	}

    push @routes, map {
        {
            routes      => 2,
            exchange    => $self->EXCHANGE_ID,
            ccy         => $_->{ccy1},
            route1  =>  {
                            pair    => $_->{pair},
                            action  => 'sell',
            },
            route2  =>  {
                            pair    => $_->{pair},
                            action  => 'buy',
            },
        }
	} grep { $self->config->is_arbitrage_ccy($_->{ccy1}) } @work;
    push @routes, map {
        {
            routes      => 2,
            exchange    => $self->EXCHANGE_ID,
            ccy         => $_->{ccy2},
            route1  =>  {
                            pair    => $_->{pair},
                            action  => 'buy',
            },
            route2  =>  {
                            pair    => $_->{pair},
                            action  => 'sell',
            },
        }
	} grep { $self->config->is_arbitrage_ccy($_->{ccy2}) } @work;

# Duplicates removal
	my $cleanup;
	map {
		my $key = $_->{ccy} . '_' .  $_->{route1}->{pair} . '_' . $_->{route2}->{pair} . '_' . ($_->{route3}->{pair}//'0') . '_' . ($_->{route4}->{pair}//'0');

		$cleanup->{$key} = $_;
	} @routes;
	@routes = map { $cleanup->{$_} } keys %{$cleanup};

	$self->log_info(scalar @routes, 'Routes');

	$self->arbitrage_routes(\@routes);

	return \@routes;
}

#
# update routes with pricing information
# --------------------------------------
sub update_arbitrage_routes
{
	my ($self) = @_;

    my $time_start = [gettimeofday];
	$self->log_debug('_calculate_routes Start');

    if ( $self->trader && !$self->trader->allow_arbitrage ) {
        $self->log_debug('Arbitrage disabled; not calculating routes');
        return ();
    }

    my @exchange_rates = $self->fetch_exchange_rates();

    my @routes = @{$self->arbitrage_routes};
    map {
            my $amount = 100;
            my $node_no = 1;

            $_->{is_profitable} = 0;

            foreach my $node ($_->{route1}, $_->{route2}, $_->{route3}, $_->{route4}) {

                $self->log_debug('ERROR: #routes < 2 !!')       if ($_->{routes}//0) < 2;

                next        if $node_no > $_->{routes} || $amount < 0.000000;

		        my ($best_sell, $second_best_sell) = $self->_get_best_sell_price($node->{pair}, \@exchange_rates);
		        my ($best_buy, $second_best_buy)   = $self->_get_best_buy_price ($node->{pair}, \@exchange_rates);
                my $rate_sell = $second_best_sell;
                my $rate_buy =  $second_best_buy;
                my ($pair_main_currency, undef) = split(/[\/_]/, $node->{pair});

                if ( ($node->{action} eq 'sell' && $rate_sell) || ($node->{action} eq 'buy' && $rate_buy) ) {
		            my ($fee_amount, $fee_ccy) = $self->estimated_trade_fee($node->{action},
                                                                            $node->{pair},
                                                                            ($node->{action} eq 'sell')
                                                                                ? $amount
                                                                                : $amount / $rate_buy
                                                                           );
                    $node->{estimated_fee} = $self->format_short_ccy($fee_amount) . " ${fee_ccy}";

                    if ( ( $node->{action} eq 'sell' && $fee_ccy eq $pair_main_currency ) || ( $node->{action} eq 'buy' && $fee_ccy ne $pair_main_currency ) ) {
                        $amount = $amount - $fee_amount;
                    }

                    $node->{rate} = ($node->{action} eq 'sell') ? $rate_sell : $rate_buy;

                    if ( $node->{rate} ) {

                        $amount = ($node->{action} eq 'sell') ? $amount * $rate_sell : $amount / $rate_buy;

                        if ( ( $node->{action} eq 'sell' && $fee_ccy ne $pair_main_currency ) || ( $node->{action} eq 'buy' && $fee_ccy eq $pair_main_currency ) ) {
                            $amount = $amount - $fee_amount;
                        }

                        $node->{result} = $amount;
                        $_->{result} = $amount;
                    }

                    $node_no++;
                }
                else {
                    last;
                }
            }
        } @routes;

	$self->arbitrage_routes(\@routes);
    map { $self->log_debug('Route:', '1:', $_->{route1}->{action}, $_->{route1}->{pair},
                                     '2:', $_->{route2}->{action}, $_->{route2}->{pair},
                                     '3:', $_->{route3}->{action}, $_->{route3}->{pair},
                                     '4:', $_->{route4}->{action}, $_->{route4}->{pair})
        } @routes;

	$self->log_debug('_calculate_routes End', tv_interval($time_start, [gettimeofday]), 'secs');
	return \@routes;
}

sub _get_best_sell_price
{
	my ($self, $trading_pair, $exchange_rates) = @_;
    my ($exchange_rate) = grep { $_->{ccypair} eq $trading_pair } @$exchange_rates;

    my $best = 0;
    my $second_best = 0;
    if ( $exchange_rate ) {
        $best = $exchange_rate->{buy} * (1 - $self->config->config->{exchange_rate_lag});
        $second_best = $best * (1 - $self->config->config->{exchange_rate_lag});
    }

    return ($best, $second_best);
}

sub _get_best_buy_price
{
	my ($self, $trading_pair, $exchange_rates) = @_;
    my ($exchange_rate) = grep { $_->{ccypair} eq $trading_pair } @$exchange_rates;

    my $best = 0;
    my $second_best = 0;
    if ( $exchange_rate ) {
        $best = $exchange_rate->{sell} * (1 + $self->config->config->{exchange_rate_lag});
        $second_best = $best * (1 + $self->config->config->{exchange_rate_lag});
    }

    return ($best, $second_best);
}

sub _cancel_expired_orders
{
    my ($self, $datastore) = @_;
    my @orders;
    $self->log_debug('_cancel_expired_orders');

    eval {
        @orders = $datastore->fetch_open_orders($self->EXCHANGE_ID, 'expired');
    };
    map {
	    $self->log_debug('Cancelling order', $_->{order_id});
	    my ($success, $error) = $self->order_cancel($_->{order_id});
	    if ( $success ) {
            $self->balances(undef); #Cause exchange to be polled for balances next time they're needed
        } else {
	        $self->log_debug('Order_cancel failed!!! Error:', $error);
	    }
    } @orders;

    $self->log_debug('End _cancel_expired_orders');
}

sub _refresh_order_status_info
{
    my ($self, $datastores) = @_;
    my $datastore = $$datastores[0];
    $self->log_debug('_refresh_order_status');
    my @orders;

    eval {
        @orders = $datastore->fetch_open_orders($self->EXCHANGE_ID);
    };
    map {
        $self->log_debug('Checking order status', $_->{order_id}, 'pair', $_->{trading_pair});
        my $order_status = $self->order_info($_->{order_id}, $_->{trading_pair});
        $self->log_debug('Order Status info', Dumper(\$order_status));

        $order_status->{status} = 'O'  if !$order_status->{status};
        foreach my $ds (@$datastores) {
            $ds->update_order($_->{order_id},
                            defined($order_status->{remains})
                                ? $order_status->{remains}
                                : ($_->{amount} - $order_status->{units}),
                            $order_status->{status});
            $ds->audit_message($self->EXCHANGE_ID, 'Order', $_->{order_id}, $_->{trade_type}, $_->{trading_pair}, '@', $_->{price}, '; Status', $order_status->{status}, '; Amount', $_->{amount}, '; Units', $order_status->{units}, '; Remains', $order_status->{remains});
        }

        if ( $order_status->{status} eq 'C' && $order_status->{units} ) {

            $self->log_debug('_refresh_order_status: Order completed/closed: Units successfully traded:', $order_status->{units});
            my ($fee_amount, $fee_ccy) = $self->estimated_trade_fee($_->{trade_type}, $_->{trading_pair}, $order_status->{units});
            my $price_wanted = (lc($_->{trade_type}) eq 'buy')
                       ? $order_status->{rate} * ( 1 + $self->config->config->{percentage_profit_wanted} )
                       : $order_status->{rate} * ( 1 - $self->config->config->{percentage_profit_wanted} )
			                        ;

            $self->log_debug('_refresh_order_status: Recording successful trade');
            foreach my $ds (@$datastores) {
                $ds->record_successful_trade(
                                            $_->{order_origin},
                                            $self->EXCHANGE_ID,
                                            $_->{trading_pair},
                                            $_->{trade_type},
                                            $order_status->{rate},
                                            $order_status->{units},
                                            $fee_amount,
                                            $fee_ccy,
                                            $_->{order_id},
                                            $self->estimated_trade_value_in_btc($_->{trading_pair}, $_->{trade_type}, $order_status->{units}),
                                            $price_wanted
                                            );
            }

            $self->log_debug('_refresh_order_status: Calling order completion trigger:', ref($self->trader));
            $self->trader->order_completion_trigger($datastores,
                                                    $self->EXCHANGE_ID,
                                                    $_->{trading_pair},
                                                    $_->{trade_type},
                                                    $order_status->{rate},
                                                    $order_status->{units},
                                                    $price_wanted
                                                    );
            }

    } @orders;

    $self->log_debug('End _refresh_order_status');
}

sub estimated_trade_value_in_btc
{
    my ($self, $trading_pair, $trade_type, $units) = @_;
    my ($unit_ccy, $fiat_ccy) = split(/[_\/-]/, $trading_pair);
if ( !$unit_ccy ) {
	$self->log_info('WARNING: Split of', $trading_pair, 'failed');
	$unit_ccy='BTC';
}
    my $estimate = 0;

    $self->log_debug('estimated_btc_trade_value: pair', $trading_pair, 'type', $trade_type, 'units', $units);

    if ( $unit_ccy eq 'BTC' || $unit_ccy eq 'XXBT' ) {
        $estimate = $units;
    }
    elsif ( $self->exchange_rates ) {
        eval {
            my @possible = grep { $_->{ccypair} =~ /${unit_ccy}.BTC/ || $_->{ccypair} =~ /${unit_ccy}.XXBT/ } @{$self->exchange_rates};
            if ( scalar(@possible) == 1 ) {
                $estimate = $units * $possible[0]->{sell};
            }

            @possible = grep { $_->{ccypair} =~ /BTC.${unit_ccy}/ || $_->{ccypair} =~ /XXBT.${unit_ccy}/ } @{$self->exchange_rates};
            if ( scalar(@possible) == 1 ) {
                $estimate = $units / $possible[0]->{buy};
            }
        };
    }
    unless ( $estimate ) {
        $estimate = -1;
    }

    $self->log_debug('Estimated BTC value:', $estimate);
    return $estimate;
}

1;
