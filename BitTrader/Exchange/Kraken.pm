package BitTrader::Exchange::Kraken;

use Modern::Perl;
use parent qw/BitTrader::Exchange::Base/;
use Finance::Bank::Kraken;

use utf8;

use JSON qw/decode_json/;
use Data::Dumper;
use DateTime;
use Time::HiRes qw/usleep/;

sub EXCHANGE_ID { 'Kraken' };

my $kraken;
my $trading_pairs_list;
my %fee_data;
my %decimals;

sub initialise
{
	my $self = shift;

	$kraken = new Finance::Bank::Kraken;
	$kraken->key($self->api_key);
	$kraken->secret($self->api_secret);

    $self->use_market_rates(0);
}

sub _get_trading_pairs
{
    my $self = shift;
	my $data = decode_json($kraken->call(Public, 'AssetPairs'));
	my $error = $data->{error};
	my $result = $data->{result};

	if ( @$error ) {
		$self->log_info('Kraken fetch trading pairs failed:', Dumper($error));
		die;
	}
	$trading_pairs_list = join(',', keys %{$result});

    map {
        my $node = $$result{$_};
        my $native = $_;
        my $massaged = substr($_, 0, 4) . '/' . substr($_, 4, 4);
        my $fee_node = $node->{fees}->[0];
        my $fee_percent = @$fee_node[1];

        $fee_data{$native}   = $fee_percent;
        $fee_data{$massaged} = $fee_percent;
        $decimals{$native}   = $node->{pair_decimals};
        $decimals{$massaged} = $node->{pair_decimals};

    } keys %{$result};

	return map { substr($_, 0, 4) . '/' . substr($_, 4, 4) } keys %{$result};
}

sub estimated_trade_fee
{
	my ($self, $short_action, $trading_pair, $units_to_trade) = @_;
	my @ccys = split(/[\/_]/, $trading_pair);
	my $fee_rate = ($fee_data{$trading_pair} // 0.33) / 100;

	return ($fee_rate * $units_to_trade, $ccys[0]);
}

sub fetch_exchange_rates
{
	my $self = shift;
	my @exchange_rates;

	eval {
		my $data = decode_json($kraken->call(Public, 'Ticker', [ "pair=$trading_pairs_list" ]));
		my $error = $data->{error};
		my $result = $data->{result};

		if ( @$error ) {
			$self->log_info('Kraken fetch exchange rates failed:', Dumper($error));
		}
		else {
			foreach my $key (keys %{$result}) {
				my $ccypair = substr($key, 0, 4) . '/' . substr($key, 4);
				my $item;
				$item->{_EXCHANGE} = EXCHANGE_ID;
				$item->{ccypair} = $ccypair;
				$item->{volume} = ($result->{$key}->{v})[0][0];
				$item->{sell} = ($result->{$key}->{a})[0][0];
				$item->{buy} = ($result->{$key}->{b})[0][0];
				$item->{high} = ($result->{$key}->{h})[0][0];
				$item->{low} = ($result->{$key}->{l})[0][0];
				$item->{timestamp} = DateTime->now()->epoch();
				push @exchange_rates, $item;
			}
		}

		$self->last_exchange_update(DateTime->now());
	};

	$self->exchange_rates(\@exchange_rates);
	return @exchange_rates;
}

sub fetch_balances
{
	my ($self) = @_;
	my $element;
	eval {
		my $data = decode_json($kraken->call(Private, 'Balance'));
		my $error = $data->{error};
		my $result = $data->{result};

		if ( @$error ) {
			$self->log_info('Kraken fetch balances failed:', Dumper($error));
		}
		else {
			$element->{_EXCHANGE} = EXCHANGE_ID;
			$element->{timestamp} = DateTime->now()->epoch();
			foreach my $ccy (keys %{$result}) {
				$element->{balances}->{$ccy} = $result->{$ccy};
			}
		}
	};

	return $element;
}


####################################################################################################################################
# New more Discrete methods
#
sub order_place
{
	my ($self, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_trade_at, $amount_to_buy, $units_to_trade, $last_price, $do_market_trade) = @_;
	$trading_pair =~ s/[\/_]//g;
    my $rate_decimals = $decimals{$trading_pair} // 3;
	$units_to_trade = (int($units_to_trade * 100000) / 100000) - 0.00001;

	my @params;
	push @params, "pair=$trading_pair";
	push @params, "type=$short_action";
    if ( $do_market_trade ) {
	    push @params, 'ordertype=market';
    } else {
	    push @params, 'ordertype=limit';
	    push @params, 'price=' . sprintf("%.${rate_decimals}f", $rate_to_trade_at);
    }
	push @params, sprintf "volume=%f", $units_to_trade;
	push @params, 'oflags=fciq';

	my $data;
	$self->log_debug('AddOrder', @params);
	eval { $data = decode_json($kraken->call(Private, 'AddOrder', [ @params ])); };

	if ( $@ ) {
		$self->log_debug('AddOrder failed', $@);
		return ( 0, undef, $@ );
	}

    $self->log_debug('AddOrder returned', Dumper($data));

	my $error = $data->{error};
	my $result = $data->{result};

	if ( $error && @$error ) {
		my $errid = $$error[0];

		return ( 0, undef, $errid );
	}

	return ( 1, $result->{txid}->[0], undef );
}

#		($success, $error) = $self->exchange->order_cancel($orderid);
sub order_cancel
{
	my ($self, $orderid) = @_;

	my $data = decode_json($kraken->call(Private, 'CancelOrder', [ "txid=$orderid" ]));
	$self->log_debug('CancelOrder return', Dumper(\$data));

	return ( 1, undef );
}

sub order_info
{
	my ($self, $orderid, $pair) = @_;
	$pair =~ s/[\/_]//g;

	my $data;
	my $response = $kraken->call(Private, 'QueryOrders', [ "trades=true", "txid=$orderid" ]);
	eval { $data = decode_json($response); };
	if ( $@ ) {
		$self->log_info("**ERROR** QueryOrders returned unexpected:", $response);
	}
	else {
		$self->log_debug('QueryOrders return', Dumper(\$data));
	}
	if ( $data->{result}->{$orderid} ) {
		$data = $data->{result}->{$orderid};
	}

	return {
		pair	=> $pair,
		type	=> $data->{descr}->{type},
		units	=> $data->{vol_exec},
		rate	=> $data->{price},
		fee     => $data->{fee},
		status	=> ($data->{status} eq 'pending' || $data->{status} eq 'open') ? 'O' : 'C',
	};
}

1;
