package BitTrader::Trader::Real;

use Modern::Perl;
use parent qw/BitTrader::Trader::Base/;
use Data::Dumper;


sub do_the_real_or_fake_trade
{
	my ($self, $origin, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_sell_at, $amount_to_buy, $units_to_trade, $last_price, $datastores, $audit_prefix, $order_timeout_override, $dont_allow_market_trades) = @_;
    my $success;
    my $orderid;
    my $error;
    my $order_status;
    $audit_prefix //= '';
    $amount_to_sell = $self->format_ccy($amount_to_sell);
    $amount_to_buy  = $self->format_ccy($amount_to_buy );
    $units_to_trade = $self->format_ccy($units_to_trade);

	if ( $self->exchange->can('order_place') ) {
# MASSIVE problems with Market Trading, dont use it until fixed
#        if ( !$dont_allow_market_trades && $self->exchange->use_market_rates ) {
#            my $minimum_sell_rate = lc($trading_pair) =~ /eur.usd/ ? 0.5 : 0.1;
#            $minimum_sell_rate = 0.005 if lc($trading_pair) =~ /ltc.btc/;
#            $rate_to_sell_at = lc($short_action) eq 'sell' ? $minimum_sell_rate : 9999 ;
#        }
		$self->log_info($audit_prefix, $self->exchange_id, 'Placing order for',
				 $short_action, 'of', $trading_pair, '.', $units_to_trade, 'units', 'at rate of', $rate_to_sell_at, '; previous rate', $last_price,
				);
		($success, $orderid, $error, $order_status) = $self->exchange->order_place($short_action,
									       $trading_pair,
									       $ccy_to_sell,
									       $ccy_to_buy,
									       $amount_to_sell,
									       $rate_to_sell_at,
									       $amount_to_buy,
									       $units_to_trade,
									       $last_price,
                                           ($dont_allow_market_trades) ? undef : $self->exchange->use_market_rates,
									      );
		$self->log_info($audit_prefix, $self->exchange_id, 'order_place. Success =>', $success, '; OrderID =>', $orderid, '; Error =>', $error, '<=');
		if ( !$success ) {
            $self->_audit($audit_prefix, 'Placement FAIL;', $error, "; INFO:", $self->exchange_id, $short_action, $trading_pair, $units_to_trade, 'units @', $rate_to_sell_at);
			if ( $error =~ /Value ([A-Z]+) must be greater than ([0-9.]+)*/i ) {
				$self->config->record_specific_minimum_amount_to_trade($self->exchange_id, $1, $2);
				$self->log_debug($audit_prefix, $self->exchange_id, "Setting minimum amount of $2 for $1 sale");
			}
			if ( $error eq 'EGeneral:Invalid arguments:volume' || $error eq 'DUST_TRADE_DISALLOWED_MIN_VALUE_50K_SAT'
              || $error eq 'MIN_TRADE_REQUIREMENT_NOT_MET' ) {
				if ( $short_action eq 'buy' ) {
					$self->config->record_specific_minimum_amount_to_trade($self->exchange_id, $ccy_to_buy, $amount_to_buy + 0.00001);
					$self->log_debug($audit_prefix, $self->exchange_id, "Setting minimum amount of @{[ $amount_to_buy + 0.00001 ]} for $ccy_to_buy sale");
				} else {
					$self->config->record_specific_minimum_amount_to_trade($self->exchange_id, $ccy_to_sell, $amount_to_sell + 0.00001);
					$self->log_debug($audit_prefix, $self->exchange_id, "Setting minimum amount of @{[ $amount_to_sell + 0.00001 ]} for $ccy_to_sell sale");
				}
			}
# TODO: Make this condition a lookup/function within the exchange module
			elsif ( $error =~ /It is not enough ([A-Z]+) in the account for sale./i
			     || $error =~ /Error: Place order error: Insufficient funds./i
			     || $error =~ /Insufficient ([A-Z]+) in account to complete this order./
			     || $error =~ /EOrder:Insufficient funds/
                 || $error =~ /INSUFFICIENT_FUNDS/ ) {
				$self->log_debug($audit_prefix, $self->exchange_id, 'Insufficient funds,', $error, 'retrying order with 99% amounts');
				return $self->do_the_real_or_fake_trade($origin, $short_action,
									$trading_pair,
									$ccy_to_sell,
									$ccy_to_buy,
									$amount_to_sell * 0.99,
									$rate_to_sell_at,
									$amount_to_buy * 0.99,
									$units_to_trade * 0.99,
									$last_price,
                                    $datastores,
                                    $audit_prefix,
                                    $order_timeout_override,
                                    ($dont_allow_market_trades) ? undef : $self->exchange->use_market_rates,
								       );
			}
			$self->log_debug($audit_prefix, 'Order placement failed:', $error);
			return 0;
		}
    }

# If no order id, the trade happened immediately
    unless ( $orderid ) {
        $self->log_debug($audit_prefix, 'Trade appears to have executed immediately', 'Pair', $trading_pair);
        my ($fee_amount, $fee_ccy) = $self->exchange->estimated_trade_fee($short_action, $trading_pair, $order_status->{units} // $units_to_trade);
        my $price_wanted = (lc($short_action) eq 'buy')
                            ? $order_status->{rate} * ( 1 + $self->config->config->{percentage_profit_wanted} )
                            : $order_status->{rate} * ( 1 - $self->config->config->{percentage_profit_wanted} )
			                ;
        $self->_audit($audit_prefix, 'Order executed immediately;', $self->exchange_id, $short_action, $trading_pair, $order_status->{units}, 'units @', $order_status->{rate});
        map {
                $_->record_successful_trade(  $origin,
                                              $self->exchange_id,
                                              $trading_pair,
                                              $short_action,
                                              $order_status->{rate} // $rate_to_sell_at,
                                              $order_status->{units} // $units_to_trade,
                                              $fee_amount,
                                              $fee_ccy,
                                              0,                        # No order id
                                              $self->exchange->estimated_trade_value_in_btc($_->{trading_pair}, $_->{trade_type}, $order_status->{units}),
                                              $price_wanted
                                            );
	    } @$datastores;
        return 1;
    }

    $self->log_debug($audit_prefix, 'Storing order details');
    $self->_audit($audit_prefix, 'Order placed;', $self->exchange_id, $short_action, $trading_pair, $units_to_trade, 'units @', $rate_to_sell_at,);
    map {
        $_->store_new_order($self->exchange_id, $trading_pair, $short_action, $rate_to_sell_at, $units_to_trade, $orderid, $order_timeout_override, $origin);
	} @$datastores;

    return 1;
}

1;
