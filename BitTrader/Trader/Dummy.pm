package BitTrader::Trader::Dummy;

use Modern::Perl;
use parent qw/BitTrader::Trader::Base/;
use Data::Dumper;


sub do_the_real_or_fake_trade
{
    my ($self, $origin, $short_action, $trading_pair, $ccy_to_sell, $ccy_to_buy, $amount_to_sell, $rate_to_sell_at, $amount_to_buy, $units_to_trade, $last_price, $datastores, $audit_prefix, $order_timeout_override, $dont_allow_market_trades) = @_;
    $audit_prefix //= '';
    $amount_to_sell = $self->format_ccy($amount_to_sell);
    $amount_to_buy  = $self->format_ccy($amount_to_buy );
    $units_to_trade = $self->format_ccy($units_to_trade);

    $self->log_info($audit_prefix, $self->exchange_id, 'Pretending to place order for',
				 $short_action, 'of', $trading_pair, '.', $units_to_trade, 'units', 'at rate of', $rate_to_sell_at, '; previous rate', $last_price,
				);

    my ($fee_amount, $fee_ccy) = $self->exchange->estimated_trade_fee($short_action, $trading_pair, $units_to_trade);
    my $price_wanted = (lc($short_action) eq 'buy')
                           ? $rate_to_sell_at * ( 1 + $self->config->config->{percentage_profit_wanted} )
                           : $rate_to_sell_at * ( 1 - $self->config->config->{percentage_profit_wanted} )
		           ;
    $origin = "Dummy ${origin}";
    map {
        $_->record_successful_trade(  $origin,
                                      $self->exchange_id,
                                      $trading_pair,
                                      $short_action,
                                      $rate_to_sell_at,
                                      $units_to_trade,
                                      $fee_amount,
                                      $fee_ccy,
                                      0,                        # No order id
                                      $self->exchange->estimated_trade_value_in_btc($trading_pair, $short_action, $units_to_trade),
                                      $price_wanted
                                  );
    } @$datastores;
 
    return 1;
}

1;
