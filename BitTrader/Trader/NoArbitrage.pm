package BitTrader::Trader::NoArbitrage;

use Modern::Perl;
use parent qw/BitTrader::Trader::Real/;
use Data::Dumper;

sub allow_arbitrage { 0 }

sub perform_arbitrage
{
    my $self = shift;

    $self->log_debug($self->exchange_id, 'Arbitrage disabled for this exchange');
    return 0;
}

1;
