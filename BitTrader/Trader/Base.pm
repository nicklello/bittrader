package BitTrader::Trader::Base;

use Modern::Perl;
use Data::Dumper;
use DateTime;

use parent qw/BitTrader::Base/;
use Time::HiRes qw/tv_interval gettimeofday/;

use Class::MethodMaker
	[	scalar	=>	[ qw/bittrader datastore exchange exchange_id/ ],
	];


sub new {
	my $class = shift;
	my $self  = bless {}, $class;

	$self->bittrader(shift);
	$self->datastore(shift);
	$self->exchange(shift);
    $self->exchange_id($self->exchange->EXCHANGE_ID);

	if ( $self->can('initialise') ) {
		$self->initialise(@_);
	}

	return $self;
}

sub allow_trades    { 1 }
sub allow_arbitrage { 1 }

sub _do_bestroute_trade
{
	my ($self, $trade_no, $exchange, $datastores, $theroute) = @_;

	my @ccys = split(/\//, $theroute->{pair});
	my $action   = $theroute->{action};
	my $pair = $theroute->{pair};
	my $rate = $theroute->{rate};
	my $ccy_sell = ($action eq 'sell') ? $ccys[0] : $ccys[1];
	my $ccy_buy  = ($action eq 'sell') ? $ccys[1] : $ccys[0];

    # Determine amount to be traded
	$exchange->balances(undef);                                         # Forces API call to get balance realtime
	my $amount = $exchange->currency_balance( $ccy_sell ) * $self->config->config->{percentage_of_funds_to_trade};
    unless ( $self->config->check_amount_exceeds_minimum($exchange, $ccy_sell, $amount) ) {
        $self->log_info('Cannot trade, not enough currency:', $self->format_ccy($amount), $ccy_sell);
        return 0;
    }
	my $units = ($action eq 'sell') ? $amount : $amount / $rate;

    return 0        unless $units * 10000;

	$self->log_info($exchange->EXCHANGE_ID, "Trade $trade_no:", $action, $pair,
                        $self->format_ccy($units), '@', $self->format_rate($rate),
                        'Selling', $self->format_ccy($amount), $ccy_sell, "($units) units");

	return $self->do_the_real_or_fake_trade(
        'Arbitrage',
		$action,
		$pair,
		$ccy_sell,
		$ccy_buy,
		($action eq 'sell') ? $units   : ($units / $rate),
		$rate,
		($action eq 'buy')  ? $units   : ($units / $rate),
		$units,
		undef,
		$datastores,
        "Trade $trade_no: $action $units $pair @ $rate:"
	);
}

sub _audit
{
    my $self = shift;

    $self->log_info($self->exchange_id, @_);
    $self->datastore->audit_message($self->exchange_id, @_);
}

sub perform_arbitrage
{
	my ($self, $exchange, $datastores) = @_;

	return	unless $exchange->arbitrage_routes;

    $exchange->update_arbitrage_routes();

    my $profitable = 100 + ($self->config->config->{percentage_profit_wanted} * 100);
	foreach my $route (grep { $_->{exchange} eq $exchange->EXCHANGE_ID } @{$exchange->arbitrage_routes}) {

		if ( !$route->{result} || $route->{result} < $profitable ) {
            next;
        }

        $self->log_debug($exchange->EXCHANGE_ID, 'Arbitrage: Profitable result', $profitable);
        $self->log_debug($exchange->EXCHANGE_ID, 'Arbitrage: Examining route', $route->{ccy}, 'with result', $route->{result});

		if ( $route->{result} > 150 ) {
            $self->log_debug($exchange->EXCHANGE_ID, 'Arbitrage: Route seems too profitable (> 150%)');
            next;
        }

        $self->log_debug($exchange->EXCHANGE_ID, 'Arbitrage: Route is profitable');
        $route->{is_profitable} = 1;

	    $exchange->balances(undef);
	    my $start_balance = $exchange->currency_balance( $route->{ccy} );
        my $trades_done = 0;
        my $failed_trade;

        if ( ($start_balance * 1000) < 1 ) {
            $self->log_debug($exchange->EXCHANGE_ID, 'Arbitrage: I do not enough coin to trade; balance is', $start_balance, $route->{ccy});
            next;
        }

	    $self->log_info('Executing', $exchange->EXCHANGE_ID, 'Arbitrage: Route', $route->{ccy}, 'Estimated profit', ($route->{result} - 100), '%');
        $self->_audit('Attempting arbitrage:', $route->{ccy},
                      $route->{route1}->{action}, $route->{route1}->{pair},
                      $route->{route2}->{action}, $route->{route2}->{pair},
                      $route->{route3}->{action}, $route->{route3}->{pair},
                      $route->{route4}->{action}, $route->{route4}->{pair},
                     );

	    if ( $self->_do_bestroute_trade( 1, $exchange, $datastores, $route->{route1}) ) {
    
            $trades_done = 1;
    
		    if ( $self->_do_bestroute_trade( 2, $exchange, $datastores, $route->{route2}) ) {
    
                $trades_done = 2;
    
			    if ( $route->{routes} > 2 && $self->_do_bestroute_trade( 3, $exchange, $datastores, $route->{route3}) ) {
    
                    $trades_done = 3;
    
			        if ( $route->{routes} > 3 && $self->_do_bestroute_trade( 4, $exchange, $datastores, $route->{route4}) ) {
    
                        $trades_done = 4;
    
                    }
                    elsif ( $route->{routes} > 3 ) {
                        $failed_trade = $route->{route4};
                    }
    
                }
                elsif ( $route->{routes} > 2 ) {
                    $failed_trade = $route->{route3};
                }
		    }
            else {
                $failed_trade = $route->{route2};
            }
        }
        else {
            $failed_trade = $route->{route1};
        }

	    $exchange->balances(undef);
        if ( $trades_done == $route->{routes} && !$failed_trade ) {
	        my $end_balance = $exchange->currency_balance( $route->{ccy} );
            $self->_audit( (($end_balance < $start_balance) ? '*PROFIT ALERT*' : ''),
                            'Started with', $self->format_ccy($start_balance), $route->{ccy},
                            '; Finished with', $end_balance, $route->{ccy});
        }
        elsif ( $failed_trade ) {
            my @ccys = split(/[_\/]/, $failed_trade->{pair});
            my $dest_ccy = $ccys[$failed_trade->{action} eq 'sell' ? 1 : 0];
	        my $end_balance = $exchange->currency_balance( $dest_ccy );
	        my $arb_balance = $exchange->currency_balance( $route->{ccy} );
            $self->_audit('Started with', $self->format_ccy($start_balance), $route->{ccy},
                            '; due to trade failure finished with', $self->format_ccy($arb_balance), $route->{ccy},
                            'and', $self->format_ccy($end_balance), $dest_ccy);
        }
        else {
            $self->_audit('***ERROR*** Trade failed in arbitrage but failed_trade did not get set');
        }

    }
}

sub process_analysis
{
	my ($self, $analysis_data, $datastores) = @_;
	my $sale_to_make;
	my $real_trader = ref($self) eq 'BitTrader::Trader::Real';

    if ( !$analysis_data ) {
		$self->log_info($self->exchange_id, "No analysis data");
        return ();
    }

# Where sell price is starting to increase after decreasing we should buy
# Where buy price is starting to decrease after increasing we should sell
	$self->exchange->balances(undef);

# Create an array showing of what we want to exchange for what
    my $process_start = [gettimeofday];
	foreach my $item (grep { $_->{exchange} eq $self->exchange_id } @$analysis_data) {
		my $trading_pair = uc($item->{currency});

		# Should never happen -- skip broken analysis rows
		unless ( $item->{currency} && $item->{reverse_sell_end} && $item->{reverse_buy_end} ) {
            $self->log_debug($self->exchange_id, 'Skipping broken/incomplete analysis data');
            $self->log_debug(Dumper($item));
			next;
		}

		# Don't bother looking at items marked as don't trade
		if ( $self->config->dont_trade($$datastores[0], $self->exchange, $trading_pair) ) {
            $self->log_debug('Pair', $trading_pair, 'marked as do not trade');
			next;
		}
		my @ccys = split(/\//, $trading_pair);
		my $ccy_to_buy;
		my $ccy_to_sell;
		my $rate_to_use;
		my $action;
		my $short_action;
		my $force_sale;
		my $price_buy = $item->{second_best_buy_price} // ($item->{reverse_buy_end});
		my $price_sell = $item->{second_best_sell_price} // ($item->{reverse_sell_end});

        $self->log_debug($item->{exchange}, $item->{currency},
                        'Recent buy trend good', $item->{recent_buy_trend_good}, 
                        'Reverse buy trend good', $item->{reverse_buy_trend_good}, 
                        'Buy price', $price_buy,
		                    'Profitable?',  $self->_buy_price_gives_us_profit($trading_pair, $price_buy) ? 'YES' : 'NO',
                        'Balance', $ccys[1], 'good for buy?', $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[1]) ? 'YES' : 'NO',
                    );
        $self->log_debug($item->{exchange}, $item->{currency},
                        'Recent sell trend good', $item->{recent_sell_trend_good}, 
                        'Reverse sell trend good', $item->{reverse_sell_trend_good},,
                        'Sell price', $price_sell,
                            'Profitable?',  $self->_sell_price_gives_us_profit($trading_pair, $price_sell) ? 'YES' : 'NO',
                        'Balance', $ccys[0], 'good for sell?', $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[0]) ? 'YES' : 'NO',
                    );
# Always Sell ?
		if (  $self->config->always_sell($$datastores[0], $self->exchange, $ccys[0])
		  && !$self->config->dont_buy($$datastores[0], $self->exchange, $ccys[1])
          &&  $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[0])
		   ) {
			$self->log_info($item->{exchange}, $ccys[0], 'Marked as always sell -- forcing Sell of', $trading_pair);
			$force_sale = 1;
			($ccy_to_sell, $ccy_to_buy) = @ccys;
			$rate_to_use = $price_sell;
			$short_action = 'Sell';
		}
		elsif ( $self->config->always_sell($$datastores[0], $self->exchange, $ccys[1])
		    && !$self->config->dont_buy($$datastores[0], $self->exchange, $ccys[0])
            &&  $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[1])
		   ) {
			$self->log_info($item->{exchange}, $ccys[0], 'Marked as always sell -- forcing Buy of', $trading_pair);
			$force_sale = 1;
			($ccy_to_buy, $ccy_to_sell) = @ccys;
			$rate_to_use = $price_buy;
			$short_action = 'Buy';
		}
# End Always Sell ?

# Sell when sell price starts going down after trending upwards so long as it's above the price bought at
# OR when sell price is above price+profit bought at And it's going up
		elsif ( !$self->config->dont_sell($$datastores[0], $self->exchange, $ccys[0])
		     && !$self->config->dont_buy($$datastores[0], $self->exchange, $ccys[1])
             &&  $item->{recent_sell_trend_good} && $item->{reverse_sell_trend_good}
             &&  $self->_sell_price_gives_us_profit($trading_pair, $price_sell)
             &&  $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[0])
		 	) {
			     $self->log_debug($item->{exchange}, 'Conditions right to Sell', $trading_pair, 'at', $price_sell);
			     ($ccy_to_sell, $ccy_to_buy) = @ccys;
			     $rate_to_use = $price_sell;
			     $short_action = 'Sell';
		}
# End Sell ?

# Buy when buy price starts going up after trending downwards so long as it's below the price sold at
# OR when buy price is below price-profit bought at And it's going down
		elsif ( !$self->config->dont_buy($$datastores[0], $self->exchange, $ccys[0])
		     && !$self->config->dont_sell($$datastores[0], $self->exchange, $ccys[1])
             &&  $item->{recent_buy_trend_good} && $item->{reverse_buy_trend_good}
		     &&  $self->_buy_price_gives_us_profit($trading_pair, $price_buy)
             &&  $self->config->check_balance_enough($$datastores[0], $self->exchange, $ccys[1])
			) {
			     $self->log_debug($item->{exchange}, 'Conditions right to Buy', $trading_pair, 'at', $price_buy);
			     ($ccy_to_buy, $ccy_to_sell) = @ccys;
			     $rate_to_use = $price_buy;
			     $short_action = 'Buy';
		}
# End Buy ?

		if ( $short_action ) {
		    $trading_pair =~ s/\//_/;
		    $trading_pair = uc($trading_pair);
            $self->log_debug($item->{exchange}, 'We may want to', $short_action, $trading_pair);

		    if ( $sale_to_make->{$ccy_to_sell}
		      && $self->config->prefer($$datastores[0], $self->exchange, $ccy_to_buy) > $sale_to_make->{$ccy_to_sell}->{pref} ) {
			    $self->log_debug($item->{exchange}, 'Prefer purchase of', $ccy_to_buy, 'over', ($sale_to_make->{$ccy_to_sell}->{ccys})[0]);
			    $force_sale = 1;
		    }

		    $action = "${short_action} ${trading_pair} based on exchange rate of ${rate_to_use} with balances of: " .
		               $self->format_ccy($self->exchange->currency_balance($ccy_to_sell)) . ' ' . $ccy_to_sell . ',  ' .
		               $self->format_ccy($self->exchange->currency_balance($ccy_to_buy))  . ' ' . $ccy_to_buy;
    
		    if ( !$sale_to_make->{$ccy_to_sell} || $force_sale) {
                $self->log_info($item->{exchange}, 'Queuing:', $action);
			    $sale_to_make->{$ccy_to_sell} = { action_text => $action,
							      short_action => $short_action,
							      trading_pair => $trading_pair,
							      ccy => $ccy_to_buy,
							      pref => $self->config->prefer($$datastores[0], $self->exchange, $ccy_to_buy),
							      rate => $rate_to_use,
							      last_price => $self->datastore->fetch_last_action($self->exchange_id,
                                                                                    $trading_pair,
                                                                                    $short_action eq 'buy' ? 'sell' : 'buy',
                                                                                    'price'),
							    };
		    }
		}
	}
    $self->log_trace($self->exchange_id, 'Determine Trades End', tv_interval($process_start, [gettimeofday]));

	if ( $sale_to_make ) {
        $self->log_trace($self->exchange_id, 'Want to make a trade');
		$self->perform_trades($sale_to_make, $datastores);
        return 1;
	}

    $self->log_trace($self->exchange_id, 'No trades to make this time');
    return 0;
}

sub perform_trades
{
	my ($self, $sale_to_make, $datastores) = @_;
	my $trades_made = 0;

# TODO: Review this code, simplify ?
	foreach my $ccy_sell (keys %{$sale_to_make}) {
		my $theaction = lc($sale_to_make->{$ccy_sell}{short_action});
		my $sell_ccy_bal = $self->exchange->currency_balance($ccy_sell) // 0;

        $self->log_debug($self->exchange_id, $ccy_sell, 'SALE TO MAKE', Dumper($sale_to_make->{$ccy_sell}));

		if ( $sale_to_make->{$ccy_sell}{ccy} && $sell_ccy_bal ) {
			my $conversion_rate = $sale_to_make->{$ccy_sell}{rate};
			if ( $theaction eq 'buy'  && $conversion_rate ) {
				$conversion_rate = 1 / $sale_to_make->{$ccy_sell}{rate};
			}

            my ($amount_to_sell, $amount_to_buy) = $self->config->adjusted_trade_amounts($self->exchange_id, $sell_ccy_bal, $theaction, $sale_to_make->{$ccy_sell}{trading_pair}, $conversion_rate);
			my $units_to_trade = $theaction eq 'buy' ? $amount_to_buy : $amount_to_sell;

			if ( $units_to_trade ) {
				$trades_made += $self->do_the_real_or_fake_trade(
                                    'Exchange Rate',
			    			        $theaction,
                                    $sale_to_make->{$ccy_sell}{trading_pair},
                                    $ccy_sell,
                                    $sale_to_make->{$ccy_sell}{ccy},
                                    $amount_to_sell,
                                    $sale_to_make->{$ccy_sell}{rate},
                                    $amount_to_buy,
                                    $units_to_trade,
                                    $sale_to_make->{$ccy_sell}{last_price},
                                    $datastores,
                                    "Non-arbitrage trade:"
					   );
			} else {
                $self->log_info('ERROR: We could not make the trade as it did not conform with configured limits --- units to trade are', $units_to_trade);
            }
		} else {
			$self->log_info("ERROR: Sale item passed with no purchase currency");
		}
	}

	return $trades_made;
}

sub _sell_price_gives_us_profit
{
	my ($self, $trading_pair, $wanted_trade_price) = @_;
	my $exchange = $self->exchange_id;
	my $sell_price_for_profit = $self->datastore->fetch_last_action($exchange, $trading_pair, 'buy', 'wanted_price')
                             // $self->datastore->fetch_last_action($exchange, $trading_pair, 'sell', 'price');

	unless ( $sell_price_for_profit ) {
		return 1;
	}

	if ( $wanted_trade_price > $sell_price_for_profit ) {
        $self->log_debug('Profit:', $exchange, 'Allowing sell of', $trading_pair, ' at price', $wanted_trade_price,
                            'needed to sell at more than', $sell_price_for_profit);
		return 1;
	}

    $self->log_debug('Profit:', $exchange, $trading_pair, 'Disallowing SELL at', $self->format_ccy($wanted_trade_price),
                        '; need to sell at', $self->format_ccy($sell_price_for_profit), 'to make a profit');
	return 0;
}

sub _buy_price_gives_us_profit
{
	my ($self, $trading_pair, $wanted_trade_price) = @_;
	my $exchange = $self->exchange_id;
	my $buy_price_for_profit = $self->datastore->fetch_last_action($exchange, $trading_pair, 'sell', 'wanted_price')
                            // $self->datastore->fetch_last_action($exchange, $trading_pair, 'buy', 'price');

	unless ( $buy_price_for_profit ) {
		return 1;
	}

	if ( $wanted_trade_price < $buy_price_for_profit ) {
        $self->log_debug('Profit:', $exchange, 'Allowing buy of', $trading_pair, ' at price', $wanted_trade_price,
                            'needed to buy at less than', $buy_price_for_profit);
		return 1;
	}

        $self->log_debug('Profit:', $exchange, $trading_pair, 'Disallowing BUY at', $self->format_ccy($wanted_trade_price),
		                    '; need to buy below', $self->format_ccy($buy_price_for_profit), 'to make a profit');
	return 0;
}

sub order_completion_trigger { shift->log_debug('order_completion_trigger is disabled in this trade module'); }

1;
