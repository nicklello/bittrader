package BitTrader::Trader::ProActive;

use Modern::Perl;
use parent qw/BitTrader::Trader::NoArbitrage/;
use Data::Dumper;


# To be trigged by exchange module when order closed,
# place a new order in the reverse direction
sub order_completion_trigger
{
    my ($self, $datastores, $exchange, $trading_pair, $trade_type, $original_trade_rate, $units, $price_wanted) = @_;
    my @ccy = split(/[-_\/]/, $trading_pair);

    $self->log_trace('order_completion_trigger; Exchange', $exchange, 'Pair', $trading_pair, 'Type', $trade_type, 'Original rate', $original_trade_rate, 'Units', $units, 'Price wanted', $price_wanted);

# Use balances based on units traded
    my @balances = ( $units, $units * $price_wanted );
    $self->log_trace('order_completion_trigger; Balances', @balances);

# Action and buy/sell pointers
    my $new_action = lc($trade_type) eq 'buy' ? 'sell' : 'buy';
    my $buy        = $new_action eq 'sell' ? 1 : 0;
    my $sell       = $new_action eq 'sell' ? 0 : 1;
    my $trade_units = $new_action eq 'buy' ? ($balances[1] * $price_wanted) : $balances[0];
    my $trade_sell  = $balances[$sell];
    my $trade_buy   = $new_action eq 'sell' ? ($balances[0] / $price_wanted) : ($balances[1] * $price_wanted);
    $self->log_trace('order_completion_trigger:', $trading_pair,
                            'Place order to', $new_action, $balances[$sell], $ccy[$sell], 'at', $price_wanted,
                            '; Units:', $trade_units, '; Buy:', $trade_buy, '; Sell:', $trade_sell);
    $self->do_the_real_or_fake_trade(   'Completion Trigger',
                                        $new_action,
                                        $trading_pair,
                                        $ccy[$sell],
                                        $ccy[$buy],
                                        $trade_sell,
                                        $price_wanted,
                                        $trade_buy,
                                        $trade_units,
                                        $original_trade_rate,
                                        $datastores,
                                        'order_completion_trigger Reverse trade',
                                        $self->config->config->{allow_loss_after_minutes},
                                        'dont_allow_market_trades');

}

1;
