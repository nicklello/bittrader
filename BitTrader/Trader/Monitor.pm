package BitTrader::Trader::Monitor;

use Modern::Perl;
use parent qw/BitTrader::Trader::Base/;

use Data::Dumper;


sub allow_arbitrage { 0 }
sub allow_trades    { 0 }

sub do_the_real_or_fake_trade
{
	my ($self, $origin, $short_action, $trading_pair, $ccy_sell, $ccy_to_buy, $amount_to_sell, $amount_to_buy, $therate, $purchase_amount, $units) = @_;

	return 0;  # Ensures no trade action stored
}

sub perform_arbitrage
{
	my ($self, $exchange, $datastores) = @_;

	$self->log_info('Exchange', $exchange->EXCHANGE_ID, 'is configured for Monitor trade module; no arbitrage performed');
}

sub process_analysis
{
	my ($self, $analysis_data, $datastores) = @_;

	$self->log_info($self->exchange_id, 'Configured for Monitor trade module; no analysis performed');
}

1;
