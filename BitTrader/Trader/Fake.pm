package BitTrader::Trader::Fake;

use Modern::Perl;
use parent qw/BitTrader::Trader::Base/;

use Data::Dumper;

my $balances;

sub initialise
{
	my $self = shift;

	$balances = $self->datastore->fetch_balances();
}

sub _adjust_balance
{
	my ($self, $ccy, $amount) = @_;

	my $new_balance = ( $balances->{"@{[ $self->exchange_id ]}|${ccy}"} || 0 ) + $amount;

	$balances->{"@{[ $self->exchange_id ]}|${ccy}"} = $new_balance;
}

sub print_balances
{
	my $self = shift;

	foreach my $key ( keys $balances ) {
		my @arr = split(/\|/, $key);
		my $ccy = $arr[1];
		next  if $arr[0] ne $self->exchange_id;

		$self->log_info($self->exchange_id, sprintf("%14.8f %3s", $balances->{$key}, $ccy));
	}
}

sub do_the_real_or_fake_trade
{
	my ($self, $origin, $short_action, $trading_pair, $ccy_sell, $ccy_to_buy, $amount_to_sell, $amount_to_buy, $therate, $purchase_amount, $units) = @_;

	$self->_adjust_balance($ccy_sell,  -($amount_to_sell));
	$self->_adjust_balance($ccy_to_buy, $purchase_amount);
	$self->print_balances();

	return 0;  # Ensures no trade action stored
}

1;
