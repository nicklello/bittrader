package BitTrader::Analysis;

use Modern::Perl;
use DateTime;
use Data::Dumper;

use parent qw/BitTrader::Base/;
use Time::HiRes qw/tv_interval gettimeofday/;

use Class::MethodMaker [	scalar		=> [ qw/bittrader/ ]    ];

sub new
{
	my $class = shift;
	my $self  = bless {}, $class;

    $self->bittrader(shift);

	return $self;
}


sub do_analysis
{
	my $self = shift;
	my $datastore = shift;
    my $exchange = shift;
    my $exchange_rates = shift;
	my $now   = DateTime->now();
    my @combined_data;

# TODO: Realistically we only need the data from the start and finish of the period
#       We really are not interested in the in-between data
#
    my $time_start = [gettimeofday];
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Previous Trends Start');
	my $previous_trend = $datastore->gather_rate_trends(
                            $exchange->EXCHANGE_ID,
                            $now->clone->subtract(minutes => ( $self->config->config->{minutes_to_use_for_recent_trends} * 2 )
                                                           + $self->config->config->{minutes_to_use_for_trend_reversal}),
                            $now->clone->subtract(minutes => $self->config->config->{minutes_to_use_for_recent_trends}
                                                           + $self->config->config->{minutes_to_use_for_trend_reversal}),
                            );
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Previous Trends End', tv_interval($time_start, [gettimeofday]));

    $time_start = [gettimeofday];
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Recent Trends Start');
	my $recent_trend  = $datastore->gather_rate_trends(
                            $exchange->EXCHANGE_ID,
                            $now->clone->subtract(minutes => $self->config->config->{minutes_to_use_for_recent_trends}
                                                           + $self->config->config->{minutes_to_use_for_trend_reversal}),
	                        $now->clone->subtract(minutes => $self->config->config->{minutes_to_use_for_trend_reversal}));
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Recent Trends End', tv_interval($time_start, [gettimeofday]));

    $time_start = [gettimeofday];
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Reverse Trends Start');
	my $reverse_trend = $datastore->gather_rate_trends(
                            $exchange->EXCHANGE_ID,
                            $now->clone->subtract(minutes => $self->config->config->{minutes_to_use_for_trend_reversal}),
                            $now);
    $self->log_trace($exchange->EXCHANGE_ID, 'Gather Reverse Trends End', tv_interval($time_start, [gettimeofday]));

# Keys for analysis may be shortened by configuration
    $time_start = [gettimeofday];
    $self->log_trace($exchange->EXCHANGE_ID, 'Shorten Keys Start');
    my @keys_for_analysis = sort { $a cmp $b } grep { $_ ne 'samples' } keys %{$reverse_trend};
    if ( $self->config->have_only_trade ) {
        @keys_for_analysis = grep {
		    my @keyinfo = split(/ /, $_);
            !$self->config->dont_trade(undef, undef, $keyinfo[1]) || $self->config->do_monitor(undef, undef, $keyinfo[1])
        } @keys_for_analysis;
        $self->log_debug('ONLY TRADE active, analysis pairs shortened to:', @keys_for_analysis);
    }
    $self->log_trace($exchange->EXCHANGE_ID, 'Shorten Keys End', tv_interval($time_start, [gettimeofday]));

# Combine the trends into new array for easy analysis
    $time_start = [gettimeofday];
    $self->log_trace($exchange->EXCHANGE_ID, 'Create Combined Data Start');
	@combined_data = grep { $_ } map {
		my $key = $_;
		my @keyinfo = split(/ /, $key);
        my $item_start = [gettimeofday];
        $self->log_trace($exchange->EXCHANGE_ID, 'Create Combined Data Item Start');

		my $data;
		$data->{exchange} = $keyinfo[0];
		$data->{currency} = $keyinfo[1];

		($data->{best_buy_price}, $data->{second_best_buy_price}) = $exchange->_get_best_buy_price($keyinfo[1], $exchange_rates);
		($data->{best_sell_price}, $data->{second_best_sell_price}) = $exchange->_get_best_sell_price($keyinfo[1], $exchange_rates);

		my $previous = $previous_trend->{$_};
		my $reverse = $reverse_trend->{$_};
		my $recent = $recent_trend->{$_};
$self->log_info(@keyinfo, 'Previous', 'Range..', ref($previous->{range_time}), $previous->{range_time});
$self->log_info(@keyinfo, 'Reverse', 'Range..', ref($reverse->{range_time}), $reverse->{range_time});
$self->log_info(@keyinfo, 'Recent', 'Range..', ref($recent->{range_time}), $recent->{range_time});

# TODO: Check date ranges are good enought, if not bypass the below and output nothing

		$data->{previous_sell_diff} = $previous->{sell_val};
		$data->{previous_sell_end} = $previous->{end_sell};
		$data->{previous_sell_trend} = trend_string($previous->{sell_val});
        $data->{previous_sell_trend_good} =($data->{previous_sell_trend}//'') ne 'D' ? 1 : 0;
		$data->{previous_buy_diff} = $previous->{buy_val};
		$data->{previous_buy_trend} = trend_string($previous->{buy_val});
        $data->{previous_buy_trend_good} =($data->{previous_buy_trend}//'') ne 'U' ? 1 : 0;
		$data->{previous_avg_buy_end} = $previous->{avg_buy_end};
		$data->{previous_avg_sell_end} = $previous->{avg_sell_end};
		$data->{previous_avg_sell_diff} = $previous->{avg_sell_diff};

		$data->{reverse_sell_diff} = $reverse->{sell_val};
		$data->{reverse_sell_end} = $reverse->{end_sell};
		$data->{reverse_sell_trend} = trend_string($reverse->{sell_val});
        $data->{reverse_sell_trend_good} =($data->{reverse_sell_trend}//'') eq 'D' ? 1 : 0;
		$data->{reverse_buy_diff} = $reverse->{buy_val};
		$data->{reverse_buy_end} = $reverse->{end_buy};
		$data->{reverse_buy_trend} = trend_string($reverse->{buy_val});
        $data->{reverse_buy_trend_good} =($data->{reverse_buy_trend}//'') eq 'U' ? 1 : 0;
		$data->{reverse_avg_buy_end} = $reverse->{avg_buy_end};
		$data->{reverse_avg_buy_diff} = $reverse->{avg_buy_diff};
		$data->{reverse_avg_sell_end} = $reverse->{avg_sell_end};
		$data->{reverse_avg_sell_diff} = $reverse->{avg_sell_diff};

		$data->{recent_sell_diff} = $recent->{sell_val};
		$data->{recent_sell_end} = $recent->{end_sell};
		$data->{recent_sell_trend} = trend_string($recent->{sell_val});
        $data->{recent_sell_trend_good} =($data->{recent_sell_trend}//'') ne 'D' ? 1 : 0;
		$data->{recent_buy_diff} = $recent->{buy_val};
		$data->{recent_buy_end} = $recent->{end_buy};
		$data->{recent_buy_trend} = trend_string($recent->{buy_val});
        $data->{recent_buy_trend_good} =($data->{recent_buy_trend}//'') ne 'U' ? 1 : 0;
		$data->{recent_avg_buy_end} = $recent->{avg_buy_end};
		$data->{recent_avg_buy_diff} = $recent->{avg_buy_diff};
		$data->{recent_avg_sell_end} = $recent->{avg_sell_end};
		$data->{recent_avg_sell_diff} = $recent->{avg_sell_diff};

        $self->log_trace($exchange->EXCHANGE_ID, 'Create Combined Data Item End', tv_interval($item_start, [gettimeofday]));

		$data;

	} @keys_for_analysis;
    $self->log_trace($exchange->EXCHANGE_ID, 'Create Combined Data End', tv_interval($time_start, [gettimeofday]));

    $self->log_debug('Analysis rows:', scalar @combined_data);
    map {
        $self->log_debug($_->{exchange}, $_->{currency}, 'Best buy', $_->{best_buy_price}, 'Best Sell', $_->{best_sell_price})
    } @combined_data;

	return \@combined_data;
}

sub trend_string
{
	my $diff = shift // 0;

	if ( $diff > 0 ) {
		return 'U';
	} elsif ( $diff < 0 ) {
		return 'D';
	}

	return '=';
}

1;
