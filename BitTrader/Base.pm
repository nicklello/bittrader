package BitTrader::Base;

use Modern::Perl;
use DateTime;

$| = 1;

sub config
{
    my $self = shift;

    return $self->bittrader->config;
}

sub _write_to_log
{
    my $self = shift;
    my $log_filename = './output.bittrader.log';

    open( LOGF, ">>$log_filename" ) or die "Cannot open logfile $log_filename";
    print LOGF join(' ', DateTime->now()->datetime(), (ref($self)||$self), map { $_//' ' } @_), "\n";
print join(' ', DateTime->now()->datetime(), (ref($self)||$self), map { $_//' ' } @_), "\n";
    close(LOGF);
}

sub log_info
{
	_write_to_log(shift, 'Info', @_);
}

sub log_debug
{
	my $self = shift;

    if ( $self->_do_i_log_this('debug') ) {
	    $self->_write_to_log('-- DEBUG --', @_);
    }
}

sub log_trace
{
	my $self = shift;

    if ( $self->_do_i_log_this('trace') ) {
	    $self->_write_to_log('-- TRACE --', @_);
    }
}

sub log_fatal
{
	my ($self, @msg) = @_;

    $self->log_info('FATAL:', @msg);
    die;
}

sub _do_i_log_this
{
    my ($self, $level) = @_;
    my @parts = split(/::/, ref($self));
    my $module_type = lc($parts[1] // $parts[0]);

    if ( !$module_type ) {
        $self->_write_to_log('CANNOT DERIVE MODULE TYPE');
die;
        return 0;
    }
#print join(' ', grep { $_ } caller(), caller(1)), "\n";
    my $log_key = join('_', 'log', $module_type, $level);
    if ( $self->config && $self->config->config && grep { $_ eq $log_key } keys %{$self->config->config}) {
        return $self->config->config->{$log_key} // 0;
    }

    return $self->config->config->{"log_${level}"};
}

sub format_ccy
{
	my $self = shift;

	return sprintf "%3.8f", shift//0;
}

sub format_rate
{
	my $self = shift;

	return sprintf "%3.11f", shift//0;
}

sub format_pct
{
	my $self = shift;

	return sprintf "%3.6f%%", (shift//0)*100;
}

sub format_short_ccy
{
	my $self = shift;

	return sprintf "%3.4f", shift//0;
}

sub format_short_rate
{
	my $self = shift;

	return sprintf "%3.4f", shift//0;
}

1;
