package BitTrader::HTML;

use Modern::Perl;
use DateTime;
use Data::Dumper;
use HTML::Template;

use parent qw/BitTrader::Base/;

use Class::MethodMaker
	[	scalar	=>	[ qw/bittrader datastore exchanges analysis/ ],
	];

my $btc_exchange_rates;
my $btc_nonspecific_rates;
my $ltc_exchange_rates;
my $ltc_nonspecific_rates;
my $usd_exchange_rates;
my $usd_nonspecific_rates;
my $eur_exchange_rates;
my $eur_nonspecific_rates;
my $gbp_exchange_rates;
my $gbp_nonspecific_rates;

sub new
{
	my $class = shift;
	my $self  = bless {}, $class;

	$self->bittrader(shift);
	$self->datastore(shift);
	$self->exchanges(shift);
	$self->analysis(shift);

	return $self;
}

sub output_templates
{
    my $self = shift;

    my $template_data = $self->_template_data();
    foreach ($self->config->html_templates) {
        my $tmpl = $_->{template};
        my $file = $_->{output};

        my @html = $self->_process_template($tmpl, $template_data);
	    open(HTDOC, ">$file") or die "Cannot open HTML Document $file for write";
	    print HTDOC join("\n", @html);
	    close(HTDOC);
    }
}

sub _template_data
{
    my $self = shift;

    return {
        UPDATE_TIME     => $self->format_datetime(DateTime->now()),
        MINS_RECENT     => $self->config->config->{minutes_to_use_for_recent_trends},
        MINS_REVERSE    => $self->config->config->{minutes_to_use_for_trend_reversal},
        ARBITRAGE       => [ $self->_arbitrage_data() ],
        AUDIT_ROWS      => [ $self->_audit_data() ],
        ORDERINFO       => [ $self->_orders_data() ],
        RECENTORDERS    => [ $self->_recent_orders_data() ],
        TRADES          => [ $self->_trades_data() ],
        PRICES          => [ $self->_prices_data() ],
        BALANCES        => [ $self->_balances_data() ],
    };
}

sub _arbitrage_data
{
    my $self = shift;

   my @routes = map {
       sort { ($b->{result}//-1) <=> ($a->{result}//-1) } @{$_->arbitrage_routes};
   } grep { $_ && $_->arbitrage_routes } @{$self->exchanges};

    return map {
        {
            RESULT              => $_->{result},
            EST_PROFIT          => _format_amount(($_->{result}//0) - 100),
            HAS_PROFIT          => _format_amount((($_->{result}//0) - 100) > 0 ? 1 : 0),
            EXCHANGE            => $_->{exchange},
            NUM_TRADES          => $_->{routes},
            CURRENCY            => $_->{ccy},
            TRADE1_ACTION       => $_->{route1}->{action},
            TRADE1_PAIR         => $_->{route1}->{pair},
            TRADE1_RATE         => _format_rate($_->{route1}->{rate}),
            TRADE1_EST_FEE      => _format_amount($_->{route1}->{estimated_fee}),
            TRADE1_EST_RESULT   => _format_amount($_->{route1}->{result}),
            TRADE2_ACTION       => $_->{route2}->{action},
            TRADE2_PAIR         => $_->{route2}->{pair},
            TRADE2_RATE         => _format_rate($_->{route2}->{rate}),
            TRADE2_EST_FEE      => _format_amount($_->{route2}->{estimated_fee}),
            TRADE2_EST_RESULT   => _format_amount($_->{route2}->{result}),
            TRADE3_ACTION       => $_->{route3}->{action},
            TRADE3_PAIR         => $_->{route3}->{pair},
            TRADE3_RATE         => _format_rate($_->{route3}->{rate}),
            TRADE3_EST_FEE      => _format_amount($_->{route3}->{estimated_fee}),
            TRADE3_EST_RESULT   => _format_amount($_->{route3}->{result}),
            TRADE4_ACTION       => $_->{route4}->{action},
            TRADE4_PAIR         => $_->{route4}->{pair},
            TRADE4_RATE         => _format_rate($_->{route4}->{rate}),
            TRADE4_EST_FEE      => _format_amount($_->{route4}->{estimated_fee}),
            TRADE4_EST_RESULT   => _format_amount($_->{route4}->{result}),
        }
    } @routes;
}

sub _audit_data
{
    my $self = shift;

	my $messages = $self->datastore->get_audit_messages();

    return !$messages ? () : map {
        {
            DATE_TIME   => $self->format_datetime($_->{timestamp}),
            MESSAGE     => $_->{message},
        }
    } @$messages;
}

sub _orders_data
{
    my $self = shift;

    return map {
        {
            DATE_TIME   => $self->format_datetime($_->{when_placed}),
            ORIGIN      => $_->{order_origin},
            EXCHANGE    => $_->{exchange},
            PAIR        => $_->{trading_pair},
            ACTION      => $_->{trade_type},
            EXPIRES     => $self->format_datetime($_->{when_timeout}),
            PRICE       => _format_rate($_->{price}//0),
            AMOUNT      => _format_amount($_->{amount}//0),
            ORDER_ID    => $_->{order_id}//0,
            STATUS      => $_->{last_order_status}//'?',
            WHEN_CHECKED => $self->format_datetime($_->{last_checked}),
            REMAINING   => _format_amount($_->{last_remaining}//0),
            TRADED      => _format_amount($_->{amount}//0) - _format_amount($_->{last_remaining}//0),
        }
    } grep { $self->_get_exchange($_->{exchange}) } $self->datastore->fetch_all_orders('open only');
}

sub _recent_orders_data
{
    my $self = shift;
    my $counter = 0;

    my @data = map {
        {
            DATE_TIME   => $self->format_datetime($_->{when_placed}),
            ORIGIN      => $_->{order_origin},
            EXCHANGE    => $_->{exchange},
            PAIR        => $_->{trading_pair},
            ACTION      => $_->{trade_type},
            EXPIRES     => $self->format_datetime($_->{when_timeout}),
            PRICE       => _format_rate($_->{price}//0),
            AMOUNT      => _format_amount($_->{amount}//0),
            ORDER_ID    => $_->{order_id}//0,
            STATUS      => $_->{last_order_status}//'?',
            WHEN_CHECKED => $self->format_datetime($_->{last_checked}),
            REMAINING   => _format_amount($_->{last_remaining}//0),
        }
    } grep { ($_->{last_order_status}//'?') ne 'O' && $counter++ < 10 }
      sort { $b->{when_placed} cmp $a->{when_placed} }
      grep { $self->_get_exchange($_->{exchange}) }
      $self->datastore->fetch_all_orders();
}

sub _trades_data
{
    my $self = shift;

	my @trades = $self->datastore->fetch_recent_trades(30);

    return map {
        {
            ORIGIN          => $_->{order_origin},
            EXCHANGE        => $_->{exchange},
            DATE_TIME       => $self->format_datetime($_->{trade_when}),
            PAIR            => $_->{trading_pair},
            ACTION          => $_->{trade_type},
            PRICE           => _format_rate($_->{price}),
            AMOUNT          => _format_amount($_->{amount}),
            PRICE_WANTED    => _format_rate($_->{price_wanted}),
        }
    } grep { $self->_get_exchange($_->{exchange}) } @trades;
}

sub _balances_data
{
    my $self = shift;

    my $balances = $self->datastore->fetch_balances();

    my $subtotal_btc = 0;
    my $total_btc = 0;
    my $subtotal_ltc = 0;
    my $total_ltc = 0;
    my $subtotal_usd = 0;
    my $total_usd = 0;
    my $subtotal_eur = 0;
    my $total_eur = 0;
    my $subtotal_gbp = 0;
    my $total_gbp = 0;
    my $current_exchange;
    my @data;
    my $exchange;
    my $ccy;

    foreach (sort { $a cmp $b } keys %{$balances}) {
		my $balance = $balances->{$_};
		($exchange, $ccy) = split(/\|/, $_);
        my ($estimated_btc, $est_btc_rate) = $self->estimated_btc_value_and_rate($exchange, $ccy, $balance);
        my ($estimated_ltc, $est_ltc_rate) = $self->estimated_ltc_value_and_rate($exchange, $ccy, $balance);
        my ($estimated_usd, $est_usd_rate) = $self->estimated_usd_value_and_rate($exchange, $ccy, $balance);
        my ($estimated_eur, $est_eur_rate) = $self->estimated_eur_value_and_rate($exchange, $ccy, $balance);
        my ($estimated_gbp, $est_gbp_rate) = $self->estimated_gbp_value_and_rate($exchange, $ccy, $balance);

        unless ( $self->_get_exchange($exchange) ) {
            $exchange = $current_exchange;
            next;
        }

        $current_exchange //= $exchange;

        if ( $current_exchange ne $exchange ) {
            push @data, {
                EXCHANGE    => $current_exchange,
                CURRENCY    => 'TOTAL',
                HAS_BALANCE => 1,
                BALANCE     => '',
                EST_BTC     => _format_amount($subtotal_btc),
                EST_LTC     => _format_amount($subtotal_ltc),
                EST_USD     => _format_fiat($subtotal_usd),
                EST_EUR     => _format_fiat($subtotal_eur),
                EST_GBP     => _format_fiat($subtotal_gbp),
            };
            $subtotal_btc = 0;
            $subtotal_usd = 0;
            $subtotal_eur = 0;
            $subtotal_gbp = 0;
            $current_exchange = $exchange;
        }
        $subtotal_btc += $estimated_btc;
        $total_btc += $estimated_btc;
        $subtotal_ltc += $estimated_ltc;
        $total_ltc += $estimated_ltc;
        $subtotal_usd += $estimated_usd;
        $total_usd += $estimated_usd;
        $subtotal_eur += $estimated_eur;
        $total_eur += $estimated_eur;
        $subtotal_gbp += $estimated_gbp;
        $total_gbp += $estimated_gbp;

        push @data, {
            EXCHANGE    => $exchange,
            CURRENCY    => $ccy,
            HAS_BALANCE => _has_balance($balance),
            BALANCE     => _format_amount($balance),
            EST_BTC     => _format_amount($estimated_btc),
            EST_BTC_RATE => _format_rate($est_btc_rate),
            EST_LTC     => _format_amount($estimated_ltc),
            EST_LTC_RATE => _format_rate($est_ltc_rate),
            EST_USD     => _format_fiat($estimated_usd),
            EST_USD_RATE => _format_rate($est_usd_rate),
            EST_EUR     => _format_fiat($estimated_eur),
            EST_EUR_RATE => _format_rate($est_eur_rate),
            EST_GBP     => _format_fiat($estimated_gbp),
            EST_GBP_RATE => _format_rate($est_gbp_rate),
        };
    };

    push @data, {
        EXCHANGE    => $exchange,
        CURRENCY    => 'TOTAL',
        HAS_BALANCE => 1,
        BALANCE     => '',
        EST_BTC     => _format_amount($subtotal_btc),
        EST_LTC     => _format_amount($subtotal_ltc),
        EST_USD     => _format_fiat($subtotal_usd),
        EST_EUR     => _format_fiat($subtotal_eur),
        EST_GBP     => _format_fiat($subtotal_gbp),
    };
    push @data, {
        EXCHANGE    => 'ALL EXCHANGES',
        CURRENCY    => 'TOTAL',
        HAS_BALANCE => 1,
        BALANCE     => '',
        EST_BTC     => _format_amount($total_btc),
        EST_LTC     => _format_amount($total_ltc),
        EST_USD     => _format_fiat($total_usd),
        EST_EUR     => _format_fiat($total_eur),
        EST_GBP     => _format_fiat($total_gbp),
    };

    return @data;
}

sub _prices_data
{
    my $self = shift;

    my $balances = $self->datastore->fetch_balances();
	my @trades = $self->datastore->get_all_last_trades();
	my @cached_prices = $self->datastore->fetch_prices();
    $self->_prepare_rates_data(@cached_prices);
    if ( $self->config->have_only_trade ) {
        $self->log_debug('ONLY TRADE active, only reporting enabled pairs');
        @cached_prices = grep { !$self->config->dont_trade(undef, undef, $_->{trading_pair})
                              || $self->config->do_monitor(undef, undef, $_->{trading_pair})
                              } @cached_prices;
    }

    return map {
        my $analysis = $self->_analysis_data($_->{exchange}, $_->{trading_pair});
        my $exchange_obj = $self->_get_exchange($_->{exchange});
        my $wanted_sell  = $self->datastore->fetch_last($_->{exchange}, $_->{trading_pair}, 'buy');
        my $sell_gives_profit = $exchange_obj && $exchange_obj->trader && $wanted_sell && $exchange_obj->trader->_sell_price_gives_us_profit($_->{trading_pair}, $_->{sell});
        my $wanted_buy   = $self->datastore->fetch_last($_->{exchange}, $_->{trading_pair}, 'sell');
        my $buy_gives_profit  = $exchange_obj && $exchange_obj->trader && $wanted_buy && $exchange_obj->trader->_buy_price_gives_us_profit($_->{trading_pair}, $_->{buy});

        {
            EXCHANGE            => $_->{exchange},
            PAIR                => $_->{trading_pair},
            PRICE_SELL          => _format_rate($_->{sell}),
            SELL_GIVES_PROFIT   => $sell_gives_profit,
            SELL_PREVIOUS_TREND   => $analysis->{previous_sell_trend},
            SELL_PREVIOUS_TREND_V => _format_rate($analysis->{previous_sell_diff}),
            SELL_PREVIOUS_GOOD    => $analysis->{previous_sell_trend_good},
            SELL_RECENT_TREND   => $analysis->{recent_sell_trend},
            SELL_RECENT_TREND_V => _format_rate($analysis->{recent_sell_diff}),
            SELL_RECENT_GOOD    => $analysis->{recent_sell_trend_good},
            SELL_REVERSE_TREND  => $analysis->{reverse_sell_trend},
            SELL_REVERSE_TREND_V => _format_rate($analysis->{reverse_sell_diff}),
            SELL_REVERSE_GOOD   => !$analysis->{reverse_sell_trend_good},
            SELL_PRICE_WANTED   => _format_rate($wanted_sell->{price_wanted}),
            SELL_WANTED_EXPIRES => $wanted_sell && $self->format_datetime($wanted_sell->{trade_expires}),
            PRICE_BUY           => _format_rate($_->{buy}),
            BUY_GIVES_PROFIT    => $buy_gives_profit,
            BUY_PREVIOUS_TREND    => $analysis->{previous_buy_trend},
            BUY_PREVIOUS_TREND_V  => _format_rate($analysis->{previous_buy_diff}),
            BUY_PREVIOUS_GOOD     => $analysis->{previous_buy_trend_good},
            BUY_RECENT_TREND    => $analysis->{recent_buy_trend},
            BUY_RECENT_TREND_V  => _format_rate($analysis->{recent_buy_diff}),
            BUY_RECENT_GOOD     => $analysis->{recent_buy_trend_good},
            BUY_REVERSE_TREND   => $analysis->{reverse_buy_trend},
            BUY_REVERSE_TREND_V => _format_rate($analysis->{reverse_buy_diff}),
            BUY_REVERSE_GOOD    => !$analysis->{reverse_buy_trend_good},
            BUY_PRICE_WANTED    => _format_rate($wanted_buy->{price_wanted}),
            BUY_WANTED_EXPIRES  => $wanted_buy && $self->format_datetime($wanted_buy->{trade_expires}),
            VOLUME              => _format_volume($_->{volume}),
        }
    } sort { $a->{sorting_pair} cmp $b->{sorting_pair}
    } map {
        $_->{sorting_pair} = $_->{trading_pair};
        $_->{sorting_pair} =~ s/XXBT/BTC/g;
        $_->{sorting_pair} =~ s/[XZ]([A-Z][A-Z][A-Z])/$1/g;
        $_
    } grep { $self->_get_exchange($_->{exchange}) } @cached_prices;
}

sub _get_exchange
{
    my ($self, $exchange_id) = @_;

    my @matches = grep { $_->EXCHANGE_ID eq $exchange_id } @{$self->exchanges};

    return $matches[0];
}

sub _process_template
{
    my ($self, $template, $param) = @_;
    my $htmltemp = HTML::Template->new(filename => $template, die_on_bad_params => 0);

    $htmltemp->param($param);

    return $htmltemp->output;
}

sub estimated_btc_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance) = @_;
    my @ccysplit = split(/ /, $ccy_id);
    $ccy_id = $ccysplit[0];

	if ( $ccy_id =~ /BTC|XXBT/ ) {
		return ( $balance, 1 );
	}

    my @ccys = split(/_/, $ccy_id);
    $ccy_id = $ccys[0];

    if ( $btc_exchange_rates->{$exchange_id . '|' . $ccy_id} ) {
        return ( ($btc_exchange_rates->{$exchange_id . '|' . $ccy_id} * $balance),
                 $btc_exchange_rates->{$exchange_id . '|' . $ccy_id} );
    }

    return ( ($btc_nonspecific_rates->{$ccy_id}) ? $btc_nonspecific_rates->{$ccy_id} * $balance : 0,
             $btc_nonspecific_rates->{$ccy_id} );
}

sub _estimated_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance, $to_ccy_id, $ccy_exchange_rates, $ccy_nonspecific_rates, $estimate_function) = @_;
    my @ccysplit = split(/ /, $ccy_id);
    $ccy_id = $ccysplit[0];

	if ( $ccy_id =~ /$to_ccy_id/ || !($balance*1000000) ) {
		return ( $balance, 1 );
	}

    if ( $balance eq 'inf' ) {
        return ( 0, 0 );
    }

    my @ccys = split(/_/, $ccy_id);
    $ccy_id = $ccys[0];
    my $secondary_ccy_id = $ccy_id;
    if ( $ccy_id =~ /^[XZ]/ ) {
        $secondary_ccy_id = substr($ccy_id, 1, 99);
    }

    my $rate = $ccy_exchange_rates->{$exchange_id . '|' . $ccy_id} // $ccy_exchange_rates->{$exchange_id . '|' . $secondary_ccy_id};
    if ( $rate ) {
        return ( $balance * $rate, $rate );
    }

    $rate = $ccy_nonspecific_rates->{$ccy_id} // $ccy_nonspecific_rates->{$secondary_ccy_id};
    if ( $rate ) {
        return ( $balance * $rate, $rate );
    }

    $rate = $btc_exchange_rates->{$exchange_id . '|' . $ccy_id} // $btc_exchange_rates->{$exchange_id . '|' . $secondary_ccy_id};
    if ( $rate ) {
        my $btc_val = $balance * $rate;
        my ($btc_eur, $btc_eur_rate) = $self->$estimate_function($exchange_id, 'BTC', $btc_val);
        return ($btc_eur, $rate * $btc_eur_rate);
    }

    $rate = $btc_nonspecific_rates->{$ccy_id} // $btc_nonspecific_rates->{$secondary_ccy_id};
    if ( $rate ) {
        my $btc_val = $balance * $rate;
        my ($btc_eur, $btc_eur_rate) = $self->$estimate_function($exchange_id, 'BTC', $btc_val);
        return ($btc_eur, $rate * $btc_eur_rate);
    }

    unless ( $rate ) {
        return ( 0, 0 );
    }

    return ( $balance * $rate, $rate );
}

sub estimated_ltc_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance) = @_;

    return ( $self->_estimated_value_and_rate($exchange_id, $ccy_id, $balance, 'LTC', $ltc_exchange_rates, $ltc_nonspecific_rates, 'estimated_ltc_value_and_rate') );
}

sub estimated_usd_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance) = @_;

    return ( $self->_estimated_value_and_rate($exchange_id, $ccy_id, $balance, 'USD', $usd_exchange_rates, $usd_nonspecific_rates, 'estimated_usd_value_and_rate') );
}

sub estimated_eur_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance) = @_;

    return ( $self->_estimated_value_and_rate($exchange_id, $ccy_id, $balance, 'EUR', $eur_exchange_rates, $eur_nonspecific_rates, 'estimated_eur_value_and_rate') );
}

sub estimated_gbp_value_and_rate
{
	my ($self, $exchange_id, $ccy_id, $balance) = @_;

    return ( $self->_estimated_value_and_rate($exchange_id, $ccy_id, $balance, 'GBP', $gbp_exchange_rates, $gbp_nonspecific_rates, 'estimated_gbp_value_and_rate') );
}

sub _has_balance
{
    my $bal = shift;

    return (($bal//0) * 10000000) ? 1 : 0;
}

sub format_datetime
{
	my ($self, $timestamp) = @_;

    return $timestamp // '-'   if !$timestamp || length($timestamp) < 15;
	return substr($timestamp, 8, 2) . '/' . substr($timestamp, 5, 2) . '/' . substr($timestamp, 2, 2) . ' ' . substr($timestamp, 11, 5);
}

sub _format_amount
{
    my $value = shift;
    return ''  unless $value;

    my @valsplit = split(/ /, $value);
    $value = shift @valsplit;

    return _has_balance($value)
            ? join(' ', sprintf ("%.8f", $value), @valsplit)
            : '';
}

sub _format_rate
{
    my $value = shift;
    return ''  unless $value;

    my @valsplit = split(/ /, $value);
    $value = shift @valsplit;

    return _has_balance($value)
            ? join(' ', sprintf ("%0.4f", $value), @valsplit)
            : '';
}

sub _format_fiat
{
    my $value = shift;

    my @valsplit = split(/ /, $value);
    $value = shift @valsplit;

    return _has_balance($value)
            ? join(' ', sprintf ("%0.2f", $value), @valsplit)
            : '0.00';
}

sub _format_volume
{
    my $value = shift;

    return sprintf "%0.5f", $value;
}

sub _prepare_rates_data
{
    my ($self, @cached_prices) = @_;

    map {
        if ( $_->{trading_pair} =~ /BTC|XXBT/ ) {
            my $pair = $_->{trading_pair};
            my @ccys = split(/[_\/-]/, $pair);
            my $buy_price  = $_->{buy};
            my $sell_price  = $_->{sell};
            eval {
                if ( $ccys[0] =~ /BT/ ) {
                    my $therate = $_->{exchange} eq 'Bittrex' ? $sell_price//$buy_price : 1 / ($sell_price // $buy_price);
                    $btc_exchange_rates->{$_->{exchange} . '|' . $ccys[1]} = $therate;
                    $btc_nonspecific_rates->{$ccys[1]} = $therate;
                } else {
                    my $therate = $sell_price // $buy_price;
                    $btc_exchange_rates->{$_->{exchange} . '|' . $ccys[0]} = $therate;
                    $btc_nonspecific_rates->{$ccys[0]} = $therate;
                }
            };
        }
        if ( $_->{trading_pair} =~ /LTC/ ) {
            my $pair = $_->{trading_pair};
            my @ccys = split(/[_\/-]/, $pair);
            my $buy_price  = $_->{buy};
            my $sell_price  = $_->{sell};
            eval {
                if ( $ccys[0] =~ /LTC/ ) {
                    my $therate = $_->{exchange} eq 'Bittrex' ? $sell_price//$buy_price : 1 / ($sell_price // $buy_price);
                    $ltc_exchange_rates->{$_->{exchange} . '|' . $ccys[1]} = $therate;
                    $ltc_nonspecific_rates->{$ccys[1]} = $therate;
                } else {
                    my $therate = $sell_price // $buy_price;
                    $ltc_exchange_rates->{$_->{exchange} . '|' . $ccys[0]} = $therate;
                    $ltc_nonspecific_rates->{$ccys[0]} = $therate;
                }
            };
        }

        if ( $_->{trading_pair} =~ /USD/ ) {
            my $pair = $_->{trading_pair};
            my @ccys = split(/[_\/-]/, $pair);
            my $buy_price  = $_->{buy};
            my $sell_price  = $_->{sell};
            eval {
                if ( $ccys[0] =~ /USD/ ) {
                    my $therate = $_->{exchange} eq 'Bittrex' ? $sell_price//$buy_price : 1 / ($sell_price // $buy_price);
                    $usd_exchange_rates->{$_->{exchange} . '|' . $ccys[1]} = $therate;
                    $usd_nonspecific_rates->{$ccys[1]} = $therate;
                } else {
                    my $therate = $sell_price // $buy_price;
                    $usd_exchange_rates->{$_->{exchange} . '|' . $ccys[0]} = $therate;
                    $usd_nonspecific_rates->{$ccys[0]} = $therate;
                }
            };
        }
        if ( $_->{trading_pair} =~ /EUR/ ) {
            my $pair = $_->{trading_pair};
            my @ccys = split(/[_\/-]/, $pair);
            my $buy_price  = $_->{buy};
            my $sell_price  = $_->{sell};
            eval {
                if ( $ccys[0] =~ /EUR/ ) {
                    my $therate = $_->{exchange} eq 'Bittrex' ? $sell_price//$buy_price : 1 / ($sell_price // $buy_price);
                    $eur_exchange_rates->{$_->{exchange} . '|' . $ccys[1]} = $therate;
                    $eur_nonspecific_rates->{$ccys[1]} = $therate;
                } else {
                    my $therate = $sell_price // $buy_price;
                    $eur_exchange_rates->{$_->{exchange} . '|' . $ccys[0]} = $therate;
                    $eur_nonspecific_rates->{$ccys[0]} = $therate;
                }
            };
        }
        if ( $_->{trading_pair} =~ /GBP/ ) {
            my $pair = $_->{trading_pair};
            my @ccys = split(/[_\/-]/, $pair);
            my $buy_price  = $_->{buy};
            my $sell_price  = $_->{sell};
            eval {
                if ( $ccys[0] =~ /GBP/ ) {
                    my $therate = $_->{exchange} eq 'Bittrex' ? $sell_price//$buy_price : 1 / ($sell_price // $buy_price);
                    $gbp_exchange_rates->{$_->{exchange} . '|' . $ccys[1]} = $therate;
                    $gbp_nonspecific_rates->{$ccys[1]} = $therate;
                    if ( $ccys[1] eq 'XXBT' && !$gbp_nonspecific_rates->{'BTC'} ) {
                        $gbp_nonspecific_rates->{'BTC'} = $therate;
                    }
                    elsif ( $ccys[1] =~ /^X/ && !$gbp_nonspecific_rates->{substr($ccys[1], 1, 99)} ) {
                        $gbp_nonspecific_rates->{substr($ccys[1], 1, 99)} = $therate;
                    }
                } else {
                    my $therate = $sell_price // $buy_price;
                    $gbp_exchange_rates->{$_->{exchange} . '|' . $ccys[0]} = $therate;
                    $gbp_nonspecific_rates->{$ccys[0]} = $therate;
                    if ( $ccys[0] eq 'XXBT' && !$gbp_nonspecific_rates->{'BTC'} ) {
                        $gbp_nonspecific_rates->{'BTC'} = $therate;
                    }
                    elsif ( $ccys[0] =~ /^X/ && !$gbp_nonspecific_rates->{substr($ccys[0], 1, 99)} ) {
                        $gbp_nonspecific_rates->{substr($ccys[0], 1, 99)} = $therate;
                    }
                }
            };
        }
    } @cached_prices;
}

sub _analysis_data
{
    my ($self, $exchange, $trading_pair) = @_;
    my ($theresult) = grep { $_->{exchange} eq $exchange && $_->{currency} eq $trading_pair } @{$self->analysis};

    return $theresult;
}

1;
