package BitTrader::Config;

use Modern::Perl;
use Data::Dumper;

use parent qw/BitTrader::Base/;

my $_current_config_file;
my $_config;
my @_dont_trade;
my @_dont_sell;
my @_dont_buy;
my @_always_sell;
my @_only_buy;
my @_only_sell;
my @_only_trade;
my @_only_monitor;
my %preferred;
my %_exchange_dont_buy;
my %_exchange_dont_sell;
my $_specific_minimum_amount_to_trade;
my $_specific_maximum_amount_to_trade;
my @_datastores;
my @_wallets;
my @_arbitrage_ccy;
my @_html_templates;

use Class::MethodMaker [	scalar		=> [ qw/bittrader/ ]    ];


sub new
{
	my $self = bless {}, shift;

    $self->bittrader(shift);

    $self->load_config();

    return $self;
}

sub config
{
	return $_config;
}

sub datastores
{
	my $self = shift;

	return \@_datastores;
}

sub wallets
{
    my $self = shift;

    return \@_wallets;
}

# Clear config switches/arrays (ie: does not affect exchanges/traders/datastores)
sub _clear_selected_config
{
	my $self = shift;

	@_dont_trade = ();
	@_dont_sell  = ();
	@_dont_buy   = ();
	@_always_sell = ();
    @_only_buy = ();
    @_only_sell = ();
    @_datastores = ();
    @_wallets = ();
    @_arbitrage_ccy = ();
    @_html_templates = ();
    undef(%preferred);
    undef(%_exchange_dont_buy);
    undef(%_exchange_dont_sell);
    $_specific_minimum_amount_to_trade = undef;
    $_specific_maximum_amount_to_trade = undef;
}

sub load_config
{
	my ($self, $config_file) = @_;
	my @config_data;
	$config_file //= 'trader.conf';

	if ( open(CONFIG, "<$config_file") ) {
		@config_data = <CONFIG>;
		close(CONFIG);
	} else {
		die "Cannot open config file $config_file";
	}
	$_current_config_file = $config_file;

	$self->_clear_selected_config();

	$_config->{allow_loss_after_minutes} = 360;			# Allow single trade at a loss after this long
    $_config->{exchange_rate_lag} = 0.10;               # Assume this percentage change in price
    $_config->{order_timeout} = 2;                      # How many minutes we want an order to stay open

	foreach my $line ( @config_data ) {
		next	if substr($line, 0, 1) eq '#';

		chomp $line;

		my @word = split(/[ 	]/, $line);
		@word = grep { $_ } @word;

		if ( @word ) {
			if ( lc($word[0]) eq 'exchange' ) {
				$_config->{exchange}->{$word[1]} = { trader => $word[2] // 'Fake',
							   	    key => $word[3],
							    	    secret => $word[4],
							    	    user => $word[5],
                                        param4 => $word[6],
							  	  };
			}

			elsif ( lc($word[0]) eq 'datastore' ) {
				push @_datastores, { class    => $word[1],
						     database => $word[2],
				 		     user     => $word[3],
						     password => $word[4],
						     dbhost   => $word[5],
						     dbport   => $word[6],
						   };
			}

            elsif ( lc($word[0]) eq 'wallet' ) {
                push @_wallets, {
                    id          =>  $word[1],
                    currency    =>  $word[2],
                    client      =>  $word[3],
                    rpcuser     =>  $word[4],
                    rpcpass     =>  $word[5],
                    rpcnode     =>  $word[6],
                    rpcport     =>  $word[7],
                };
            }

			elsif ( uc($word[0]) eq 'PERCENTAGE_OF_FUNDS_TO_TRADE' ) {
				$_config->{lc($word[0])} = $word[1]/100;
			}

			elsif ( lc($word[0]) eq 'percentage_fee' ) {
				$_config->{lc($word[0])} = $word[1]/100;
			}

			elsif ( lc($word[0]) eq 'percentage_profit_wanted' ) {
				$_config->{lc($word[0])} = $word[1]/100;
			}

			elsif ( lc($word[0]) eq 'exchange_rate_lag' ) {
				$_config->{lc($word[0])} = $word[1]/100;
			}

			elsif ( lc($word[0]) eq 'minimum_amount_to_trade' ) {
				if ( scalar @word > 3 && substr($word[1], 0, 1) ne '#' && substr($word[2], 0, 1) ne '#' ) {
					$self->record_specific_minimum_amount_to_trade($word[1], $word[2], $word[3], 'dont write config');
				}
				else {
					$_config->{lc($word[0])} = $word[1];
				}
			}

			elsif ( lc($word[0]) eq 'maximum_amount_to_trade' ) {
		        $_specific_maximum_amount_to_trade->{$word[1]}->{$word[2]} = $word[3];
			}

			elsif ( lc($word[0]) eq 'only_buy' ) {
				@_only_buy = split(/,/, $word[1]);
			}

			elsif ( lc($word[0]) eq 'only_sell' ) {
				@_only_sell = split(/,/, $word[1]);
			}

			elsif ( lc($word[0]) eq 'only_trade' ) {
				@_only_trade = split(/,/, uc($word[1]));
			}

			elsif ( lc($word[0]) eq 'only_monitor' ) {
				@_only_monitor = split(/,/, uc($word[1]));
			}

			elsif ( lc($word[0]) eq 'arbitrage_ccy' ) {
				@_arbitrage_ccy = split(/,/, $word[1]);
			}

			elsif ( lc($word[0]) eq 'dont_trade' ) {
				push @_dont_trade, $word[1];
			}

			elsif ( lc($word[0]) eq 'dont_sell' ) {
				if ( $word[2] && substr($word[2],0,1) ne '#' ) {
					my @list = $_exchange_dont_sell{$word[1]};
					push @list, $word[2];
					$_exchange_dont_sell{$word[1]} = $word[2];
				} else {
					push @_dont_sell, $word[1];
				}
			}

			elsif ( lc($word[0]) eq 'dont_buy' ) {
				if ( $word[2] && substr($word[2],0,1) ne '#' ) {
					my @list = $_exchange_dont_buy{$word[1]};
					push @list, $word[2];
					$_exchange_dont_buy{$word[1]} = $word[2];
				} else {
					push @_dont_buy, $word[1];
				}
			}

			elsif ( lc($word[0]) eq 'always_sell' ) {
				push @_always_sell, { ccy => $word[1], trigger => $word[2]//0.1 };
				push @_dont_buy, $word[1];
			}

			elsif ( lc($word[0]) eq 'prefer' ) {
				$preferred{$word[1]} = $word[2]//1;
			}

            elsif ( lc($word[0]) eq 'html_template' ) {
                push @_html_templates, { template => $word[1], output => $word[2] };
            }

            else {
                $_config->{lc($word[0])} = $word[1];
            }
		}
	}

	return $_config;
}

sub html_templates
{
    my ($self) = @_;

    return @_html_templates;
}

sub do_monitor
{
	my ( $self, $datastore_obj, $exchange_obj, $trading_pair ) = @_;

    # Only_monitor pairs are to be monitored
    if ( scalar @_only_monitor ) {
        if ( grep { lc($_) eq lc($trading_pair) } @_only_monitor ) {
            return 1;
        }
    }

	return 0;
}

sub dont_trade
{
	my ( $self, $datastore_obj, $exchange_obj, $trading_pair ) = @_;

    # Only_monitor pairs are not to be traded
    if ( scalar @_only_monitor ) {
        if ( grep { lc($_) eq lc($trading_pair) } @_only_monitor ) {
            return 1;
        }
    }

    # Only_trade superceeds everything else
    if ( scalar @_only_trade ) {
	    return ! grep { lc($_) eq lc($trading_pair) } @_only_trade;
    }

	return grep { lc($_) eq lc($trading_pair) } @_dont_trade;
}

sub dont_sell
{
	my ( $self, $datastore_obj, $exchange_obj, $currency_id ) = @_;
	my @list;

    # Only_trade superceeds everything else
    if ( scalar @_only_trade ) {
		@list = grep { $_ =~ /^$currency_id\/|\/$currency_id$/ } @_only_trade;
		return scalar @list == 0;
    }

	# Only sell forces this restriction
	if ( scalar @_only_sell ) {
		@list = grep { $_ eq $currency_id } @_only_sell;
		return scalar @list == 0;
	}

	if ( ref($exchange_obj) && $_exchange_dont_sell{$exchange_obj->EXCHANGE_ID} ) {
		@list = grep { lc($_) eq lc($currency_id) } $_exchange_dont_sell{$exchange_obj->EXCHANGE_ID};
	}
	elsif ( $_exchange_dont_sell{$exchange_obj} ) {
		@list = grep { lc($_) eq lc($currency_id) } $_exchange_dont_sell{$exchange_obj};
	}
	else {
		@list = grep { lc($_) eq lc($currency_id) } @_dont_sell;
	}

	return scalar @list > 0;
}

sub dont_buy
{
	my ( $self, $datastore_obj, $exchange_obj, $currency_id ) = @_;
	my @list;

    # Only_trade superceeds everything else
    if ( scalar @_only_trade ) {
		@list = grep { $_ =~ /^$currency_id\/|\/$currency_id$/ } @_only_trade;
		return scalar @list == 0;
    }

	# Only buy forces this restriction
	if ( scalar @_only_buy ) {
		@list = grep { $_ eq $currency_id } @_only_buy;
		return scalar @list == 0;
	}

	if ( $_exchange_dont_buy{$exchange_obj->EXCHANGE_ID} ) {
		@list = grep { lc($_) eq lc($currency_id) } $_exchange_dont_buy{$exchange_obj->EXCHANGE_ID};
	}
	else {
		@list = grep { lc($_) eq lc($currency_id) } @_dont_buy;
	}

	return scalar @list > 0;
}

sub always_sell
{
	my ( $self, $datastore_obj, $exchange_obj, $currency_id ) = @_;
	my $balance_to_check = $datastore_obj->ccy_balance($exchange_obj->EXCHANGE_ID, $currency_id);
	$balance_to_check //= 0;

	return grep { lc($_->{ccy}) eq lc($currency_id) && $_->{trigger} <= $balance_to_check } @_always_sell;
}

sub prefer
{
	my ( $self, $datastore_obj, $exchange_obj, $currency_id ) = @_;

	return $preferred{$currency_id} // 1;
}

sub check_balance_enough
{
	my ( $self, $datastore_obj, $exchange_obj, $currency_id ) = @_;
	my $balance_to_check = $exchange_obj->currency_balance($currency_id);

	if ( !$balance_to_check || ($balance_to_check * 1) == 0 ) {
		return 0;
	}

	return $self->check_amount_exceeds_minimum($exchange_obj, $currency_id, $balance_to_check);
}

sub check_amount_exceeds_minimum
{
	my ( $self, $exchange_obj, $currency_id, $amount ) = @_;
	my $__minimum_amount;

    if ( ! $amount ) {
        return 0;
    }

	if ( $_specific_minimum_amount_to_trade->{$exchange_obj->EXCHANGE_ID}->{$currency_id} ) {
		$__minimum_amount = $_specific_minimum_amount_to_trade->{$exchange_obj->EXCHANGE_ID}->{$currency_id};
	}
	else {
		$__minimum_amount = $_config->{minimum_amount_to_trade};
	}

	if ( !$__minimum_amount ) {
		return 1;
	}

	return ($amount >= $__minimum_amount);
}

sub adjusted_trade_amounts
{
    my ($self, $exchange_id, $sell_amount, $theaction, $trading_pair, $conversion_rate) = @_;
    my @ccy = split(/_/, $trading_pair);
    my ($sell_ccy, $buy_ccy) = ($theaction eq 'sell') ? ($ccy[0], $ccy[1]) : ($ccy[1], $ccy[0]);

    my $amount_to_sell = $sell_amount * $self->config->{percentage_of_funds_to_trade};
# TODO: Take balance into account... if we dont have enough, we need to return 0's
    if ( $_specific_minimum_amount_to_trade->{$exchange_id}->{$sell_ccy}
      && $amount_to_sell < $_specific_minimum_amount_to_trade->{$exchange_id}->{$sell_ccy} ) {
        $amount_to_sell = $_specific_minimum_amount_to_trade->{$exchange_id}->{$sell_ccy};
    }
    if ( $_specific_maximum_amount_to_trade->{$exchange_id}->{$sell_ccy}
      && $amount_to_sell > $_specific_maximum_amount_to_trade->{$exchange_id}->{$sell_ccy} ) {
        $amount_to_sell = $_specific_maximum_amount_to_trade->{$exchange_id}->{$sell_ccy};
    }

    my $amount_to_buy = $amount_to_sell * $conversion_rate;

    if ( $_specific_minimum_amount_to_trade->{$exchange_id}->{$buy_ccy}
      && $amount_to_buy < $_specific_minimum_amount_to_trade->{$exchange_id}->{$buy_ccy} ) {
$self->log_info('Amount to buy', $amount_to_buy, 'is below configured limit', $_specific_minimum_amount_to_trade->{$exchange_id}->{$buy_ccy});
        return ( 0, 0 );
    }

    return ($amount_to_sell, $amount_to_buy);
}

sub record_specific_minimum_amount_to_trade
{
	my ( $self, $exchange_id, $currency_id, $amount, $dont_write_config ) = @_;
    $amount = sprintf ("%.8f", $amount);

	if ( !$_specific_minimum_amount_to_trade->{$exchange_id}->{$currency_id} || $_specific_minimum_amount_to_trade->{$exchange_id}->{$currency_id} < $amount ) {
		$_specific_minimum_amount_to_trade->{$exchange_id}->{$currency_id} = $amount;

		if ( !$dont_write_config ) {
			open(CONFIG, ">>${_current_config_file}") || die "Cannot append to config file ${_current_config_file}: $!";
			print CONFIG join("\t", 'minimum_amount_to_trade', $exchange_id, $currency_id, $amount, '# Rule added automatically from exchange API error'), "\n";
			close(CONFIG);
		}
	}
}

sub is_arbitrage_ccy
{
	my ($self, $ccy) = @_;

	return !scalar @_arbitrage_ccy || grep { $_ eq $ccy } @_arbitrage_ccy;
}

sub have_only_trade
{
    return scalar @_only_trade;
}

1;
