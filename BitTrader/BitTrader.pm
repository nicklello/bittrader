package BitTrader::BitTrader;

use Modern::Perl;
use DateTime;

$| = 1;

use parent qw/BitTrader::Base/;
use Time::HiRes qw/tv_interval gettimeofday/;

use Class::MethodMaker
	[	scalar	=>	[ qw/config/ ],
        hash    =>  [ qw/config_data/ ],
	];

my @_datastore;
my @_exchanges;
my @_trader;

require BitTrader::Config;
require BitTrader::Analysis;
require BitTrader::HTML;


sub new
{
	my $self = bless {}, shift;

    $self->config(BitTrader::Config->new($self));
    $self->config_data($self->config->load_config());

    return $self;
}

sub datastores
{
    return @_datastore;
}

sub initialise
{
    my $self = shift;

    @_datastore = map {
        $self->initialise_datastore(qq/BitTrader::Datastore::$_->{class}/, $_->{database}, $_->{user}, $_->{password}, $_->{dbhost}, $_->{dbport})
    } @{$self->config->datastores};

    @_exchanges = map { $self->initialise_exchange(qq/BitTrader::Exchange::$_/,
					  $self->config_data->{exchange}->{$_}->{key},
					  $self->config_data->{exchange}->{$_}->{secret},
					  $self->config_data->{exchange}->{$_}->{user},
					  $self->config_data->{exchange}->{$_}->{param4},
					 )
		    } sort { $a cmp $b } keys %{$self->config_data->{exchange}};

    push @_exchanges, map { $self->initialise_wallet(
                     $_->{id},
                     $_->{currency},
                     $_->{client},
                     $_->{rpcuser},
                     $_->{rpcpass},
                     $_->{rpcnode},
                     $_->{rpcport},
                    )
                } @{$self->config->wallets};

    my $idx = 0; 
    @_trader = map {
		$self->initialise_trader(qq/BitTrader::Trader::/ . $self->config_data->{exchange}->{$_}->{trader},
				  $_datastore[0],
				  $_exchanges[$idx++]
				 );
	     } sort { $a cmp $b } keys %{$self->config_data->{exchange}};

    return $self;
}

sub initialise_exchange
{
	my ($self, $classname, $key, $secret, $apiuser, $param4) = @_;
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	eval require $classfile;

	return $classname->new($self, $key//' ', $secret//' ', $apiuser//' ', $param4//' ');
}

sub initialise_wallet
{
    my $self = shift;

    require BitTrader::Wallet;

    return BitTrader::Wallet->new($self, @_);
}

sub initialise_datastore
{
	my ($self, $classname) = (shift, shift);
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	require $classfile;

	return $classname->new($self, @_);
}

sub initialise_trader
{
	my ($self, $classname, $datastore, $exchange) = @_;
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	require $classfile;

	my $trader = $classname->new($self, $datastore, $exchange);
    $exchange->trader($trader);

    return $trader;
}

sub run_exchange_arbitrage
{
    my ($self, $exchange) = @_;
    my $time_start = [gettimeofday];

    $self->log_trace($exchange->EXCHANGE_ID, 'Arbitrage Start');

	$exchange->trader->perform_arbitrage($exchange, \@_datastore);

    $self->log_trace($exchange->EXCHANGE_ID, 'Arbitrage End', tv_interval($time_start, [gettimeofday]), 'secs');

    foreach my $ds (@_datastore) {
        $ds->store_good_arbitrage($exchange);
    }
}

sub run_collectors
{
    my $self = shift;
    my $collector_start = [gettimeofday];
    $self->log_trace('Run Collectors Start');
    my $time_start;

    $time_start = [gettimeofday];
    $self->log_trace('All exchanges arbitrage Start');
    map { $self->run_exchange_arbitrage($_) } grep { $_->trader->allow_arbitrage } @_exchanges;
    $self->log_trace('All exchanges arbitrage End', tv_interval($time_start, [gettimeofday]));

    $time_start = [gettimeofday];
    $self->log_trace('Cancel expired orders Start');
    map { $_->_cancel_expired_orders($_datastore[0]) } @_exchanges;
    $self->log_trace('Cancel expired orders End', tv_interval($time_start, [gettimeofday]));

    $time_start = [gettimeofday];
    $self->log_trace('Refresh order status Start');
    map { $_->_refresh_order_status_info(\@_datastore) } @_exchanges;
    $self->log_trace('Refresh order status End', tv_interval($time_start, [gettimeofday]));

    my @complete_analysis;
    my @complete_v2analysis;
    foreach my $exchange (@_exchanges) {
        $self->log_info($exchange->EXCHANGE_ID, 'Start');

        $time_start = [gettimeofday];
        $self->log_trace($exchange->EXCHANGE_ID, 'Main Cycle Start');

        $time_start = [gettimeofday];
        $self->log_info($exchange->EXCHANGE_ID, 'Fetch Exchange Rates Start');
        my @th_exchange_rates = $exchange->fetch_exchange_rates();
        $self->log_info($exchange->EXCHANGE_ID, 'Fetch Exchange Rates End', tv_interval($time_start, [gettimeofday]));

        $time_start = [gettimeofday];
        $self->log_trace($exchange->EXCHANGE_ID, 'Store Exchange Rates Start');
        foreach my $ds (@_datastore) {
            $ds->store_exchange_rates(\@th_exchange_rates);
        }
        $self->log_trace($exchange->EXCHANGE_ID, 'Store Exchange Rates End', tv_interval($time_start, [gettimeofday]));

        $time_start = [gettimeofday];
        $self->log_trace($exchange->EXCHANGE_ID, 'Analysis Start');
        my $analyser = BitTrader::Analysis->new($self);
        $self->log_trace($exchange->EXCHANGE_ID, 'Analysis Object Initialised', tv_interval($time_start, [gettimeofday]));
        my $analysis = $analyser->do_analysis($_datastore[0], $exchange, \@th_exchange_rates);
        $self->log_trace($exchange->EXCHANGE_ID, 'Analysis End', tv_interval($time_start, [gettimeofday]));

        # (try to) Perform Trades
        $time_start = [gettimeofday];
        if ( ref($analysis) eq 'ARRAY' && scalar @$analysis ) {
            push @complete_analysis, @$analysis;
            $self->log_trace($exchange->EXCHANGE_ID, 'Process Analysis Start');
		    $exchange->trader->process_analysis($analysis, \@_datastore);
            $self->log_trace($exchange->EXCHANGE_ID, 'Process Analysis End', tv_interval($time_start, [gettimeofday]));
        }

        $self->log_info($exchange->EXCHANGE_ID, 'Finish');
    }

	# Refresh balances data
    $time_start = [gettimeofday];
    $self->log_trace('Refreshing balances Start');
	foreach my $exchange (@_exchanges) {
        my @balances = $exchange->can('fetch_balances') ?  $exchange->fetch_balances() : ();
        foreach my $ds (@_datastore) {
	        $ds->store_balances(\@balances)           if @balances;
        }
	};
    $self->log_trace('Refreshing balances End', tv_interval($time_start, [gettimeofday]));

    # Process HTML templates
    $time_start = [gettimeofday];
    $self->log_trace('HTML Production Start');
    my $html_processor = BitTrader::HTML->new($self, $_datastore[0], \@_exchanges, \@complete_analysis);
    $self->log_trace('HTML Object Initialised', tv_interval($time_start, [gettimeofday]));
    $html_processor->output_templates();
    $self->log_trace('HTML Production End', tv_interval($time_start, [gettimeofday]));

    $self->log_trace('Run Collectors End', tv_interval($collector_start, [gettimeofday]));
}

1;
