package BitTrader::Wallet;

use Modern::Perl;

use parent qw/BitTrader::Exchange::Base/;

use DateTime;
use JSON::RPC::Client;
use JSON qw/decode_json/;
use Data::Dumper;

use Class::MethodMaker
	[	scalar		=> [ qw/bittrader EXCHANGE_ID currency client rpcuser rpcpass rpchost rpcport protocol/ ],
	];


sub _get_trading_pairs      {   ()  };

sub new
{
	my $class = shift;
	my $self = bless {}, $class;

    $self->bittrader(shift);
    $self->EXCHANGE_ID(shift);
    $self->currency(shift);
    $self->client(shift);
	$self->rpcuser(shift);
	$self->rpcpass(shift);
	$self->rpchost(shift);
	$self->rpcport(shift);
    $self->protocol(shift // 'http');

    $self->log_info('Initialised for', $self->EXCHANGE_ID, $self->currency);

	return $self;
}

sub fetch_exchange_rates
{
	my $self = shift;
	my @exchange_rates;

	$self->last_exchange_update(DateTime->now());

	return ();
}

sub _make_rpc_call
{
    my $self = shift;
    my $method = shift;
    my $http_client = JSON::RPC::Client->new;
    $http_client->ua->credentials(
        $self->rpchost . ':' . $self->rpcport,
        'jsonrpc',
        $self->rpcuser => $self->rpcpass
        );

    my $uri = $self->protocol . '://' . $self->rpchost . ':' . $self->rpcport;
    my $obj = { method  => $method, params  => [] };

    $self->log_debug($uri, $obj);
    eval {
            my $res = $http_client->call($uri, $obj);
            if ( !$res ) {
                $self->log_info($self->EXCHANGE_ID, 'Wallet', $method, 'HTTP Error:', $http_client->status_line);
                $self->log_debug(Dumper($http_client));
            }
            elsif ( $res->is_error ) {
                $self->log_info($self->EXCHANGE_ID, 'RPC Call', $method, 'Error:', $res->error_message);
                return undef;
            }
            else {
                $self->log_debug(Dumper($res->result));
            }

            return $res;
    };

    $self->log_info('RPC Call failed');
    return undef;
}

sub fetch_balances
{
	my ($self) = @_;
	my $element;
	$element->{_EXCHANGE} = $self->EXCHANGE_ID;
	$element->{timestamp} = DateTime->now()->epoch();

    if ( $self->client && $self->client ne '-' ) {
        $self->log_info('Client not supported:', $self->client);
        return $element;
    }

    my $res = $self->_make_rpc_call('listaccounts');
    if ( $res ) {
        map {
            $element->{balances}->{"@{[ $self->currency ]} @{[ $_ ]}"} = $res->result->{$_}
        } keys %{$res->result};
    }

    $element->{balances}->{"@{[ $self->currency ]} immature"} = $self->_immature_total();

	return $element;
}

sub _immature_total
{
    my ($self) = @_;
    my $total = 0.00000000000;

    my $res = $self->_make_rpc_call('listtransactions');
    if ( $res ) {
        eval {
            map { $total += $_->{amount} } grep { $_->{category} eq 'immature' } @$res->result
        };
    }

    return $total;
}

1;
