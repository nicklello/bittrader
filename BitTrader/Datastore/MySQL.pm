package BitTrader::Datastore::MySQL;

use Modern::Perl;

use DBI;
use parent qw/BitTrader::Datastore::Base/;
use DateTime;
use DateTime::Format::MySQL;

use Data::Dumper;

my $_dbh;
my $_cache;

sub get_dbh
{
	my $self = shift;
    my $dbhost = $self->hostname // 'localhost';

	$_dbh //= DBI->connect("DBI:mysql:database=@{[ $self->database ]};host=${dbhost}",
				$self->username,
				$self->password,
				{ 'RaiseError' => 1,
				  'AutoCommit' => 0,
				},
			      );

	return $_dbh;
}

sub disconnect
{
    if ( $_dbh ) {
        $_dbh->disconnect();
    }
    $_dbh = undef;
}

sub audit_message
{
	my $self = shift;
	my $dbh = $self->get_dbh();
	my $table = 'audit_messages';

	my $statement = $dbh->prepare(qq/INSERT INTO $table (timestamp, message) VALUES ( ?, ? ) /);
	$statement->execute(DateTime->now(), join(' ', grep { $_ } @_));
	$statement->finish();

	$dbh->commit();

	return 1;
}

sub get_audit_messages
{
	my $self = shift;
	my $dbh = $self->get_dbh();
	my $table = 'audit_messages';
    my @messages;

	my $statement = $dbh->prepare(qq/SELECT timestamp, message FROM $table ORDER BY audit_no DESC/);
	$statement->execute();
	while (my $ref = $statement->fetchrow_hashref()) {
        push @messages, { timestamp => $ref->{timestamp}, message => $ref->{message} };
    }
	$statement->finish();

	return \@messages;
}

sub store_balances
{
	my ($self, $balances) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'balance_history';

	my $cache_data;
	map {
		my $exchange = $_->{_EXCHANGE};
		my $currencydata = $_->{balances};
		my $timestamp = $_->{timestamp};
        my $stored_balances = $self->fetch_balances();
		if ( defined($currencydata) ) {
			my $statement = $dbh->prepare(qq/INSERT INTO $table (timestamp, exchange, ccy, amount) VALUES ( ?, ?, ?, ? ) /);
			my $latest_ins = $dbh->prepare(qq/INSERT INTO latest_balance (timestamp, exchange, ccy, amount) VALUES ( ?, ?, ?, ? ) /);
			my $latest_del = $dbh->prepare(qq/DELETE FROM latest_balance WHERE exchange = ?/);
			$latest_del->execute($exchange);
			$latest_del->finish();
			map {
                my $cache_key = join('|', $exchange, uc($_));
                my $stored_balance = $stored_balances->{$cache_key};
                my $new_balance = sprintf "%.8f", $currencydata->{$_};
				$latest_ins->execute(DateTime->from_epoch(epoch => $timestamp),
					        $exchange,
					        uc($_),
					        $new_balance);
				$cache_data->{$cache_key} = $new_balance;

# Check current latest balance; if there's no change do not write new rows.
                if ( !$stored_balance || $stored_balance != $new_balance ) {
				    $statement->execute(DateTime->from_epoch(epoch => $timestamp),
						    $exchange,
						    uc($_),
						    $new_balance);
                }
			} keys %{$currencydata};
			$latest_ins->finish();
			$statement->finish();
		}
	} @$balances;

	$dbh->commit();

	return 1;
}

sub store_exchange_rates
{
	my ($self, $rates) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'exchange_rates';

	if ( $rates ) {
		my $statement = $dbh->prepare(qq/
			INSERT INTO $table ( date_time, exchange, trading_pair, sell, buy, volume )
			VALUES ( ?, ?, ?, ?, ?, ? ) /);
		map {
            eval {
			    $statement->execute(
				        DateTime->from_epoch(epoch => $_->{timestamp}),
				        $_->{_EXCHANGE},
				        $_->{ccypair},
				        sprintf("%.14f", $_->{sell}//0),
				        sprintf("%.14f", $_->{buy}//0),
				        sprintf("%.14f", $_->{volume}//0),
			    );
            };
		} @$rates;
		$statement->finish();
	}

	$dbh->commit();

	$self->analyze_table($dbh, $table);

	return 1;
}

sub store_new_order
{
	my ($self, $exchange_id, $trading_pair, $action, $rate, $amount, $order_id, $timeout_override, $origin) = @_;
	$trading_pair =~ s/_/\//;
	my $dbh = $self->get_dbh();
	my $table = 'orders';

	my $statement = $dbh->prepare(qq/
		INSERT INTO $table (when_placed, exchange, trading_pair, trade_type, when_timeout, price, amount, order_id, last_order_status, last_remaining, order_origin)
		VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )/);
	$statement->execute(
		DateTime->now(),
		$exchange_id,
		$trading_pair,
		$action,
        DateTime->now()->add(minutes => ($timeout_override // $self->config->config->{order_timeout}) ),
		$rate,
		$amount,
		$order_id,
        'O',
        $amount,
        $origin,
	);
	$statement->finish();

	$dbh->commit();

	return 1;
}

sub record_successful_trade
{
	my ($self, $origin, $exchange_id, $trading_pair, $action, $rate, $amount, $estimated_fee, $fee_ccy, $order_id, $estimated, $price_wanted) = @_;
	$trading_pair =~ s/_/\//;
	my $dbh = $self->get_dbh();
	my $table = 'successful_trades';
	my $lasttable = 'last_trade';

    return unless $rate && $amount && $price_wanted;

	my $statement = $dbh->prepare(qq/
		INSERT INTO $table (trade_when, exchange, trading_pair, trade_type, price, amount, fee, fee_ccy, order_id, est_btc, price_wanted, order_origin)
		VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )/);
	$statement->execute(
		DateTime->now(),
		$exchange_id,
		$trading_pair,
		$action,
		$rate,
		$amount,
		$estimated_fee,
		$fee_ccy,
		$order_id,
		$estimated,
		$price_wanted,
        $origin,
	);
	$statement->finish();

	my $latest_upd = $dbh->prepare(qq/UPDATE $lasttable SET trade_when = ?, trade_type = ?, order_origin = ?, price = ?, amount = ?, est_btc = ?, price_wanted = ?
					   WHERE exchange = ? AND trading_pair = ?/);
	my $latest_ins = $dbh->prepare(qq/INSERT INTO $lasttable (trade_when, trade_type, order_origin, price, amount, price_wanted, exchange, trading_pair, est_btc)
					  VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) /);
	my $rc = $latest_upd->execute(DateTime->now(), $action, $rate, $origin, $amount, $estimated, $price_wanted, $exchange_id, $trading_pair);
	if ( !$rc || $rc eq '0E0' ) {
		$latest_ins->execute(DateTime->now(), $action, $origin, $rate, $amount, $price_wanted, $exchange_id, $trading_pair, $estimated);
	}

	$dbh->commit();

	return 1;
}

sub store_good_arbitrage
{
    my ($self, $exchange) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'arbitrage_estimate';

    return      unless $exchange->arbitrage_routes;

	my $insert = $dbh->prepare(qq/INSERT INTO $table ( timestamp, exchange, currency, trades, result,
                                                       trade1_action, trade1_pair, trade1_rate, trade1_result, trade1_fee,
                                                       trade2_action, trade2_pair, trade2_rate, trade2_result, trade2_fee,
                                                       trade3_action, trade3_pair, trade3_rate, trade3_result, trade3_fee,
                                                       trade4_action, trade4_pair, trade4_rate, trade4_result, trade4_fee )
					                          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) /);

    my $count=0;
    foreach my $arb ( @{$exchange->arbitrage_routes} ) {
		$insert->execute(DateTime->now(), $exchange->EXCHANGE_ID, $arb->{ccy}, $arb->{routes}, $arb->{result},
                     $arb->{route1}->{action}, $arb->{route1}->{pair}, $arb->{route1}->{rate}, $arb->{route1}->{result}, $arb->{route1}->{estimated_fee},
                     $arb->{route2}->{action}, $arb->{route2}->{pair}, $arb->{route2}->{rate}, $arb->{route2}->{result}, $arb->{route2}->{estimated_fee},
                     $arb->{route3}->{action}, $arb->{route3}->{pair}, $arb->{route3}->{rate}, $arb->{route3}->{result}, $arb->{route3}->{estimated_fee},
                     $arb->{route4}->{action}, $arb->{route4}->{pair}, $arb->{route4}->{rate}, $arb->{route4}->{result}, $arb->{route4}->{estimated_fee}
                     );
        $self->log_debug($insert->{ix_sqlcode}, $insert->{ix_sqlerrp}, $insert->{ix_sqlerrm})
            if $insert->{ix_sqlcode};
        $count++;
    }
    $insert->finish();
    $dbh->commit();
    $self->log_debug($exchange->EXCHANGE_ID, 'Stored', $count, 'routes into database');
}

sub analyze_table
{
	my ($self, $dbh, $tablename) = @_;

#	$dbh->do(qq/ANALYZE TABLE $tablename/);
}

sub fetch_recent_trades
{
	my ($self, $limit) = @_;
	my $dbh = $self->get_dbh();
	my @trades;

	$limit //= 20;

	my $statement = $dbh->prepare(qq/
		SELECT trade_when, exchange, trading_pair, order_origin, trade_type, price, amount, est_btc, order_id, price_wanted
		  FROM successful_trades
		 ORDER BY sequence DESC
         LIMIT $limit 
		/);

	eval {
		$statement->execute();
	};
	if ($@) {
		$self->log_debug('ERROR in fetch_recent_trends:', $@);
		return @trades;
	}

	while (my $ref = $statement->fetchrow_hashref()) {
		push @trades, $ref;
	}
	$statement->finish();

	return @trades;
}

1;
