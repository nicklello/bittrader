package BitTrader::Datastore::Informix;

use Modern::Perl;

use DBI;
use parent qw/BitTrader::Datastore::Base/;
use DateTime;
use Time::HiRes qw/tv_interval gettimeofday/;
use File::Temp qw(tempfile);

use Data::Dumper;

my $_dbh;

sub initialise
{
    my $self = shift;

    $self->log_trace('Informix specific initialisation');

    my (undef, $server) = split(/@/, $self->database);
    if ( $self->hostname && $self->port && $server ) {
        $self->log_info('Creating a local sqlhosts file for server', $server, 'at port', $self->port, 'on host', $self->hostname);

        my ($handle, $path) = tempfile("sqlhosts_$server\_XXXXX");
        print $handle join(' ', $server, 'onsoctcp', $self->hostname, $self->port, "\n");
        close($handle);

        my $sqlhosts = File::Spec->rel2abs($path);
        $ENV{'INFORMIXSQLHOSTS'} = $sqlhosts;
        $ENV{'INFORMIXSERVER'}   = $server;
        $self->log_info('SQLHOSTS is', $sqlhosts);
    }
}

sub get_dbh
{
	my $self = shift;

	$ENV{GL_DATETIME} = "%Y-%m-%d %H:%M:%S";
	$_dbh->commit()		if $_dbh;
	$_dbh //= DBI->connect("DBI:Informix:@{[ $self->database ]}",
				$self->username,
				$self->password,
				{ 'RaiseError' => 1,
				  'AutoCommit' => 0,
				  'PrintError' => 1,
				},
			      );

	return $_dbh;
}

sub disconnect
{
    if ( $_dbh ) {
        $_dbh->disconnect();
    }
    $_dbh = undef;
}

sub audit_message
{
	my $self = shift;
	my $dbh = $self->get_dbh();
	my $table = 'audit_messages';

	my $statement = $dbh->prepare(qq/INSERT INTO $table (timestamp, message) VALUES ( ?, ? ) /);
	$statement->execute(DateTime->now()->strftime($ENV{GL_DATETIME}), join(' ', grep { $_ } @_));
	$statement->finish();

	$dbh->commit();

	return 1;
}

sub get_audit_messages
{
	my $self = shift;
	my $dbh = $self->get_dbh();
	my $table = 'audit_messages';
    my @messages;

	my $statement = $dbh->prepare(qq/SELECT FIRST 15 timestamp, message FROM $table ORDER BY audit_no DESC/);
	$statement->execute();
	while (my $ref = $statement->fetchrow_hashref()) {
        push @messages, { timestamp => $ref->{timestamp}, message => $ref->{message} };
    }
	$statement->finish();

	return \@messages;
}

sub store_balances
{
	my ($self, $balances) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'balance_history';

	my $cache_data;
	map {
		my $exchange = $_->{_EXCHANGE};
		my $currencydata = $_->{balances};
		my $timestamp = $_->{timestamp};
        my $stored_balances = $self->fetch_balances();
		if ( defined($currencydata) ) {
			my $statement = $dbh->prepare(qq/INSERT INTO $table (timestamp, Exchange, CCY, Amount) VALUES ( ?, ?, ?, ? ) /);
			my $latest_ins = $dbh->prepare(qq/INSERT INTO latest_balance (timestamp, Exchange, CCY, Amount) VALUES ( ?, ?, ?, ? ) /);
			my $latest_del = $dbh->prepare(qq/DELETE FROM latest_balance WHERE Exchange = ?/);
			$latest_del->execute($exchange);
			$latest_del->finish();
			map {
                my $cache_key = join('|', $exchange, uc($_));
                my $stored_balance = $stored_balances->{$cache_key};
                my $new_balance = sprintf "%.8f", $currencydata->{$_};
				$latest_ins->execute(DateTime->from_epoch(epoch => $timestamp)->strftime($ENV{GL_DATETIME}),
					        $exchange,
					        uc($_),
					        $new_balance);
				$cache_data->{$cache_key} = $new_balance;

# Check current latest balance; if there's no change do not write new rows.
                if ( !$stored_balance || $stored_balance != $new_balance ) {
				    $statement->execute(DateTime->from_epoch(epoch => $timestamp)->strftime($ENV{GL_DATETIME}),
						    $exchange,
						    uc($_),
						    $new_balance);
                }
			} keys %{$currencydata};
			$latest_ins->finish();
			$statement->finish();
		}
	} @$balances;

	$dbh->commit();

	return 1;
}

sub store_exchange_rates
{
	my ($self, $rates) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'exchange_rates';

	if ( $rates ) {
		my $statement = $dbh->prepare(qq/
			INSERT INTO $table ( date_time, Exchange, Trading_Pair, sell, buy, volume )
			VALUES ( ?, ?, ?, ?, ?, ? ) /);
		map {
			$statement->execute(
				    DateTime->from_epoch(epoch => $_->{timestamp})->strftime($ENV{GL_DATETIME}),
				    $_->{_EXCHANGE},
				    $_->{ccypair},
				    sprintf("%.14f", $_->{sell}//0),
				    sprintf("%.14f", $_->{buy}//0),
				    sprintf("%.14f", $_->{volume}//0),
			);
		} @$rates;
		$statement->finish();
	}

	$dbh->commit();

	$self->analyze_table($dbh, $table);

	return 1;
}

sub record_successful_trade
{
	my ($self, $origin, $exchange_id, $trading_pair, $action, $rate, $amount, $estimated_fee, $fee_ccy, $order_id, $estimated, $price_wanted) = @_;
	$trading_pair =~ s/_/\//;
	my $dbh = $self->get_dbh();
	my $table = 'successful_trades';
	my $lasttable = 'last_trade';

	my $statement = $dbh->prepare(qq/
		INSERT INTO $table (trade_when, Exchange, Trading_Pair, trade_type, price, amount, fee, fee_ccy, order_id, est_btc, price_wanted, order_origin)
		VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )/);
	$statement->execute(
		DateTime->now()->strftime($ENV{GL_DATETIME}),
		$exchange_id,
		$trading_pair,
		$action,
		$rate,
		$amount,
		$estimated_fee,
		$fee_ccy,
		$order_id,
		$estimated,
		$price_wanted,
        $origin,
	);
	$statement->finish();

	my $latest_upd = $dbh->prepare(qq/UPDATE $lasttable SET trade_when = ?, trade_type = ?, order_origin = ?, price = ?, amount = ?, price_wanted = ?
					   WHERE Exchange = ? AND trading_pair = ?/);
	my $rc = $latest_upd->execute(DateTime->now()->strftime($ENV{GL_DATETIME}), $action, $origin, $rate, $amount, $price_wanted, $exchange_id, $trading_pair);
	if ( !$rc || $rc eq '0E0' ) {
	    my $latest_ins = $dbh->prepare(qq/INSERT INTO $lasttable (trade_when, trade_type, order_origin, price, amount, price_wanted, exchange, trading_pair)
					                      VALUES ( ?, ?, ?, ?, ?, ?, ?, ? ) /);
		$latest_ins->execute(DateTime->now()->strftime($ENV{GL_DATETIME}), $action, $origin, $rate, $amount, $price_wanted, $exchange_id, $trading_pair);
        $latest_ins->finish();
	}

    $latest_upd->finish();
	$dbh->commit();

	return 1;
}

sub store_good_arbitrage
{
    my ($self, $exchange) = @_;
	my $dbh = $self->get_dbh();
	my $table = 'arbitrage_estimate';

    return      unless $exchange->arbitrage_routes;

	my $insert = $dbh->prepare(qq/INSERT INTO $table ( timestamp, Exchange, Currency, trades, result,
                                                       trade1_action, trade1_pair, trade1_rate, trade1_result, trade1_fee,
                                                       trade2_action, trade2_pair, trade2_rate, trade2_result, trade2_fee,
                                                       trade3_action, trade3_pair, trade3_rate, trade3_result, trade3_fee,
                                                       trade4_action, trade4_pair, trade4_rate, trade4_result, trade4_fee )
					                          VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) /);

    my $count=0;
    foreach my $arb ( @{$exchange->arbitrage_routes} ) {
		$insert->execute(DateTime->now()->strftime($ENV{GL_DATETIME}), $exchange->EXCHANGE_ID, $arb->{ccy}, $arb->{routes}, $arb->{result},
                     $arb->{route1}->{action}, $arb->{route1}->{pair}, $arb->{route1}->{rate}, $arb->{route1}->{result}, $arb->{route1}->{estimated_fee},
                     $arb->{route2}->{action}, $arb->{route2}->{pair}, $arb->{route2}->{rate}, $arb->{route2}->{result}, $arb->{route2}->{estimated_fee},
                     $arb->{route3}->{action}, $arb->{route3}->{pair}, $arb->{route3}->{rate}, $arb->{route3}->{result}, $arb->{route3}->{estimated_fee},
                     $arb->{route4}->{action}, $arb->{route4}->{pair}, $arb->{route4}->{rate}, $arb->{route4}->{result}, $arb->{route4}->{estimated_fee}
                     );
        $self->log_debug($insert->{ix_sqlcode}, $insert->{ix_sqlerrp}, $insert->{ix_sqlerrm})
            if $insert->{ix_sqlcode};
        $count++;
    }
    $insert->finish();
    $dbh->commit();
    $self->log_debug($exchange->EXCHANGE_ID, 'Stored', $count, 'routes into database');
}

sub analyze_table
{
	my ($self, $dbh, $tablename) = @_;

#	$dbh->do(qq/ANALYZE TABLE $tablename/);
}

sub fetch_recent_trades
{
	my ($self, $limit) = @_;
	my $dbh = $self->get_dbh();
	my @trades;

	$limit //= 20;

	my $statement = $dbh->prepare(qq/
		SELECT FIRST $limit trade_when, Exchange, order_origin, Trading_Pair, trade_type, price, amount, order_id, est_btc, price_wanted
		  FROM successful_trades
		 ORDER BY trade_when DESC
		/);

	eval {
		$statement->execute();
	};
	if ($@) {
		$self->log_debug('ERROR in fetch_recent_trends:', $@);
		return @trades;
	}

	while (my $ref = $statement->fetchrow_hashref()) {
		push @trades, $ref;
	}
	$statement->finish();

	return @trades;
}

sub fetch_prices
{
	my ($self) = @_;
	my $dbh = $self->get_dbh();
	my @prices;

    $self->log_trace('fetch_prices Start');
	my $statement = $dbh->prepare(qq/
		SELECT e.date_time, e.exchange, e.trading_pair, e.sell, e.buy, e.volume
		  FROM exchange_rates e
		    INNER JOIN (
			SELECT max(date_time) timestamp, exchange, trading_pair
			  FROM exchange_rates
			 WHERE date_time > ?
			 GROUP BY exchange, trading_pair
		    ) x
			 ON x.timestamp = e.date_time
			AND x.exchange  = e.exchange
			AND x.trading_pair = e.trading_pair
		 ORDER BY e.exchange, e.trading_pair, e.date_time DESC
		/);

	$statement->execute(DateTime->now->subtract(hours => 12)->strftime($ENV{GL_DATETIME}));

    $self->log_trace('fetch_prices Storing rows');
	while (my $ref = $statement->fetchrow_hashref()) {
		push @prices, $ref;
	}
	$statement->finish();

    $self->log_trace('fetch_prices End');
	return @prices;
}

sub fetch_open_orders
{
	my ($self, $exchange, $want_expired) = @_;
	my $dbh = $self->get_dbh();
    my @orders;

    $self->log_trace('fetch_open_orders Start');
    my $expired = 'AND when_timeout < ?'  if $want_expired;
    $expired //= '';

	my $statement = $dbh->prepare(qq/
        SELECT *
          FROM orders
         WHERE ( last_order_status = 'O' OR last_order_status IS NULL )
           AND exchange = ?
         $expired
    /);
    if ( $want_expired ) {
        eval { $statement->execute($exchange, DateTime->now->strftime($ENV{GL_DATETIME})); };
        return @orders  if $@;
    } else {
        eval { $statement->execute($exchange); };
        return @orders  if $@;
    }

    while ( my $ref = $statement->fetchrow_hashref() ) {
        push @orders, $ref;
    }
    $statement->finish();

    $self->log_trace('fetch_open_orders End');
	return @orders;
}

sub gather_rate_trends
{
	my ($self, $exchange_id, $timestamp_from, $timestamp_to, $ok_to_fail) = @_;
	my $duration = $timestamp_to->subtract_datetime_absolute($timestamp_from)->in_units('seconds');
	my $timestamp_prev = $timestamp_from->clone->subtract(seconds => $duration);
	my $dbh = $self->get_dbh();
	my $table = 'exchange_rates';
	my $rate_trends;
	my @filedata;
    my $n_samples = 0;
    my $dt_last = 0;

# Start of time range
#Keys
    my $time_start = [gettimeofday];
    $self->log_trace('Select Raw Exchange Rate Data Start');
    my $rates_sth = $dbh->prepare(qq[
SELECT  qry1.exchange,
        qry1.trading_pair,
        MIN(qry1.min_timestamp)                                          min_timestamp,
        MAX(CASE WHEN date_time = min_timestamp  THEN buy    ELSE 0 END) start_buy,
        MAX(CASE WHEN date_time = min_timestamp  THEN sell   ELSE 0 END) start_sell,
        MAX(CASE WHEN date_time = min_timestamp  THEN volume ELSE 0 END) start_volume,
        MAX(qry1.max_timestamp)                                          max_timestamp,
        MAX(CASE WHEN date_time = max_timestamp  THEN buy    ELSE 0 END) end_buy,
        MAX(CASE WHEN date_time = max_timestamp  THEN sell   ELSE 0 END) end_sell,
        MAX(CASE WHEN date_time = max_timestamp  THEN volume ELSE 0 END) end_volume,
        MAX(qry1.range_time)                                             range_time,
        SUM(buy)                                                         buy_total,
        COUNT(buy)                                                       buy_count,
        AVG(buy)                                                         buy_avg,
        SUM(sell)                                                        sell_total,
        COUNT(sell)                                                      sell_count,
        AVG(sell)                                                        sell_avg,
        COUNT(DISTINCT TO_CHAR(date_time, '%H%M'))                       samples,
        MAX(prev.buy_total)                                              prev_buy_total,
        MAX(prev.buy_count)                                              prev_buy_count,
        MAX(prev.buy_avg)                                                prev_buy_avg,
        MAX(prev.sell_total)                                             prev_sell_total,
        MAX(prev.sell_count)                                             prev_sell_count,
        MAX(prev.sell_avg)                                               prev_sell_avg
  FROM  exchange_rates
    LEFT JOIN (
        SELECT  exchange, trading_pair, MIN(date_time) min_timestamp, MAX(date_time) max_timestamp,
                MAX(date_time) - MIN(date_time) AS range_time
          FROM  exchange_rates
         WHERE  date_time >= ?
           AND  date_time <= ?
           AND  exchange = ?
         GROUP BY exchange, trading_pair
    ) AS qry1
         ON (qry1.exchange = exchange_rates.exchange
         AND qry1.trading_pair = exchange_rates.trading_pair)
    LEFT OUTER JOIN (
        SELECT  qry2.exchange,
                qry2.trading_pair,
                SUM(exr2.buy)                                                         buy_total,
                COUNT(exr2.buy)                                                       buy_count,
                AVG(exr2.buy)                                                         buy_avg,
                SUM(exr2.sell)                                                        sell_total,
                COUNT(exr2.sell)                                                      sell_count,
                AVG(exr2.sell)                                                        sell_avg,
                COUNT(DISTINCT TO_CHAR(date_time, '%H%M'))                            samples
          FROM  exchange_rates exr2
            LEFT JOIN (
                SELECT  exchange, trading_pair, MIN(date_time) min_timestamp, MAX(date_time) max_timestamp
                  FROM  exchange_rates
                 WHERE  date_time >= ?
                   AND  date_time <= ?
                   AND  exchange = ?
                 GROUP BY exchange, trading_pair
                ) AS qry2
                 ON (qry2.exchange = exr2.exchange
                 AND qry2.trading_pair = exr2.trading_pair)
         WHERE  exr2.date_time >= ?
           AND  exr2.date_time <= ?
           AND  exr2.exchange = ?
        GROUP BY qry2.exchange, qry2.trading_pair
    ) AS prev
         ON (prev.exchange = exchange_rates.exchange
         AND prev.trading_pair = exchange_rates.trading_pair)
 WHERE  exchange_rates.date_time >= ?
   AND  exchange_rates.date_time <= ?
   AND  exchange_rates.exchange = ?
 GROUP BY qry1.exchange, qry1.trading_pair
    ]);

    $rates_sth->execute($timestamp_from->strftime($ENV{GL_DATETIME}), $timestamp_to->strftime($ENV{GL_DATETIME}), $exchange_id,
                        $timestamp_prev->strftime($ENV{GL_DATETIME}), $timestamp_from->strftime($ENV{GL_DATETIME}), $exchange_id,
                        $timestamp_prev->strftime($ENV{GL_DATETIME}), $timestamp_from->strftime($ENV{GL_DATETIME}), $exchange_id,
                        $timestamp_from->strftime($ENV{GL_DATETIME}), $timestamp_to->strftime($ENV{GL_DATETIME}), $exchange_id);
    $self->log_trace('Select Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

# Data has been ordered by date_time; therefore we can assume 1st row is the minimal date_time row
    $time_start = [gettimeofday];
    $self->log_trace('Process Raw Exchange Rate Data Start');
    my %saw_first_row;
    while (my $rates_ref = $rates_sth->fetchrow_hashref()) {
        if ( $rates_ref->{samples}//0 > $n_samples ) {
            $n_samples = $rates_ref->{samples};
        }

        my $hash_key = join(' ', $rates_ref->{exchange}, $rates_ref->{trading_pair});
        $rate_trends->{$hash_key}->{min_timestamp}   = $rates_ref->{min_timestamp};
        $rate_trends->{$hash_key}->{start_buy}       = $rates_ref->{start_buy};
        $rate_trends->{$hash_key}->{start_sell}      = $rates_ref->{start_sell};
        $rate_trends->{$hash_key}->{start_volume}    = $rates_ref->{start_volume};
        $rate_trends->{$hash_key}->{max_timestamp}   = $rates_ref->{max_timestamp};
        $rate_trends->{$hash_key}->{end_buy}         = $rates_ref->{end_buy};
        $rate_trends->{$hash_key}->{end_sell}        = $rates_ref->{end_sell};
        $rate_trends->{$hash_key}->{end_volume}      = $rates_ref->{end_volume};
        $rate_trends->{$hash_key}->{buy_total}       = $rates_ref->{buy_total};
        $rate_trends->{$hash_key}->{buy_count}       = $rates_ref->{buy_count};
        $rate_trends->{$hash_key}->{avg_buy_end}     = $rates_ref->{buy_avg};
        $rate_trends->{$hash_key}->{sell_total}      = $rates_ref->{sell_total};
        $rate_trends->{$hash_key}->{sell_count}      = $rates_ref->{sell_count};
        $rate_trends->{$hash_key}->{avg_sell_end}    = $rates_ref->{sell_avg};
        $rate_trends->{$hash_key}->{prev_buy_total}  = $rates_ref->{prev_buy_total};
        $rate_trends->{$hash_key}->{prev_buy_count}  = $rates_ref->{prev_buy_count};
        $rate_trends->{$hash_key}->{avg_buy_start}   = $rates_ref->{prev_buy_avg};
        $rate_trends->{$hash_key}->{prev_sell_total} = $rates_ref->{prev_sell_total};
        $rate_trends->{$hash_key}->{prev_sell_count} = $rates_ref->{prev_sell_count};
        $rate_trends->{$hash_key}->{avg_sell_start}  = $rates_ref->{prev_sell_avg};
        $rate_trends->{$hash_key}->{range_time}      = $rates_ref->{range_time};
    }
    $rates_sth->finish();
    $self->log_trace('Process Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

# Calculations....
    $time_start = [gettimeofday];
    $self->log_trace('Calculations Start');
    if ( $rate_trends ) {
        foreach my $hash_key (keys(%{$rate_trends})) {
            $rate_trends->{$hash_key}->{buy_val} = $rate_trends->{$hash_key}->{end_buy} - $rate_trends->{$hash_key}->{start_buy};
            $rate_trends->{$hash_key}->{sell_val} = $rate_trends->{$hash_key}->{end_sell} - $rate_trends->{$hash_key}->{start_sell};
# TODO: This assumes that the volume data is a rolling window
            $rate_trends->{$hash_key}->{volume} = $rate_trends->{$hash_key}->{end_volume} - $rate_trends->{$hash_key}->{start_volume};
		    eval { $rate_trends->{$hash_key}->{sell_pct} = $rate_trends->{$hash_key}->{sell_val} / $rate_trends->{$hash_key}->{start_sell} };
		    eval { $rate_trends->{$hash_key}->{buy_pct}  = $rate_trends->{$hash_key}->{buy_val}  / $rate_trends->{$hash_key}->{start_buy} };
# Calculations....
            if ( $rate_trends->{$hash_key}->{prev_sell_count} ) {
                $rate_trends->{$hash_key}->{avg_sell_start} = $rate_trends->{$hash_key}->{prev_sell_total} / $rate_trends->{$hash_key}->{prev_sell_count};
            }
		    $rate_trends->{$hash_key}->{avg_buy_diff} = ($rate_trends->{$hash_key}->{avg_buy_end}//0) - ($rate_trends->{$hash_key}->{avg_buy_start}//$rate_trends->{$hash_key}->{avg_buy_end}//0);
		    $rate_trends->{$hash_key}->{avg_sell_diff} = ($rate_trends->{$hash_key}->{avg_sell_end}//0) - ($rate_trends->{$hash_key}->{avg_sell_start}//$rate_trends->{$hash_key}->{avg_buy_end}//0);
	    }
	}
    $self->log_trace('Calculations End', tv_interval($time_start, [gettimeofday]));

    $rate_trends->{samples} = $n_samples;
	return $rate_trends;
}

sub store_new_order
{
	my ($self, $exchange_id, $trading_pair, $action, $rate, $amount, $order_id, $timeout_override, $origin) = @_;
	$trading_pair =~ s/_/\//;
	my $dbh = $self->get_dbh();
	my $table = 'orders';

	my $statement = $dbh->prepare(qq/
		INSERT INTO $table (when_placed, exchange, trading_pair, trade_type, when_timeout, price, amount, order_id, last_order_status, last_remaining, order_origin)
		VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )/);
	$statement->execute(
		DateTime->now()->strftime($ENV{GL_DATETIME}),
		$exchange_id,
		$trading_pair,
		$action,
        DateTime->now()->add(minutes => ($timeout_override // $self->config->config->{order_timeout}) )->strftime($ENV{GL_DATETIME}),
		$rate,
		$amount,
		$order_id,
        'O',
        $amount,
        $origin,
	);
	$statement->finish();

	$dbh->commit();

	return 1;
}

sub update_order
{
	my ($self, $order_id, $new_remaining, $new_status) = @_;
	my $dbh = $self->get_dbh();

    $self->log_trace('update_order Start');
	my $updated = $dbh->do(qq/
        UPDATE  orders
           SET  last_order_status = ?,
                last_remaining    = ?,
                last_checked      = ?
         WHERE  order_id = ?
    /, {}, $new_status, $new_remaining, DateTime->now()->strftime($ENV{GL_DATETIME}), $order_id );

    if ( $updated != 1 ) {
        $self->log_debug('ERROR: Updated', $updated, 'rows -- only expected to update 1');
    }

    $self->log_trace('update_order End');
}

sub fetch_last
{
	my ($self, $exchange, $trading_pair, $trade_type) = @_;
	$trading_pair =~ s/_/\//g;
	my $dbh = $self->get_dbh();
    my $action;

	my $statement = $dbh->prepare(qq/
		SELECT *, (trade_when + ? UNITS MINUTE) trade_expires
		  FROM last_trade e
         WHERE exchange = ?
           AND trading_pair =? 
           AND (trade_when + ? UNITS MINUTE) >= SYSDATE
           AND LOWER(trade_type) = ?
		/);

	$statement->execute($self->config->config->{allow_loss_after_minutes}, $exchange, $trading_pair, $self->config->config->{allow_loss_after_minutes}, lc($trade_type));

	while (my $ref = $statement->fetchrow_hashref()) {
		$action = $ref;
	}
	$statement->finish();

    return $action;
}

sub fetch_last_action
{
	my ($self, $exchange, $trading_pair, $trade_type, $what_wanted) = @_;

    my $action = $self->fetch_last($exchange, $trading_pair, $trade_type);

    unless ( $action ) {
        return undef;
    }
    if ( lc($what_wanted) eq 'price' ) {
        return $action->{price};
    }
    return $action->{price_wanted};
}

1;
