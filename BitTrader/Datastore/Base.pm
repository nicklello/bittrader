package BitTrader::Datastore::Base;

use Modern::Perl;

use parent qw/BitTrader::Base/;
use DateTime;
use Time::HiRes qw/tv_interval gettimeofday/;
use List::Util qw/max/;
use Data::Dumper;

use Class::MethodMaker
	[	scalar		=> [ qw/bittrader database username password hostname port nonce_value/ ]
	];


sub new
{
	my $self = bless {}, shift;

	$self->bittrader(shift);
	$self->database(shift);
	$self->username(shift);
	$self->password(shift);
	$self->hostname(shift);
	$self->port(shift);

    if ( $self->can('initialise') ) {
        $self->initialise();
    }

	return $self;
}

sub store_exchange_rates
{
	my $self = shift;
	my @rates = @_;

	die "store_exchange_rates must be defined in subclass";
	return 1;
}

sub ccy_balance
{
	my ($self, $exchange, $ccy) = @_;
	return unless $ccy;

	my $balances = $self->fetch_balances;

	return $balances->{"${exchange}|${ccy}"};
}

sub fetch_balances
{
	my $self = shift;
	my $opt_exchangeid;
	my $dbh = $self->get_dbh();
	my $table = 'latest_balance';
	my $balances;

	my $sth = $dbh->prepare(qq/
			SELECT exchange, ccy, amount
			  FROM $table
			@{[ ($opt_exchangeid) ? 'WHERE exchange = ?' : '' ]}
		/);
	$sth->execute()            unless $opt_exchangeid;
	$sth->execute($opt_exchangeid) if $opt_exchangeid;

	while (my $ref = $sth->fetchrow_hashref()) {
		my $key = join('|', $ref->{exchange}, $ref->{ccy});

		$balances->{$key} = sprintf "%.8f", $ref->{amount};
	}
	$sth->finish();

	return $balances;
}

sub get_all_last_trades
{
    my $self = shift;
	my $lasttable = 'last_trade';
	my $dbh = $self->get_dbh();

    my $statement;
    eval {
        $statement = $dbh->prepare(qq/
            SELECT  exchange, trading_pair, trade_type, price, amount, price_wanted, trade_when
              FROM  $lasttable
        /);
    };
    if ( $@ ) {
        $self->log_info('**PREPARE ERROR**', $@);
        return 0;
    }

    eval {
	    $statement->execute();
    };
    if ( $@ ) {
        $self->log_info('**EXECUTE ERROR**', $@);
        return 0;
    }

    my @data;
	while (my $ref = $statement->fetchrow_hashref()) {
        push @data, $ref;
    }

    return @data;
}

sub _string_to_datetime
{
    my ($self, $str) = @_;

    return DateTime->new(
        year        =>  substr($str,  0, 4),
        month       =>  substr($str,  5, 2),
        day         =>  substr($str,  8, 2),
        hour        =>  substr($str, 11, 2),
        minute      =>  substr($str, 14, 2),
        second      =>  substr($str, 17, 2),
    );
}

sub gather_rate_trends
{
	my ($self, $exchange_id, $timestamp_from, $timestamp_to, $ok_to_fail) = @_;
	my $duration = $timestamp_to->subtract_datetime_absolute($timestamp_from)->in_units('seconds');
	my $timestamp_prev = $timestamp_from->clone->subtract(seconds => $duration);
	my $dbh = $self->get_dbh();
	my $table = 'exchange_rates';
	my $rate_trends;
	my @filedata;
    my $n_samples = 0;
    my $dt_last = 0;

# Start of time range
#Keys
    my $time_start = [gettimeofday];
    $self->log_trace('Select Raw Exchange Rate Data Start');
    my $rates_sth = $dbh->prepare(qq/
SELECT  qry1.exchange,
        qry1.trading_pair,
        MIN(qry1.min_timestamp)                                          min_timestamp,
        MAX(CASE WHEN date_time = min_timestamp  THEN buy    ELSE 0 END) start_buy,
        MAX(CASE WHEN date_time = min_timestamp  THEN sell   ELSE 0 END) start_sell,
        MAX(CASE WHEN date_time = min_timestamp  THEN volume ELSE 0 END) start_volume,
        MAX(qry1.max_timestamp)                                          max_timestamp,
        MAX(CASE WHEN date_time = max_timestamp  THEN buy    ELSE 0 END) end_buy,
        MAX(CASE WHEN date_time = max_timestamp  THEN sell   ELSE 0 END) end_sell,
        MAX(CASE WHEN date_time = max_timestamp  THEN volume ELSE 0 END) end_volume,
        SUM(buy)                                                         buy_total,
        COUNT(buy)                                                       buy_count,
        SUM(sell)                                                        sell_total,
        COUNT(sell)                                                      sell_count,
        COUNT(DISTINCT MINUTE(from_unixtime(date_time)))                 samples,
        MAX(range_timestamp)                                             range_time
  FROM  exchange_rates
    LEFT JOIN (
        SELECT  exchange, trading_pair, MIN(date_time) min_timestamp, MAX(date_time) max_timestamp,
                MAX(date_time) - MIN(date_time) AS range_timestamp
          FROM  exchange_rates
         WHERE  date_time >= ?
           AND  date_time <= ?
           AND  exchange = ?
         GROUP BY exchange, trading_pair
    ) AS qry1
         ON (qry1.exchange = exchange_rates.exchange
         AND qry1.trading_pair = exchange_rates.trading_pair)
 WHERE  exchange_rates.date_time >= ?
   AND  exchange_rates.date_time <= ?
   AND  exchange_rates.exchange = ?
 GROUP BY qry1.exchange, qry1.trading_pair
    /);

    $rates_sth->execute($timestamp_from->epoch, $timestamp_to->epoch, $exchange_id,
                        $timestamp_from->epoch, $timestamp_to->epoch, $exchange_id);
    $self->log_trace('Select Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

# Data has been ordered by date_time; therefore we can assume 1st row is the minimal date_time row
    $time_start = [gettimeofday];
    $self->log_trace('Process Raw Exchange Rate Data Start');
    my %saw_first_row;
    while (my $rates_ref = $rates_sth->fetchrow_hashref()) {
        if ( $rates_ref->{samples}//0 > $n_samples ) {
            $n_samples = $rates_ref->{samples};
        }

        my $hash_key = join(' ', $rates_ref->{exchange}, $rates_ref->{trading_pair});
        $rate_trends->{$hash_key}->{min_timestamp} = $rates_ref->{min_timestamp};
        $rate_trends->{$hash_key}->{start_buy}     = $rates_ref->{start_buy};
        $rate_trends->{$hash_key}->{start_sell}    = $rates_ref->{start_sell};
        $rate_trends->{$hash_key}->{start_volume}  = $rates_ref->{start_volume};
        $rate_trends->{$hash_key}->{max_timestamp} = $rates_ref->{max_timestamp};
        $rate_trends->{$hash_key}->{end_buy}       = $rates_ref->{end_buy};
        $rate_trends->{$hash_key}->{end_sell}      = $rates_ref->{end_sell};
        $rate_trends->{$hash_key}->{end_volume}    = $rates_ref->{end_volume};
        $rate_trends->{$hash_key}->{buy_total}     = $rates_ref->{buy_total};
        $rate_trends->{$hash_key}->{buy_count}     = $rates_ref->{buy_count};
        $rate_trends->{$hash_key}->{sell_total}    = $rates_ref->{sell_total};
        $rate_trends->{$hash_key}->{sell_count}    = $rates_ref->{sell_count};
        $rate_trends->{$hash_key}->{range_time}    = $rates_ref->{range_time};
$self->log_info($hash_key, $rates_ref->{range_time});
    }
    $rates_sth->finish();
    $self->log_trace('Process Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

# Previous range averages
    $time_start = [gettimeofday];
    $self->log_trace('Select Previous Raw Exchange Rate Data Start');
	my $sth = $dbh->prepare(qq{
             SELECT Exchange, Trading_Pair, sell, buy
			   FROM $table
			  WHERE date_time >= ? AND date_time <= ?
                AND exchange = ?
            });
	$sth->execute( $timestamp_prev->epoch, $timestamp_from->epoch, $exchange_id );
    $self->log_trace('Select Previous Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

    $time_start = [gettimeofday];
    $self->log_trace('Process Previous Raw Exchange Rate Data Start');
	while (my $ref = $sth->fetchrow_hashref()) {
        my $hash_key = $ref->{Exchange} . ' ' . $ref->{Trading_Pair};

		next  unless $rate_trends->{$hash_key};

        if ( !$rate_trends->{$hash_key}->{prev_buy_count} ) {
            $rate_trends->{$hash_key}->{prev_avg_buy} = 0;
            $rate_trends->{$hash_key}->{prev_buy_total} = 0;
            $rate_trends->{$hash_key}->{prev_buy_count} = 0;
        }
        $rate_trends->{$hash_key}->{prev_buy_total} = $rate_trends->{$hash_key}->{prev_buy_total} + $ref->{buy};
        $rate_trends->{$hash_key}->{prev_buy_count} = $rate_trends->{$hash_key}->{prev_buy_count} + 1;
        $rate_trends->{$hash_key}->{avg_buy_start} = $rate_trends->{$hash_key}->{prev_buy_total} / $rate_trends->{$hash_key}->{prev_buy_count};

        if ( !$rate_trends->{$hash_key}->{prev_sell_count} ) {
            $rate_trends->{$hash_key}->{prev_avg_sell} = 0;
            $rate_trends->{$hash_key}->{prev_sell_total} = 0;
            $rate_trends->{$hash_key}->{prev_sell_count} = 0;
        }
        $rate_trends->{$hash_key}->{prev_sell_total} = $rate_trends->{$hash_key}->{prev_sell_total} + $ref->{sell};
        $rate_trends->{$hash_key}->{prev_sell_count} = $rate_trends->{$hash_key}->{prev_sell_count} + 1;
	}
	$sth->finish();
    $self->log_trace('Process Previous Raw Exchange Rate Data End', tv_interval($time_start, [gettimeofday]));

# Calculations....
    $time_start = [gettimeofday];
    $self->log_trace('Calculations Start');
    if ( $rate_trends ) {
        foreach my $hash_key (keys(%{$rate_trends})) {
            $rate_trends->{$hash_key}->{avg_buy_end} = $rate_trends->{$hash_key}->{buy_total} / $rate_trends->{$hash_key}->{buy_count};
            $rate_trends->{$hash_key}->{avg_sell_end} = $rate_trends->{$hash_key}->{sell_total} / $rate_trends->{$hash_key}->{sell_count};
            $rate_trends->{$hash_key}->{buy_val} = $rate_trends->{$hash_key}->{end_buy} - $rate_trends->{$hash_key}->{start_buy};
            $rate_trends->{$hash_key}->{sell_val} = $rate_trends->{$hash_key}->{end_sell} - $rate_trends->{$hash_key}->{start_sell};
# TODO: This assumes that the volume data is a rolling window
            $rate_trends->{$hash_key}->{volume} = $rate_trends->{$hash_key}->{end_volume} - $rate_trends->{$hash_key}->{start_volume};
		    eval { $rate_trends->{$hash_key}->{sell_pct} = $rate_trends->{$hash_key}->{sell_val} / $rate_trends->{$hash_key}->{start_sell} };
		    eval { $rate_trends->{$hash_key}->{buy_pct}  = $rate_trends->{$hash_key}->{buy_val}  / $rate_trends->{$hash_key}->{start_buy} };
# Calculations....
            if ( $rate_trends->{$hash_key}->{prev_sell_count} ) {
                $rate_trends->{$hash_key}->{avg_sell_start} = $rate_trends->{$hash_key}->{prev_sell_total} / $rate_trends->{$hash_key}->{prev_sell_count};
            }
		    $rate_trends->{$hash_key}->{avg_buy_diff} = ($rate_trends->{$hash_key}->{avg_buy_end}//0) - ($rate_trends->{$hash_key}->{avg_buy_start}//$rate_trends->{$hash_key}->{avg_buy_end}//0);
		    $rate_trends->{$hash_key}->{avg_sell_diff} = ($rate_trends->{$hash_key}->{avg_sell_end}//0) - ($rate_trends->{$hash_key}->{avg_sell_start}//$rate_trends->{$hash_key}->{avg_buy_end}//0);
	    }
	}
    $self->log_trace('Calculations End', tv_interval($time_start, [gettimeofday]));

    $rate_trends->{samples} = $n_samples;
	return $rate_trends;
}

sub fetch_last_action
{
	my ($self, $exchange, $trading_pair, $trade_type, $what_wanted) = @_;
	$trading_pair =~ s/_/\//g;
	my $dbh = $self->get_dbh();
    my $action;

	my $statement = $dbh->prepare(qq/
		SELECT *
		  FROM last_trade e
         WHERE exchange = ?
           AND trading_pair = ?
		/);

	$statement->execute($exchange, $trading_pair);

	while (my $ref = $statement->fetchrow_hashref()) {
		$action = $ref;
	}
	$statement->finish();

    unless ( $action ) {
        return undef;
    }
    if ( lc($trade_type) ne lc($action->{trade_type}) ) {
        return undef;
    }
# TODO: Fix this!!!!
# If trade_when < allow_loss_after_minutes ignore the record
#    if ( DateTime->compare( DateTime::Format::MySQL->parse_datetime($action->{trade_when}),
#                            DateTime->now()->subtract(minutes => $self->config->config->{allow_loss_after_minutes}) )
#          < 0 ) {
#        return undef;
#    }
    if ( lc($what_wanted) eq 'price' ) {
        return $action->{price};
    }
    return $action->{price_wanted};
}

sub fetch_prices
{
	my ($self) = @_;
	my $dbh = $self->get_dbh();
	my @prices;

    $self->log_trace('fetch_prices Start');
	my $statement = $dbh->prepare(qq/
		SELECT e.date_time, e.exchange, e.trading_pair, e.sell, e.buy, e.volume
		  FROM exchange_rates e
		    INNER JOIN (
			SELECT max(date_time) timestamp, exchange, trading_pair
			  FROM exchange_rates
			 WHERE date_time > ?
			 GROUP BY exchange, trading_pair
		    ) x
			 ON x.timestamp = e.date_time
			AND x.exchange  = e.exchange
			AND x.trading_pair = e.trading_pair
		 ORDER BY e.exchange, e.trading_pair, e.date_time DESC
		/);

	$statement->execute(DateTime->now->subtract(hours => 12)->epoch());

    $self->log_trace('fetch_prices Storing rows');
	while (my $ref = $statement->fetchrow_hashref()) {
		push @prices, $ref;
	}
	$statement->finish();

    $self->log_trace('fetch_prices End');
$self->log_info(Dumper(\@prices));

	return @prices;
}

sub fetch_all_orders
{
	my ($self, $want_open) = @_;
	my $dbh = $self->get_dbh();
    my @orders;

    $self->log_trace('fetch_all_orders Start');

    my $open_only = "WHERE last_order_status = 'O'"     if $want_open;
    $open_only //= '';

	my $statement = $dbh->prepare(qq/
        SELECT *
          FROM orders
         $open_only
    /);
    eval { $statement->execute(); };
    $self->log_info($@) if $@;
    return @orders  if $@;

    while ( my $ref = $statement->fetchrow_hashref() ) {
        push @orders, $ref;
    }
    $statement->finish();

    $self->log_trace('fetch_all_orders End');
	return @orders;
}

sub fetch_open_orders
{
	my ($self, $exchange, $want_expired) = @_;
	my $dbh = $self->get_dbh();
    my @orders;

    $self->log_trace('fetch_open_orders Start');
    my $expired = 'AND when_timeout < ?'  if $want_expired;
    $expired //= '';

	my $statement = $dbh->prepare(qq/
        SELECT *
          FROM orders
         WHERE ( last_order_status = 'O' OR last_order_status IS NULL )
           AND exchange = ?
         $expired
    /);
    if ( $want_expired ) {
        eval { $statement->execute($exchange, DateTime->now); };
        return @orders  if $@;
    } else {
        eval { $statement->execute($exchange); };
        return @orders  if $@;
    }

    while ( my $ref = $statement->fetchrow_hashref() ) {
        push @orders, $ref;
    }
    $statement->finish();

    $self->log_trace('fetch_open_orders End');
	return @orders;
}

sub update_order
{
	my ($self, $order_id, $new_remaining, $new_status) = @_;
	my $dbh = $self->get_dbh();

    $self->log_trace('update_order Start');
	my $updated = $dbh->do(qq/
        UPDATE  orders
           SET  last_order_status = ?,
                last_remaining    = ?,
                last_checked      = ?
         WHERE  order_id = ?
    /, {}, $new_status, $new_remaining, DateTime->now(), $order_id );

    if ( $updated != 1 ) {
        $self->log_debug('ERROR: Updated', $updated, 'rows -- only expected to update 1');
    }

    $self->log_trace('update_order End');
}

1;
