# Bittrader

override INC_DIR += -I ./src -I /usr/include/mysql
override LIB_DIR += -L .
CFLAGS   := -std=c99
CXXFLAGS := -Wall -pedantic -std=c++11 -Wno-missing-braces
LDFLAGS  := 
LDLIBS   := -lcrypto -ljansson -lcurl
CC       := gcc


COLLECTOR = bittrader-collect
SOURCES = $(wildcard src/*.cpp src/*.c)
OBJECTS = $(SOURCES:.cpp=.o)

ifndef VERBOSE
  Q := @
else
  Q :=
endif

ifndef DEBUG
  CFLAGS   += -O2 -DNDEBUG
  CXXFLAGS += -O2 -DNDEBUG
else
  CFLAGS   += -O0 -g
  CXXFLAGS += -O0 -g
endif


all: $(COLLECTOR)

$(COLLECTOR): $(OBJECTS) src/collector/main.o
	@echo Linking $@:
	$(Q)$(CXX) $(LDFLAGS) $(LIB_DIR) $(OBJECTS) -o $@ $(LDLIBS)

%.o: %.cpp
	@echo Compiling $@:
	$(Q)$(CXX) $(CXXFLAGS) $(INC_DIR) -c $< -o $@

%.o: %.c
	@echo Compiling $@:
	$(Q)$(CC) $(CFLAGS) $(INC_DIR) -c $< -o $@

clean:
	$(Q)rm -rf core $(OBJECTS)
