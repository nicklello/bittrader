#!/usr/bin/perl -w

#
#	BitTrader
#
#	Main module
#
$| = 1;

use Modern::Perl;

use parent qw/BitTrader::Base/;

use DateTime;
use Time::HiRes qw/tv_interval gettimeofday/;

require BitTrader::Config;
require BitTrader::Analysis;
require BitTrader::HTML;

my $self = __PACKAGE__;
my $config = BitTrader::Config->new();
my $config_data = $config->load_config();

my @datastore = ( initialise_datastore(qq/BitTrader::Datastore::MySQL/, 'tradedb', 'tradedb', 'tradedb') );
my @exchanges = ( initialise_exchange(qq/BitTrader::Exchange::BitX/, 'NFUEU8CK-EYCAXRX4-DLBGRTNX-85U8YTPM-8K12TDM7', '6cd3dfd39aa77bd93ddbe1a69ffec10c9528e6eb', 'S5NVFYBA-KHTOZ288-M8DME5L6-2TXAS6KL-C8GS7R2R', '2b564868c44921f166b80aa0f099ad5a3aaca6ca') );

my $trader = initialise_trader(qq/BitTrader::Trader::Real/, $datastore[0], $exchanges[0]);

my $amount_to_buy;

$trader->do_the_real_or_fake_trade('btest', 'sell', 'BTC/DOGE', 'BTC', 'DOGE', 0.009115782, 0.000000630, $amount_to_buy, 0.009115782, undef, \@datastore, 'test');
$trader->do_the_real_or_fake_trade('btest', 'buy',  'BTC/DOGE', 'DOGE', 'BTC', 14469.00000, 0.000000630, $amount_to_buy, 14469.00000, undef, \@datastore, 'test');


sub initialise_exchange
{
	my ($classname, $key, $secret, $apiuser) = @_;
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	eval require $classfile;

	return $classname->new($config, $key//' ', $secret//' ', $apiuser//' ');
}

sub initialise_datastore
{
	my $classname = shift;
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	require $classfile;

	return $classname->new($config, @_);
}

sub initialise_trader
{
	my ($classname, $datastore, $exchange) = @_;
	my $classfile = $classname . '.pm';
	$classfile =~ s/::/\//g;

	require $classfile;

	my $trader = $classname->new($config, $datastore, $exchange);
    $exchange->trader($trader);

    return $trader;
}
