
# TRADING....
PERCENTAGE_OF_FUNDS_TO_TRADE	    99          # What percentage of the currency balance to try to trade
minimum_amount_to_trade	Bittrex	BTC	0.01        # Minimum per-exchange per-currency amount for trades (optional)
maximum_amount_to_trade	Bittrex	BTC	0.014       # Maximum per-exchange per-currency amount for trades (optional)

PERCENTAGE_FEE			            0.35        # If unavailable from the exchange, this is the %age rate fee charged for each trade
percentage_profit_wanted	        1.00        # %age profit wanted, trades will only be attempted if the price changes by this or more




# ANALYSIS...
MINUTES_TO_USE_FOR_RECENT_TRENDS	20	        # How many minutes of constant trend we need to register before looking for a reversal
MINUTES_TO_USE_FOR_TREND_REVERSAL	 7	        # How many minutes the trend needs to reverse over

# V2 ANALYSIS
sample_size_in_minutes               12         # How many minutes per sample
sample_count                         7          # How many samples
samples_recent                       7          # How many samples needed to establish a 'recent' trend
samples_reverse                      2          # How many samples needed to establish a 'reverse' trend


exchange_rate_lag       0.1    # Percentage, default

standard_order_timeout  5       # How many minutes we allow an order to stay alive

# Trades are normally only made if they will result in a profit according to percentage_profit_wanted or percentage_fee
# However, this timer will allow trades to occur 'at loss' after the stated number of minutes
# Default if not defined is 360 minutes = 6 hours
allow_loss_after_minutes	10080       # One WEEK

exchange	Bittrex	    TRADE_MODULE API_KEY API_SECRET
exchange    BTCe        TRADE_MODULE API_KEY API_SECRET
exchange    CEXio       TRADE_MODULE API_KEY API_SECRET USER_NAME
exchange    Kraken      TRADE_MODULE API_KEY API_SECRET

wallet      EXCHANGE_NAME      CURRENCY     -   RPCUSER RPCPASS RPCHOST RPCPORT

datastore   Informix    tradedb
datastore	MySQL	dev tradedb	password

# sendtowallet	Ccy	Amount	>send_excess,<send_all	Address

# prefer	Ccy     Score -- Prefer trading with this currency when selling, highest score wins
prefer		BTC	    99
prefer		XXBT	99

dont_buy	POINTS			# Dont buy rewards
dont_buy	XRP             # Bad things happen
dont_buy    FTC
dont_buy    PPC
dont_buy    DASH
dont_buy    RBBT

##dont_sell	MN
dont_trade	TRC/BTC
#always_sell	TRC			# We don't want to hold this coin, get rid of it as soon as we get it

# If present, this will restrict trades to only buying one of the currencies listed
# NOTE: This overrides all other dont_buy, dont_trade clauses
only_buy	BTC,XXBT,GBP,ZGBP,USD,ZEUR
#only_sell	BTC,XXBT,GBP,ZGBP,USD,ZEUR,LTC,RUR,DOGE,DVC,MEC,NMC

# Only_trade superceeds all of the above
only_trade  BTC/LTC,LTC/BTC,BTC/USD,BTC/EUR,LTC/USD,LTC/EUR,LTC/DOGE,BTC/DOGE,EUR/USD

# Pairs defined here will be monitored and reported but not traded
only_monitor    EUR/USD

# The currencies we want to perform arbitrage for; if not specified all ccys will be considered
arbitrage_ccy	BTC,LTC,GHS,XXBT,XLTC,DOGE,EUR,ZEUR

logfile             ./output.bittrader.log
log_debug           0
log_exchange_debug	1
log_trade_debug     0
log_html_debug      1

html_template   trader_status.tmpl /var/www/html/dev_bittrader_status.html

