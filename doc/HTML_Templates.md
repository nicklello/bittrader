
BitTrader will use HTML::Template to generate output on each cycle; this will allow the end user to have their own view of how trading is performing.

Full documentation of the templating language is available at CPAN


Variables available from BitTrader:-

UPDATE_TIME     When data was last updated
MINS_RECENT
MINS_REVERSE

ARBITRAGE           Arbitrage data (array for use in loops)
    RESULT          Expected result of the arbitrage when started with 100 units
    EST_PROFIT
    HAS_PROFIT
    EXCHANGE
    NUM_TRADES
    CURRENCY
    TRADE1_ACTION
    TRADE1_PAIR
    TRADE1_RATE
    TRADE1_EST_FEE
    TRADE1_EST_RESULT

AUDIT_ROWS
    DATE_TIME
    MESSAGE

ORDERINFO       Currently open orders
    DATE_TIME   When the order was placed
    EXCHANGE    Exchange placed on
    PAIR        Trading pair placed on
    ACTION      Buy or Sell
    EXPIRES     When the order is due to expire
    PRICE       The rate the order was placed at
    AMOUNT      The amount the order was placed for
    ORDER_ID    The exchange-specfic order id
    STATUS      Status of the order
    WHEN_CHECKED When status was last updated
    REMAINING   How much is remaining to trade

TRADES
    EXCHANGE
    DATE_TIME
    PAIR
    ACTION
    PRICE
    AMOUNT
    PRICE_WANTED

PRICES
    EXCHANGE
    PAIR
    PRICE_SELL
    SELL_GIVES_PROFIT
    SELL_RECENT_TREND
    SELL_RECENT_TREND_V     Value of difference in price
    SELL_REVERSE_TREND
    SELL_REVERSE_TREND_V    Value of difference in price
    PRICE_BUY
    BUY_GIVES_PROFIT
    BUY_RECENT_TREND
    BUY_RECENT_TREND_V      Value of difference in price
    BUY_REVERSE_TREND
    BUY_REVERSE_TREND_V     Value of difference in price
    VOLUME

BALANCES
    EXCHANGE
    CURRENCY
    HAS_BALANCE
    BALANCE
    EST_BTC
    EST_BTC_RATE
    EST_USD
    EST_USD_RATE
    EST_EUR
    EST_EUR_RATE
    EST_GBP
    EST_GBP_RATE

