#!/usr/bin/perl -w

#
#	BitTrader
#
#	Manual trade logging script
#
$| = 1;

use Modern::Perl;

use DateTime;
use BitTrader::BitTrader;

my $bittrader = BitTrader::BitTrader->new();
$bittrader->initialise();

my $sold_at = 3910.5;
my @datastores = $bittrader->datastores();
my $exchange_id     = 'Kraken';
my $trading_pair    = 'XXBT/ZUSD';
my $short_action    = 'sell',
my $units_to_trade  = 0.02747;
my $fee_amount      = '0';
my $fee_ccy         = undef;
my $orderid         = 'OC25LC-KCTTI-UIOTIP';

        map {
                $_->record_successful_trade(  'Manual Entry: ' . $0,
                                              $exchange_id,
                                              $trading_pair,
                                              $short_action,
                                              $sold_at,
                                              $units_to_trade,
                                              $fee_amount,
                                              $fee_ccy,
                                              $orderid,
                                              0,
                                              $sold_at * 1.005
                                            );
	    } @datastores;
