SELECT  h.exchange, h.trading_pair, c.date_time, MIN(h.date_time) date_start, MAX(h.date_time) date_end,
        AVG(h.buy)  buy_avg,  c.buy  AS buy_end,
        AVG(h.sell) sell_avg, c.sell AS sell_end
  FROM  exchange_rates c
    INNER JOIN exchange_rates h
        ON  h.exchange = c.exchange
       AND  h.trading_pair = c.trading_pair
       AND  h.date_time >= c.date_time - 10 units minute
       AND  h.date_time <= c.date_time
 WHERE  c.exchange = 'BTCe'
   AND  c.trading_pair = 'BTC/USD'
   AND  c.date_time > SYSDATE - 10 units minute
 GROUP BY h.exchange, h.trading_pair, c.date_time, buy_end, sell_end
 ORDER BY h.exchange, h.trading_pair, c.date_time
