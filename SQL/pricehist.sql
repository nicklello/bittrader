SELECT date_time,
        buy - prev_buy AS buy_change, CASE WHEN buy > prev_buy THEN 'U' WHEN buy < prev_buy THEN 'D' ELSE '=' END AS buy_trend,
        sell - prev_sell AS sell_change, CASE WHEN sell > prev_sell THEN 'U' WHEN sell < prev_sell THEN 'D' ELSE '=' END AS sell_trend
FROM (
    select date_time, buy, lead(buy) OVER (order by date_time) AS prev_buy, sell, lead(sell) OVER (order by date_time) AS prev_sell
      FROM (
          SELECT EXTEND(date_time, YEAR TO HOUR) date_time_hm, (EXTEND(date_time, MINUTE TO MINUTE)::char(8)::int8 /10)::int8 date_time_10m,
                 MIN(date_time) AS start, MAX(date_time) AS date_time,
                 MIN(buy) AS buy_min, MAX(buy) AS buy_max, AVG(buy) AS buy,
                 MIN(sell) AS sell_min, MAX(sell) AS sell_max, AVG(sell) AS sell
            FROM exchange_rates
           where exchange='BTCe'
             and trading_pair='BTC/USD'
             and date_time > sysdate - 2 units hour
            GROUP BY 1, 2
            ORDER BY 1, 2
           ) ratedata
     order by date_time
    )
WHERE buy != prev_buy
;
