SELECT  date_time, exchange, trading_pair,
        sell,
        buy
  FROM  exchange_rates
 WHERE  exchange = 'BTCe'
   AND  trading_pair = 'BTC/EUR'
 ORDER BY date_time
;
