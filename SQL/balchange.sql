CREATE TEMPORARY TABLE tmp_btc AS (
    SELECT  CASE WHEN exchange like 'Local%' THEN 'Local' ELSE exchange END AS exchange, MAX(timestamp) AS timestamp, ccy, amount
      FROM  (
        SELECT exchange, timestamp, 'BTC' ccy, SUM(amount) amount
          FROM balance_history
         WHERE ccy like 'BTC%'
         GROUP BY exchange, timestamp
         ) x
     GROUP  BY 1, ccy, amount
     )
;
CREATE TEMPORARY TABLE tmp_vtc AS (
    SELECT  CASE WHEN exchange like 'Local%' THEN 'Local' ELSE exchange END AS exchange, MAX(timestamp) AS timestamp, ccy, amount
      FROM  (
        SELECT exchange, timestamp, 'VTC' ccy, SUM(amount) amount
          FROM balance_history
         WHERE ccy like 'VTC%'
         GROUP BY exchange, timestamp
         ) x
     GROUP  BY 1, ccy, amount
     )
;
CREATE TEMPORARY TABLE tmp_ltc AS (
    SELECT  CASE WHEN exchange like 'Local%' THEN 'Local' ELSE exchange END AS exchange, MAX(timestamp) AS timestamp, ccy, amount
      FROM  balance_history
     WHERE  ccy = 'LTC'
     GROUP  BY 1, ccy, amount
     )
;
CREATE TEMPORARY TABLE tmp_ghs AS (
    SELECT  CASE WHEN exchange like 'Local%' THEN 'Local' ELSE exchange END AS exchange, MAX(timestamp) AS timestamp, ccy, amount
      FROM  balance_history
     WHERE  ccy = 'GHS'
     GROUP  BY 1, ccy, amount
     )
;
SELECT  btc.exchange, btc.timestamp, btc.amount btc, vtc.amount vtc, ltc.amount ltc, ghs.amount ghs
  FROM  tmp_btc btc
    LEFT OUTER JOIN tmp_vtc vtc
         ON vtc.exchange = btc.exchange
        AND vtc.timestamp = btc.timestamp
    LEFT OUTER JOIN tmp_ltc ltc
         ON ltc.exchange = btc.exchange
        AND ltc.timestamp = btc.timestamp
    LEFT OUTER JOIN tmp_ghs ghs
         ON ghs.exchange = btc.exchange
        AND ghs.timestamp = btc.timestamp
 ORDER BY btc.exchange, btc.timestamp
;
