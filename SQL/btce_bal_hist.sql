select COALESCE(b.timestamp, l.timestamp), b.amount btc, l.amount ltc
 from balance_history b
    left outer join balance_history l on b.timestamp = l.timestamp
where b.exchange='BTCe'
  and b.ccy = 'BTC'
  and l.exchange='BTCe'
  and l.ccy = 'LTC'
order by COALESCE(b.timestamp, l.timestamp) desc
