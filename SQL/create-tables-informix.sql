

CREATE TABLE audit_messages (
                audit_no  SERIAL,
                timestamp DATETIME YEAR TO SECOND,
                message TEXT,

                PRIMARY KEY (audit_no)
                )
;
CREATE TABLE balance_history (
                timestamp DATETIME YEAR TO SECOND,
                exchange VARCHAR(10),
                ccy VARCHAR(64),
                amount DOUBLE PRECISION,

                PRIMARY KEY (timestamp, exchange, ccy)
                )
;
CREATE TABLE latest_balance (
                timestamp DATETIME YEAR TO SECOND,
                exchange VARCHAR(10),
                ccy VARCHAR(64),
                amount DOUBLE PRECISION,

                PRIMARY KEY (exchange, ccy)
                )
;
CREATE TABLE exchange_rates (
                date_time DATETIME YEAR TO SECOND,
                exchange VARCHAR(10),
                trading_pair VARCHAR(20),
                sell DOUBLE PRECISION,
                buy  DOUBLE PRECISION,
                volume DOUBLE PRECISION,

                PRIMARY KEY (date_time, exchange, trading_pair)
                )
;
CREATE INDEX idx_exchange_rates_pk ON exchange_rates(date_time)
;
CREATE INDEX idx_exchange_rates_ex ON exchange_rates(exchange)
;
CREATE TABLE orders (
                sequence            SERIAL,
                when_placed         DATETIME YEAR TO SECOND,
                exchange            VARCHAR(10),
                trading_pair        VARCHAR(20),
                trade_type          VARCHAR(5),
                order_origin VARCHAR(20),
                when_timeout        DATETIME YEAR TO SECOND,
                price               DOUBLE PRECISION,
                amount              DOUBLE PRECISION,
                order_id            VARCHAR(32),

                last_order_status   VARCHAR(1),
                last_checked        DATETIME YEAR TO SECOND,
                last_remaining      DOUBLE PRECISION,

                PRIMARY KEY (when_placed, exchange, trading_pair, trade_type)
                )
;
CREATE INDEX idx_orders_st_to ON orders(last_order_status, when_timeout)
;
CREATE INDEX idx_orders_oid   ON orders(order_id)
;
CREATE TABLE successful_trades (
                sequence SERIAL,
                trade_when DATETIME YEAR TO SECOND,
                exchange VARCHAR(10),
                trading_pair VARCHAR(20),
                trade_type VARCHAR(5),
                order_origin VARCHAR(20),
                price  DOUBLE PRECISION,
                amount DOUBLE PRECISION,
                fee    DOUBLE PRECISION,
                fee_ccy VARCHAR(10),
                order_id VARCHAR(32),
                est_btc DOUBLE PRECISION,
                price_wanted  DOUBLE PRECISION,

                PRIMARY KEY (exchange, trading_pair, trade_when)
                )
;
CREATE INDEX idx_successful_trades_oid ON successful_trades(order_id)
;
CREATE TABLE last_trade (
                exchange VARCHAR(10),
                trading_pair VARCHAR(20),
                trade_type VARCHAR(5),
                order_origin VARCHAR(20),
                price  DOUBLE PRECISION,
                amount DOUBLE PRECISION,
                est_btc DOUBLE PRECISION,
                price_wanted  DOUBLE PRECISION,
                trade_when   DATETIME YEAR TO SECOND,

                PRIMARY KEY (exchange, trading_pair)
                )
;
CREATE TABLE arbitrage_estimate (
                sequence SERIAL,
                timestamp DATETIME YEAR TO SECOND,
                exchange VARCHAR(10),
                currency VARCHAR(10),
                trades   INTEGER,
                result        DOUBLE PRECISION,
                trade1_action VARCHAR(5),
                trade1_pair   VARCHAR(10),
                trade1_rate   DOUBLE PRECISION,
                trade1_result DOUBLE PRECISION,
                trade1_fee    VARCHAR(30),
                trade2_action VARCHAR(5),
                trade2_pair   VARCHAR(10),
                trade2_rate   DOUBLE PRECISION,
                trade2_result DOUBLE PRECISION,
                trade2_fee    VARCHAR(30),
                trade3_action VARCHAR(5),
                trade3_pair   VARCHAR(10),
                trade3_rate   DOUBLE PRECISION,
                trade3_result DOUBLE PRECISION,
                trade3_fee    VARCHAR(30),
                trade4_action VARCHAR(5),
                trade4_pair   VARCHAR(10),
                trade4_rate   DOUBLE PRECISION,
                trade4_result DOUBLE PRECISION,
                trade4_fee    VARCHAR(30),

                PRIMARY KEY (sequence)
                )
;
CREATE INDEX idx_arbitrage_estimate_exch_ccy ON arbitrage_estimate(exchange, currency)
;
