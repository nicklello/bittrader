
CREATE TABLE IF NOT EXISTS audit_messages (
				date_time TIMESTAMP,
				message TEXT,

				PRIMARY KEY (date_time)
);

CREATE TABLE IF NOT EXISTS balance_history (
			date_time TIMESTAMP,
			Exchange VARCHAR,
			CCY VARCHAR,
			Amount DOUBLE,

			PRIMARY KEY (date_time, Exchange, CCY)
);

CREATE TABLE IF NOT EXISTS latest_balance (
			date_time TIMESTAMP,
			Exchange VARCHAR,
			CCY VARCHAR,
			Amount DOUBLE,

			PRIMARY KEY (Exchange, CCY)
);

CREATE TABLE IF NOT EXISTS exchange_rates (
			Exchange VARCHAR,
			Trading_Pair VARCHAR,
			sell DOUBLE,
			buy  DOUBLE,
			volume DOUBLE,

			PRIMARY KEY (Exchange, Trading_Pair)
);

CREATE TABLE IF NOT EXISTS latest_exchange_rates (
			date_time TIMESTAMP,
			Exchange VARCHAR,
			Trading_Pair VARCHAR,
			sell DOUBLE,
			buy  DOUBLE,
			volume DOUBLE,

			PRIMARY KEY (Exchange, date_time, Trading_Pair)
);
CREATE INDEX IF NOT EXISTS exchange_rates_dt ON exchange_rates(date_time);

CREATE TABLE IF NOT EXISTS successful_trades (
			trade_when TIMESTAMP,
			Exchange VARCHAR,
			Trading_Pair VARCHAR,
			trade_type VARCHAR,
            order_origin VARCHAR,
			price  DOUBLE,
			amount DOUBLE,
			fee    DOUBLE,
			fee_ccy VARCHAR,
			order_id VARCHAR,
			est_btc DOUBLE,
			price_wanted  DOUBLE,

			PRIMARY KEY (Exchange, Trading_Pair, trade_when)
);
CREATE INDEX IF NOT EXISTS idx_successful_trades_oid ON successful_trades(order_id);

CREATE TABLE IF NOT EXISTS last_trade (
			Exchange VARCHAR,
			Trading_Pair VARCHAR,
			trade_type VARCHAR,
            order_origin VARCHAR,
			price  DOUBLE,
			amount DOUBLE,
			price_wanted  DOUBLE,
			trade_when   TIMESTAMP,

			PRIMARY KEY (Exchange, Trading_Pair)
);

CREATE TABLE IF NOT EXISTS arbitrage_estimate (
			date_time TIMESTAMP,
			Exchange VARCHAR,
			Currency VARCHAR,
			trades   INT,
            result        DOUBLE,
            trade1_action VARCHAR,
            trade1_pair   VARCHAR,
            trade1_rate   DOUBLE,
            trade1_result DOUBLE,
            trade1_fee    VARCHAR,
            trade2_action VARCHAR,
            trade2_pair   VARCHAR,
            trade2_rate   DOUBLE,
            trade2_result DOUBLE,
            trade2_fee    VARCHAR,
            trade3_action VARCHAR,
            trade3_pair   VARCHAR,
            trade3_rate   DOUBLE,
            trade3_result DOUBLE,
            trade3_fee    VARCHAR,
            trade4_action VARCHAR,
            trade4_pair   VARCHAR,
            trade4_rate   DOUBLE,
            trade4_result DOUBLE,
            trade4_fee    VARCHAR,

			PRIMARY KEY (date_time, Exchange, Currency)
);

CREATE TABLE IF NOT EXISTS orders (
				when_placed         TIMESTAMP,
				exchange            VARCHAR,
				trading_pair        VARCHAR,
				trade_type          VARCHAR,
            order_origin VARCHAR,
				when_timeout        TIMESTAMP,
				price               DOUBLE,
				amount              DOUBLE,
				order_id            VARCHAR,

                last_order_status   VARCHAR,
                last_checked        TIMESTAMP,
                last_remaining      DOUBLE,

                PRIMARY KEY (order_id)
);
CREATE INDEX idx_orders_when   ON orders(when_placed);
CREATE INDEX idx_orders_pair  ON orders(trading_pair);
CREATE INDEX idx_orders_stat  ON orders(last_order_status);
CREATE INDEX idx_orders_exch  ON orders(exchange);
CREATE INDEX idx_orders_timeout  ON orders(when_timeout);
