SELECT  count(*)
  FROM  arbitrage_estimate
;
SELECT  exchange, currency, CASE WHEN result > 100 THEN 'Profit' ELSE 'Loss' END AS status, count(*)
  FROM  arbitrage_estimate
 GROUP  BY exchange, currency, 3
 ORDER  BY count(*) DESC
;
