DROP TABLE tmp_raw
;
SELECT  EXTEND(date_time, year to minute) date_time, exchange, buy, sell
  FROM  exchange_rates
 WHERE  trading_pair IN ( 'BTC/USD', 'XXBT/ZUSD' )
 ORDER BY date_time
;
