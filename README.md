
REWRITE
-------
Rewrite of bittrader Perl version into c/c++

Step 0 is to retire Informix as a datastore; correct operation of the mysql code needs to be verified first

Stage 1 is to fork off 'blackbird's source to create a data collector process
*- Data will be logged to mariadb/mysql
*- Existing perl code will modified to use the collector's data
*- May use the existing tables...

Stage 2 will introduce an analysis process/thread
* Initial model will follow the existing analysis methods
* Results to be stored in new tables
* Existing perl code to be reduced/retroed to work off the potted analysis data
* Data must be formatted for modelling/machine learning

Stage 3 - trading
* Move trading parts from perl into a new process / thread triggered by analysis
* Perl code will reduce to html production

Stage 4 - Replacement of existing html production code
* Possibly keep it in perl to keep the html::template 






BitTrader
=========

Coin trading monitor / bot

Does not (knowingly) use any existing trading methods other than watching price movements over time.

It will:

* Monitor and Record coin balances on multiple exchanges

* Monitor and Record exchange rates on multiple exchanges.

* Perform simple trading based on price +/- fees exceeding previous trade price

* Attempt to perform arbitrage to increase the amount of any one coin.


**NEW PHASE**

I am planning a new phase for the bot development; ACTIVE trading... ie (configurable):-
* Place orders for the rate we want to make a profit
* Monitor those orders
  * Order timeout
* When an order completes, place a new order to trade back at a profit


BitTrader needs a data store
============================
Currently, modules exist for:-
* Informix IDS Database (my personal choice)
* MySQL Database
* Flat Files (Workable but v.slow)

In a (very?) alpha stage is a module for Cassandra data storage


BitTrader uses *LOTS* of memory
===============================
It's leaky as hell.... I have not had the time to track down the memory leaks


Exchanges Supported
===================
* BittRex
* BitX
* BTC-e
* CEX.io
* Cryptsy (Turned into a PONZI!! Removed...)
* Kraken

Supported for balance tracking only

* CoinBase
* HashNest

Wallets using a bitcoin-cli compatible command line tool are also supported.
