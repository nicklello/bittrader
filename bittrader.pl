#!/usr/bin/perl -w

#
#	BitTrader
#
#	Main module
#
$| = 1;

use Modern::Perl;

use DateTime;
use BitTrader::BitTrader;

#for ( ;; ) {
    my $start = DateTime->now();
    print $start->datetime(), ' Start cycle', "\n";
	my $start_sec = ( $start->minute() * 60 ) + $start->second;
    my $bittrader = BitTrader::BitTrader->new();
    $bittrader->initialise();

	$bittrader->run_collectors();
    undef($bittrader);

    my $finish = DateTime->now();
	my $end_sec = ( $finish->minute() * 60 ) + $finish->second;
#    my $sleepfor = ($start_sec + 60) - $end_sec;
#    $sleepfor = 60                  if $sleepfor <= 0 || $sleepfor > 60;
my $sleepfor = 10;

	print $finish->datetime(), ' Finish cycle - Sleeping ', $sleepfor, ' seconds', "\n";
	sleep($sleepfor);
    print "\n";
#}
