#include <stdio.h>
#include "config.h"
#include "database.h"

/*
*   1. Load config
*   2. Open database
*   3. Begin transaction
*   4. Loop through exchanges/currencies collecting data and writing to database
*   5. Commit
*   6. Recurse
*/

int main(int argc, char** argv)
{
    printf ("\nCollector v0.01\n---------------\n\n");

/*   1. Load config */
    struct Config *config = load_config();

/*   2. Open database */
    MYSQL *database = open_mysql_database('tradedb');

/*   3. Begin transaction */
/*   4. Loop through exchanges/currencies collecting data and writing to database */
/*   5. Commit */
/*   6. Recurse */
}

